import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthSigninComponent } from './demo/pages/authentication/auth-signin/auth-signin.component';
import { AuthSignupComponent } from './demo/pages/authentication/auth-signup/auth-signup.component';
import { AdminComponent } from './theme/layout/admin/admin.component';

const routes: Routes = [
  {
    path: '',
    component: AuthSigninComponent,
    children: [
      {
        path: '',
        redirectTo: 'signin',
        pathMatch: 'full'
      },
      {
        path: '',
        loadChildren: () => import('./demo/pages/authentication/auth-signin/auth-signin.module').then(module => module.AuthSigninModule)
      }
    ]
  },
  {
    path: 'signup',
    component: AuthSignupComponent,
    children: [
      {
        path: 'signup',
        loadChildren: () => import('./demo/pages/authentication/auth-signup/auth-signup.module').then(module => module.AuthSignupModule)
      }
    ]
  },
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard/analytics',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./demo/dashboard/dashboard.module').then(module => module.DashboardModule)
      },
      {
        path: 'user',
        loadChildren: () => import('./demo/pages/user/user.module').then(module => module.UserModule)
      },
      {
        path: 'access-matrix',
        loadChildren: () => import('./demo/pages/access-matrixs/access-matrixs.module').then(module => module.AccessMatrixsModule)
      },
      {
        path: 'role',
        loadChildren: () => import('./demo/pages/roles/roles.module').then(module => module.RolesModule)
      },
      {
        path: 'company',
        loadChildren: () => import('./demo/pages/company/company.module').then(module => module.CompanyModule)
      },
      {
        path: 'skill',
        loadChildren: () => import('./demo/pages/skills/skills.module').then(module => module.SkillsModule)
      },
      {
        path: 'career-level',
        loadChildren: () => import('./demo/pages/career-levels/career-levels.module').then(module => module.CareerLevelsModule)
      },
      {
        path: 'job-type',
        loadChildren: () => import('./demo/pages/job-types/job-types.module').then(module => module.JobTypesModule)
      },
      {
        path: 'specialization',
        loadChildren: () => import('./demo/pages/specializations/specializations.module').then(module => module.SpecializationsModule)
      },
      {
        path: 'working-site',
        loadChildren: () => import('./demo/pages/working-sites/working-sites.module').then(module => module.WorkingSitesModule)
      },
      {
        path: 'location',
        loadChildren: () => import('./demo/pages/locations/locations.module').then(module => module.LocationsModule)
      },
      {
        path: 'vacancy',
        loadChildren: () => import('./demo/pages/user-vacancy/user-vacancy.module').then(module => module.UserVacancyModule)
      },
      {
        path: 'applicant-list',
        loadChildren: () => import('./demo/pages/application-lists/application-lists.module').then(module => module.ApplicationListsModule)
      },
      {
        path: 'business-unit',
        loadChildren: () => import('./demo/pages/business-units/business-units.module').then(module => module.BusinessUnitsModule)
      },
      {
        path: 'user-applicant',
        loadChildren: () => import('./demo/pages/user-applicants/user-applicants.module').then(module => module.UserApplicantsModule)
      },
      {
        path: 'notif',
        loadChildren: () => import('./demo/pages/notif/notif.module').then(module => module.NotifModule)
      },
      {
        path: 'cv',
        loadChildren: () => import('./demo/pages/cv-lists/cv-lists.module').then(module => module.CvListsModule)
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
