import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BusinessUnitsRoutingModule } from './business-units-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BusinessUnitsRoutingModule
  ]
})
export class BusinessUnitsModule { }
