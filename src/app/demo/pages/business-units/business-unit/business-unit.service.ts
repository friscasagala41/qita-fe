import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { BusinessUnit } from './business-unit.model';

@Injectable({
  providedIn: 'root'
})
export class BusinessUnitService {
  // public subject = new Subject<any>();

  private dataSource = new BehaviorSubject<any>(new BusinessUnit());
  data = this.dataSource.asObservable();
  // data = this.subject.asObservable();
  SERVER: string;
  constructor(private http: HttpClient,
              private configService: AppConfigService) {
        this.SERVER = this.configService.getSavedServerPath();
    }

  updatedDataSelection(data: BusinessUnit) {
      this.dataSource.next(data);
  }

  getAllBusinessUnit(): Observable<HttpResponse<BusinessUnit[]>> {
    let newResourceUrl = null;
    let result = null;
    const search = {};

    newResourceUrl = this.SERVER + `/api/business-unit/all`;

    result = this.http.post<BusinessUnit[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  filter(req?: any): Observable<HttpResponse<BusinessUnit[]>> {
    let pageNumber = null;
    let pageCount = null;
    let newResourceUrl = null;
    let result = null;
    const search = {
        name: ''
    };

    Object.keys(req).forEach((key) => {
        if (key === 'page') {
            pageNumber = req[key];
        }
        if (key === 'count') {
            pageCount = req[key];
        }
        if (key === 'name') {
            search.name = req[key];
        }
    });

    newResourceUrl = this.SERVER + `/api/business-unit/filter/page/${pageNumber}/count/${pageCount}`;

    result = this.http.post<BusinessUnit[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

}
