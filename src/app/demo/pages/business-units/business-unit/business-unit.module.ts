import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BusinessUnitRoutingModule } from './business-unit-routing.module';
import { BusinessUnitComponent } from './business-unit.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import {NgbDropdownModule, NgbModalModule, NgbModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [BusinessUnitComponent],
  imports: [
    CommonModule,
    BusinessUnitRoutingModule,
    SharedModule,
    NgbDropdownModule,
    NgbModalModule,
    NgbPaginationModule
  ]
})
export class BusinessUnitModule { }
