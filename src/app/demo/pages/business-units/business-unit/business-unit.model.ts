export class BusinessUnit {
    constructor(
        public id?: number,
        public name?: string,
        public code?: string,
        public errCode?: string,
        public errDesc?: string
    ) {}
}
