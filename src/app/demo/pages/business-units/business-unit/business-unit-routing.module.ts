import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BusinessUnitComponent } from './business-unit.component';


const routes: Routes = [
  {
    path: '',
    component: BusinessUnitComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessUnitRoutingModule { }
