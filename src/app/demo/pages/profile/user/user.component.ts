import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  showForm: boolean;

  constructor() { }

  ngOnInit() {
    this.showForm = true;
  }

  edit() {
    this.showForm = false;
  }

  batal() {
    this.showForm = true;
  }

}
