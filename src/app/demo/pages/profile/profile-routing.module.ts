import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  // {
  //   path: '',
  //   children: [
  //     {
  //       path: 'profile',
  //       loadChildren: () => import('./user/user.module').then(module => module.UserModule)
  //     },
  //     {
  //       path: 'company',
  //       loadChildren: () => import('./company/company.module').then(module => module.CompanyModule)
  //     }
  //   ]
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
