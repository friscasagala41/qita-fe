import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'profile',
        loadChildren: () => import('./company-profile/company-profile.module').then(module => module.CompanyProfileModule)
      },
      {
        path: 'vacancy',
        loadChildren: () => import('./company-vacancy/company-vacancy.module').then(module => module.CompanyVacancyModule)
      },
      {
        path: 'location',
        loadChildren: () => import('./company-location/company-location.module').then(module => module.CompanyLocationModule)
      },
      {
        path: 'additional',
        loadChildren: () => import('./company-additional/company-additional.module').then(module => module.CompanyAdditionalModule)
      },
      {
        path: 'requirement',
        loadChildren: () => import('./vacancy-requirement/vacancy-requirement.module').then(module => module.VacancyRequirementModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyRoutingModule { }
