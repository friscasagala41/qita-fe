import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyAdditionalComponent } from './company-additional.component';

describe('CompanyAdditionalComponent', () => {
  let component: CompanyAdditionalComponent;
  let fixture: ComponentFixture<CompanyAdditionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyAdditionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyAdditionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
