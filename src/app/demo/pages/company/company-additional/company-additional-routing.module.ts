import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyAdditionalComponent } from './company-additional.component';


const routes: Routes = [
  {
    path: '',
    component: CompanyAdditionalComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyAdditionalRoutingModule { }
