import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyAdditionalRoutingModule } from './company-additional-routing.module';
import { CompanyAdditionalComponent } from './company-additional.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import {NgbDropdownModule, NgbModalModule, NgbModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [CompanyAdditionalComponent],
  imports: [
    CommonModule,
    CompanyAdditionalRoutingModule,
    SharedModule,
    NgbDropdownModule,
    NgbModalModule,
    NgbPaginationModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class CompanyAdditionalModule { }
