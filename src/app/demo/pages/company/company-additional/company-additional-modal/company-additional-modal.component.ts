import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MDBModalRef, MDBModalService, ToastService } from 'ng-uikit-pro-standard';
import { Subject, Subscription } from 'rxjs';
import Swal from 'sweetalert2';
import { CompanyAdditional } from '../company-additional.model';
import { CompanyAdditionalService } from '../company-additional.service';

@Component({
  selector: 'app-company-additional-modal',
  templateUrl: './company-additional-modal.component.html',
  styleUrls: ['./company-additional-modal.component.scss']
})
export class CompanyAdditionalModalComponent implements OnInit {
  objedit: any;
  action = new Subject();
  additional: CompanyAdditional;
  messages;
  subscription: Subscription;
  dataFromGrid: MDBModalRef;
  nama: string;
  uneditable: boolean;
  addtionalForm: FormGroup;

  constructor(
    private modalService: MDBModalService,
    public modalRef: MDBModalRef,
    public addtionalService: CompanyAdditionalService,
    private toastrService: ToastService,
    private fb: FormBuilder
  ) {
    this.rForm();
  }

  rForm() {
    this.addtionalForm = this.fb.group({
      id: [{value: '', disabled: true}],
      description: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.subscription = this.addtionalService.data.subscribe(data => {
      console.log('data behavierrrr', data);
      this.additional = data;
    });
    console.log('on init modal ');
    console.log('user modal ', this.objedit);
  }

  closeForm() {
    this.action.next('refresh');
    this.modalService._hideModal(1);
  }

  save() {
    this.additional.description = this.addtionalForm.get('description').value;

    console.log('save ==> ', this.additional);
    this.addtionalService.add(this.additional).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
        if (result.body.errDesc !== 'SUCCESS..') {
          // const options = { progressBar: true, extendedTimeOut: 3000, timeOut: 5000 };
          // this.toastrService.error(result.body.errDesc, 'New Password', options);
          // alert('Password ' + );
          Swal.fire('Error', result.body.errDesc, 'error');
        } else {
          // this.toastrService.success('Save success !', 'save');
          Swal.fire('Success', 'Company additional saved', 'success');
        }
        this.action.next('refresh');
        this.closeForm();
      } else {
        alert('error');
      }
    });
  }

  edit() {
    console.log('update ==> ', this.additional);
    this.addtionalService.update(this.additional).subscribe(result => {
      console.log(result);
      // if (result.body.errCode !== '00') {
      if (result.body.errCode !== '00') {
        // const options = { progressBar: true, extendedTimeOut: 3000, timeOut: 5000 };
        // this.toastrService.error(result.body.errDesc, 'New Password', options);
        Swal.fire('Error', result.body.errDesc, 'error');
        // alert('Password ' + );
      } else {
        // this.toastrService.success('Update success !');
        Swal.fire('Success', 'Company additional saved', 'success');
      }
      this.action.next('refresh');
      this.closeForm();
      // } else {
      //   alert('error');
      // }
    });
  }

}
