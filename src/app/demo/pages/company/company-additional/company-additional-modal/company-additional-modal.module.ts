import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyAdditionalModalComponent } from './company-additional-modal.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CompanyAdditionalModalComponent]
})
export class CompanyAdditionalModalModule { }
