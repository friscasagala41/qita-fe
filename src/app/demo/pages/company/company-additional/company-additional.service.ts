import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { CompanyAdditional } from './company-additional.model';

@Injectable({
  providedIn: 'root'
})
export class CompanyAdditionalService {

  // public subject = new Subject<any>();

  private dataSource = new BehaviorSubject<any>(new CompanyAdditional());
  data = this.dataSource.asObservable();
  // data = this.subject.asObservable();
  SERVER: string;
  constructor(private http: HttpClient,
              private configService: AppConfigService) {
        this.SERVER = this.configService.getSavedServerPath();
    }

  updatedDataSelection(data: CompanyAdditional) {
      this.dataSource.next(data);
  }

  filter(req?: any): Observable<HttpResponse<CompanyAdditional[]>> {
    let pageNumber = null;
    let pageCount = null;
    let newResourceUrl = null;
    let result = null;
    const search = {
        name: ''
    };

    Object.keys(req).forEach((key) => {
        if (key === 'page') {
            pageNumber = req[key];
        }
        if (key === 'count') {
            pageCount = req[key];
        }
        if (key === 'name') {
            search.name = req[key];
        }
    });

    newResourceUrl = this.SERVER + `/api/company/additional/filter/page/${pageNumber}/count/${pageCount}`;

    result = this.http.post<CompanyAdditional[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  getAllAdditional(): Observable<HttpResponse<CompanyAdditional[]>> {
      let newResourceUrl = null;
      let result = null;
      const search = {};

      newResourceUrl = this.SERVER + `/api/company/additional/all`;

      result = this.http.post<CompanyAdditional[]>(newResourceUrl, search, { observe: 'response' })
          .pipe(
              tap(
                  // console.log('sdad')
                  results => console.log('raw', results)
              )
          );
      // console.log(result);
      return result;
  }

  add(addtional: CompanyAdditional): Observable<HttpResponse<CompanyAdditional>> {
    const copy = Object.assign({}, addtional);

    copy.id = +copy.id;

    const newResourceUrl = this.SERVER + `/api/company/additional/`;

    return this.http.post<CompanyAdditional>(newResourceUrl, copy, { observe: 'response'})
        .pipe(map((res: HttpResponse<CompanyAdditional>) => Object.assign({}, res)));
  }

  getAdditional(req ?: any): Observable<HttpResponse<CompanyAdditional>> {
    let newResourceUrl = null;
    let result = null;
    let id = null;
    const search = {};

    Object.keys(req).forEach((key) => {
      if (key === 'id') {
          id = req[key];
      }
  });

    newResourceUrl = this.SERVER + `/api/company/additional/${id}`;

    result = this.http.post<CompanyAdditional[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  update(addtional: CompanyAdditional): Observable<HttpResponse<CompanyAdditional>> {
    const copy = Object.assign({}, addtional);

    copy.id = + copy.id;

    const newResourceUrl = this.SERVER + `/api/company/additional/`;

    return this.http.put<CompanyAdditional>(newResourceUrl, copy, { observe: 'response' })
        .pipe(map((res: HttpResponse<CompanyAdditional>) => Object.assign({}, res)));
  }

}
