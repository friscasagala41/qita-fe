import { HttpResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MDBModalRef, MDBModalService } from 'ng-uikit-pro-standard';
import { Subject } from 'rxjs';
import { TOTAL_RECORD_PER_PAGE } from 'src/app/theme/shared/constant/base.constant';
import { CompanyAdditionalModalComponent } from './company-additional-modal/company-additional-modal.component';
import { CompanyAdditional } from './company-additional.model';
import { CompanyAdditionalService } from './company-additional.service';

@Component({
  selector: 'app-company-additional',
  templateUrl: './company-additional.component.html',
  styleUrls: ['./company-additional.component.scss']
})
export class CompanyAdditionalComponent implements OnInit, OnDestroy {

  searchName;
  addtionals: CompanyAdditional[];

  maxTotalpage = 1;
  curPage: number;
  totalItems: any;
  itemsPerPage: any;
  previousPage: any;
  // total = [];
  nameCareer = '';
  description: string;
  addtionalForm: FormGroup;
  action = new Subject();
  closeResult: string;
  modalRef: MDBModalRef;
  addtional: CompanyAdditional;

  constructor(
    private addtionalService: CompanyAdditionalService,
    private fb: FormBuilder,
    private modalService: MDBModalService,
  ) { }

  ngOnInit() {
    this.curPage = 1;
    this.filter(this.curPage);
  }

  rForm() {
    this.addtionalForm = this.fb.group({
      description: ['', Validators.required]
    });
  }


  filter(curpage) {
    console.log('searc ', this.searchName);
    this.addtionalService.filter({
        page: curpage,
        count: 10,
        name: this.searchName,
    })
        .subscribe(
            (res: HttpResponse<CompanyAdditional[]>) => {
                console.log('res.body', res.body);
                this.successLoadUser(res.body);

            }
        );
  }

  private successLoadUser(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.addtionals = data.contents;
    this.maxTotalpage = Math.ceil(data.totalRow / TOTAL_RECORD_PER_PAGE);
    this.totalItems = data.totalRow;
    this.itemsPerPage = TOTAL_RECORD_PER_PAGE;
  }

  ngOnDestroy() {
    // this.userService.clearMessages();
  }

  loadPage(curPage) {
      if (this.curPage !== this.previousPage) {
        this.previousPage = this.curPage;
        this.filter(this.curPage);
        console.log('filter', this.filter);
      }
  }

  open(status: string, data: CompanyAdditional) {

    if (status === 'Add') {
        data = new CompanyAdditional();
        data.id = 0;
        data.description = '';
    }
    console.log('isi dataaa ', data);

    this.addtionalService.updatedDataSelection(data);

    const modalOptions = {
        backdrop: true,
        keyboard: true,
        focus: true,
        show: false,
        ignoreBackdropClick: false,
        class: 'modal-lg',
        containerClass: 'right',
        animated: true,
        data: {
            objedit: data,
            uneditable: false
        }
    };

    if (status === 'detail') {
        modalOptions.data.uneditable = true;
    }

    // console.log()
    this.modalRef = this.modalService.show(CompanyAdditionalModalComponent, modalOptions);
    this.modalRef.content.action.subscribe(
        (result: any) => {
            console.log(result);
            if (result === 'refresh') {
                this.curPage = 1;
                this.filter(this.curPage);
            }
        });
  }

}
