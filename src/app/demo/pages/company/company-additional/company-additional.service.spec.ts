/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CompanyAdditionalService } from './company-additional.service';

describe('Service: CompanyAdditional', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CompanyAdditionalService]
    });
  });

  it('should ...', inject([CompanyAdditionalService], (service: CompanyAdditionalService) => {
    expect(service).toBeTruthy();
  }));
});
