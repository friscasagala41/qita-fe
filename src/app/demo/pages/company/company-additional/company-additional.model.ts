export class CompanyAdditional {
    constructor(
        public id?: number,
        public description?: string,
        public errCode?: string,
        public errDesc?: string
    ) {}
}
