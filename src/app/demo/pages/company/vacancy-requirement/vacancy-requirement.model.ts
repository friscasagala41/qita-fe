import { Skill } from '../../skills/skill/skill.model';
import { CompanyVacancy } from '../company-vacancy/company-vacancy.model';

export class VacancySkill {
    constructor(
        public id?: number,
        public skillID?: number,
        public skill?: Skill,
        public errCode?: string,
        public errDesc?: string,
        public vacancyID?: number,
        public vacancy?: CompanyVacancy
    ) {}
}
