import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VacancyRequirementComponent } from './vacancy-requirement.component';

describe('VacancyRequirementComponent', () => {
  let component: VacancyRequirementComponent;
  let fixture: ComponentFixture<VacancyRequirementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VacancyRequirementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VacancyRequirementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
