import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VacancyRequirementRoutingModule } from './vacancy-requirement-routing.module';
import { VacancyRequirementComponent } from './vacancy-requirement.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { Select2Module } from 'ng2-select2';
import { NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [VacancyRequirementComponent],
  imports: [
    CommonModule,
    VacancyRequirementRoutingModule,
    SharedModule,
    SharedModule,
    Select2Module,
    NgbProgressbarModule,
    Select2Module
  ]
})
export class VacancyRequirementModule { }
