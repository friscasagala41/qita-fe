import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyVacancyComponent } from './company-vacancy.component';


const routes: Routes = [
  {
    path: '',
    component: CompanyVacancyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyVacancyRoutingModule { }
