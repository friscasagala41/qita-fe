import { CurrencyPipe } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2';
import { CareerLevel } from '../../career-levels/career-level/career-level.model';
import { CareerLevelService } from '../../career-levels/career-level/career-level.service';
import { JobType } from '../../job-types/job-type/job-type.model';
import { JobTypeService } from '../../job-types/job-type/job-type.service';
import { Specialization } from '../../specializations/specialization/specialization.model';
import { SpecializationService } from '../../specializations/specialization/specialization.service';
import { WorkingSite } from '../../working-sites/working-site/working-site.model';
import { WorkingSiteService } from '../../working-sites/working-site/working-site.service';
import { Location } from '../company-location/company-location.model';
import { CompanyLocationService } from '../company-location/company-location.service';
import { CompanyVacancy } from './company-vacancy.model';
import { CompanyVacancyService } from './company-vacancy.service';

@Component({
  selector: 'app-company-vacancy',
  templateUrl: './company-vacancy.component.html',
  styleUrls: ['./company-vacancy.component.css']
})
export class CompanyVacancyComponent implements OnInit {

  vacancys: CompanyVacancy[];
  showFormEdit: boolean;
  id: number;
  showDiv: boolean;
  status: boolean;
  showFormAdd: boolean;
  showBtnEdit: boolean;
  showBtnAdd: boolean;
  vacancy: CompanyVacancy;
  vacancyAddForm: FormGroup;

  action = new Subject();

  vacancyEditForm: FormGroup;

  formattedAmountStart;
  salaryStart = '';

  formattedAmountEnd;
  salaryEnd = '';

  jobTypes: JobType[];
  jobs = [];

  public jobTypeID: string;
  public selectedJobType: string;

  careerLevels: CareerLevel[];
  careers = [];

  public careerLevelID: string;
  public selectedCareerLevel: string;

  workingSites: WorkingSite[];
  sites = [];

  public workingSiteID: string;
  public selectedWorkingSite: string;

  specializations: Specialization[];
  specials = [];

  public specializationID: string;
  public selectedSpecialization: string;

  public showSalary: string;
  public selectedShowSalary: string;

  public shwSalary: boolean;

  locations: Location[];
  locs = [];

  public locationID: string;
  public selectedlocation: string;

  public oJob: string;
  public selectedOpenJob: string;

  public workType: string;
  public selectedWorkType: string;

  public processType: string;
  public selectedProcessType: string;

  public priorityJob: string;
  public selectedPriorityJob: string;

  formattedDate: Date;
  closedDates: string;


  constructor(
    private fb: FormBuilder,
    private jobTypeService: JobTypeService,
    private careerLevelService: CareerLevelService,
    private specializationService: SpecializationService,
    private workingSiteService: WorkingSiteService,
    private currencyPipe: CurrencyPipe,
    private vacancyService: CompanyVacancyService,
    private locationService: CompanyLocationService
  ) {
    this.sVAddForm();
    this.sVEditForm();
  }

  salaryShow = [
    {
      id: 1,
      text: 'Yes',
    },
    {
      id: 0,
      text: 'No'
    }
  ];

  openJobs = [
    {
      id: 1,
      text: 'Yes',
    },
    {
      id: 0,
      text: 'No'
    }
  ];

  workTypes = [
    {
      id: 'Work From Home',
      text: 'Work From Home',
    },
    {
      id: 'Work From Office',
      text: 'Work From Office'
    }
  ];

  processTypes = [
    {
      id: 'Offline',
      text: 'Offline',
    },
    {
      id: 'Online',
      text: 'Online'
    }
  ];

  priorityJobs = [
    {
      id: 1,
      text: 'Urgent'
    },
    {
      id: 2,
      text: 'Now Hiring'
    }
  ];


  sVAddForm() {
    this.vacancyAddForm = this.fb.group({
        id: [{ value: '', disabled: true }],
        responbility: [''],
        name: [''],
        salaryStart: [''],
        salaryEnd: [''],
        jobTypeID: [''],
        careerLevelID: [''],
        specializationID: [''],
        requirement: [''],
        description: [''],
        closedDate: ['', Validators.required]
    });
  }

  sVEditForm() {
    this.vacancyEditForm = this.fb.group({
        id: [{ value: '', disabled: true }],
        responbility: [''],
        name: [''],
        salaryStart: [''],
        salaryEnd: [''],
        jobTypeID: [''],
        careerLevelID: [''],
        specializationID: [''],
        requirement: [''],
        description: [''],
        closedDate: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.getAllVacancy();

    this.showFormEdit = false;
    this.showDiv = true;
    this.status = false;
    this.showFormAdd = false;
    this.showBtnEdit = true;
    this.showBtnAdd = true;
  }

  batal() {
    this.getAllVacancy();
    this.showFormEdit = false;
    this.showDiv = true;
    this.status = false;
    this.showFormAdd = false;
    this.showBtnEdit = true;
    this.showBtnAdd = true;
  }

  getAllVacancy() {
    this.vacancyService.getAllCompanyVacancy()
        .subscribe(
            (res: HttpResponse<CompanyVacancy[]>) => {
                console.log('res.body', res.body);
                this.successLoadUser(res.body);
            }
        );
  }

  private successLoadUser(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.vacancys = data.contents;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.vacancys.length; i++) {
      const tamp = this.currencyPipe.transform(this.vacancys[i].salaryStart);
      const tamps1 = tamp.toString().replace(/\./g, '|').replace(/\,/g, '.').replace(/\|/g, ',').replace(/\$/g, '');
      this.vacancys[i].startSalary = tamps1;

      const tamp2 = this.currencyPipe.transform(this.vacancys[i].salaryEnd);
      const tamps2 = tamp2.toString().replace(/\./g, '|').replace(/\,/g, '.').replace(/\|/g, ',').replace(/\$/g, '');
      this.vacancys[i].endSalary = tamps2;

    }
  }

  add() {
    this.jobTypeService.getAllJobType().subscribe(
      (res: HttpResponse<JobType[]>) => {
          // tslint:disable-next-line:no-string-literal
          const jobType1 = res.body['contents'];
          this.jobs = [];
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < jobType1.length; i++) {
            // console.log('skill name: ', skills1[i].name);
            // this.skills.push({value: skills1[i].id, label: skills1[i].name});
            this.jobs.push({id: jobType1[i].id, text: jobType1[i].name});
          }
      }
    );

    this.careerLevelService.getAllCareerLevel().subscribe(
      (res: HttpResponse<CareerLevel[]>) => {
          // tslint:disable-next-line:no-string-literal
          const careerLevel1 = res.body['contents'];
          this.careers = [];
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < careerLevel1.length; i++) {
            // console.log('skill name: ', skills1[i].name);
            // this.skills.push({value: skills1[i].id, label: skills1[i].name});
            this.careers.push({id: careerLevel1[i].id, text: careerLevel1[i].name});
          }
      }
    );

    this.specializationService.getAllSpecialization().subscribe(
      (res: HttpResponse<Specialization[]>) => {
          // tslint:disable-next-line:no-string-literal
          const special1 = res.body['contents'];
          this.specials = [];
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < special1.length; i++) {
            // console.log('skill name: ', skills1[i].name);
            // this.skills.push({value: skills1[i].id, label: skills1[i].name});
            this.specials.push({id: special1[i].id, text: special1[i].name});
          }
      }
    );

    this.workingSiteService.getAllWorkingSite().subscribe(
      (res: HttpResponse<WorkingSite[]>) => {
          // tslint:disable-next-line:no-string-literal
          const sites1 = res.body['contents'];
          this.sites = [];
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < sites1.length; i++) {
            // console.log('skill name: ', skills1[i].name);
            // this.skills.push({value: skills1[i].id, label: skills1[i].name});
            this.sites.push({id: sites1[i].id, text: sites1[i].name});
          }
      }
    );

    this.locationService.getAllLocation().subscribe(
      (res: HttpResponse<Location[]>) => {
          // tslint:disable-next-line:no-string-literal
          const locs1 = res.body['contents'];
          this.locs = [];
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < locs1.length; i++) {
            // console.log('skill name: ', skills1[i].name);
            // this.skills.push({value: skills1[i].id, label: skills1[i].name});
            this.locs.push({id: locs1[i].id, text: locs1[i].city});
          }
      }
    );

    this.salaryShow = this.salaryShow;

    this.formattedAmountEnd = 0;
    this.formattedAmountStart = 0;

    this.showFormAdd = true;
    this.showBtnEdit = false;
    this.showBtnAdd = false;

    this.openJobs = this.openJobs;
    this.priorityJobs = this.priorityJobs;
    this.workTypes = this.workTypes;
    this.processTypes = this.processTypes;


    this.vacancy = new CompanyVacancy();
    this.vacancyAddForm.reset();
  }

  getAllJobType() {
    this.jobTypeService.getAllJobType()
        .subscribe(
            (res: HttpResponse<JobType[]>) => {
                console.log('res.body', res.body);
                this.successLoadJobTypes(res.body);
            }
        );
  }

  private successLoadJobTypes(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.jobTypes = data.contents;
  }

  public changedJobType(e: any): void {
    this.selectedJobType = e.value;
  }

  public changedLocation(e: any): void {
    this.selectedlocation = e.value;
  }

  getAllCareer() {
    this.careerLevelService.getAllCareerLevel()
        .subscribe(
            (res: HttpResponse<CareerLevel[]>) => {
                console.log('res.body', res.body);
                this.successLoadCareerLevels(res.body);
            }
        );
  }

  private successLoadCareerLevels(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.careerLevels = data.contents;
  }

  public changedCareerLevel(e: any): void {
    this.selectedCareerLevel = e.value;
  }

  getAllSpecialization() {
    this.specializationService.getAllSpecialization()
        .subscribe(
            (res: HttpResponse<Specialization[]>) => {
                console.log('res.body', res.body);
                this.successLoadSpecialization(res.body);
            }
        );
  }

  private successLoadSpecialization(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.specializations = data.contents;
  }

  public changedSpecialization(e: any): void {
    this.selectedSpecialization = e.value;
  }

  getAllWorkingSite() {
    this.workingSiteService.getAllWorkingSite()
        .subscribe(
            (res: HttpResponse<WorkingSite[]>) => {
                console.log('res.body', res.body);
                this.successLoadWorking(res.body);
            }
        );
  }

  private successLoadWorking(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.workingSites = data.contents;
  }

  public changedWorkingSite(e: any): void {
    this.selectedWorkingSite = e.value;
  }

  public changedShowSalary(e: any): void {
    this.selectedShowSalary = e.value;
  }

  transformAmountStart(element) {
    console.log('element: ', this.formattedAmountStart);

    const tamps1 = this.formattedAmountStart.toString().replace(/\./g, '').replace(/\,/g, '').replace(/\|/g, '').replace(/\$/g, '');
    this.formattedAmountStart = this.currencyPipe.transform(tamps1);
    const tamp = this.formattedAmountStart.toString().replace(/\./g, '|').replace(/\,/g, '.').replace(/\|/g, ',').replace(/\$/g, '');

    this.formattedAmountStart = tamp;
  }

  transformAmountEnd(element) {
    console.log('element: ', this.formattedAmountEnd);

    const tamps1 = this.formattedAmountEnd.toString().replace(/\./g, '').replace(/\,/g, '').replace(/\|/g, '').replace(/\$/g, '');
    this.formattedAmountEnd = this.currencyPipe.transform(tamps1);
    const tamp = this.formattedAmountEnd.toString().replace(/\./g, '|').replace(/\,/g, '.').replace(/\|/g, ',').replace(/\$/g, '');

    this.formattedAmountEnd = tamp;
  }

  save() {
    this.shwSalary = false;
    // this.sSalary = this.vacancyAddForm.get('salary')

    // if (sDate > eDate) {
    //     Swal.fire('Error', 'sampai bekerja harus lebih besar dari mulai bekerja', 'error');
    //     return;
    // }

    const salaryStart = this.formattedAmountStart.slice(0, -2);
    this.vacancy.salaryStart = +salaryStart.toString().replace(/\./g, '').replace(/\,/g, '').replace(/\|/g, '').replace(/\$/g, '');

    const salaryEnd = this.formattedAmountEnd.slice(0, -2);
    this.vacancy.salaryEnd = +salaryEnd.toString().replace(/\./g, '').replace(/\,/g, '').replace(/\|/g, '').replace(/\$/g, '');

    if (+this.selectedShowSalary === 0) {
      this.shwSalary = false;
    } else {
      this.shwSalary = true;
    }

    if (this.vacancy.salaryEnd < this.vacancy.salaryStart) {
      Swal.fire('Failed', 'Salary sampai harus lebih besar dari salary dari', 'error');
      return;
    }

    if (this.selectedOpenJob === '1') {
      this.vacancy.openJob = true;
    } else {
      this.vacancy.openJob = false;
    }


    this.vacancy.name = this.vacancyAddForm.get('name').value;
    this.vacancy.responbility = this.vacancyAddForm.get('responbility').value;
    this.vacancy.jobTypeID = +this.selectedJobType;
    this.vacancy.careerLevelID = +this.selectedCareerLevel;
    this.vacancy.specializationID = +this.selectedSpecialization;
    this.vacancy.workingSiteID = +this.selectedWorkingSite;
    this.vacancy.salaryShow = this.shwSalary;
    this.vacancy.locationID = +this.selectedlocation;
    this.vacancy.requirement = this.vacancyAddForm.get('requirement').value;
    this.vacancy.description = this.vacancyAddForm.get('description').value;
    this.vacancy.workType = this.selectedWorkType;
    this.vacancy.processType = this.selectedProcessType;
    this.vacancy.closedDate = this.closedDates;


    console.log('vacancy: ', this.vacancy);

    this.vacancyService.add(this.vacancy).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
            Swal.fire('Success', 'Vacancy berhasil disimpan', 'success');
            this.action.next('refresh');
            this.getAllVacancy();
            this.showFormEdit = false;
            this.showDiv = true;
            this.status = false;
            this.showFormAdd = false;
            this.showBtnEdit = true;
            this.showBtnAdd = true;
            this.vacancyAddForm.reset();
      } else {
          Swal.fire('Failed', 'Vacancy tidak berhasil disimpan', 'error');
      }
      this.action.next('refresh');
    });
  }

  editForm(id: number) {
    this.salaryShow = this.salaryShow;

    this.jobTypeService.getAllJobType().subscribe(
      (res: HttpResponse<JobType[]>) => {
          // tslint:disable-next-line:no-string-literal
          const jobType1 = res.body['contents'];
          this.jobs = [];
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < jobType1.length; i++) {
            // console.log('skill name: ', skills1[i].name);
            // this.skills.push({value: skills1[i].id, label: skills1[i].name});
            this.jobs.push({id: jobType1[i].id, text: jobType1[i].name});
          }
      }
    );

    this.careerLevelService.getAllCareerLevel().subscribe(
      (res: HttpResponse<CareerLevel[]>) => {
          // tslint:disable-next-line:no-string-literal
          const careerLevel1 = res.body['contents'];
          this.careers = [];
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < careerLevel1.length; i++) {
            // console.log('skill name: ', skills1[i].name);
            // this.skills.push({value: skills1[i].id, label: skills1[i].name});
            this.careers.push({id: careerLevel1[i].id, text: careerLevel1[i].name});
          }
      }
    );

    this.specializationService.getAllSpecialization().subscribe(
      (res: HttpResponse<Specialization[]>) => {
          // tslint:disable-next-line:no-string-literal
          const special1 = res.body['contents'];
          this.specials = [];
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < special1.length; i++) {
            // console.log('skill name: ', skills1[i].name);
            // this.skills.push({value: skills1[i].id, label: skills1[i].name});
            this.specials.push({id: special1[i].id, text: special1[i].name});
          }
      }
    );

    this.workingSiteService.getAllWorkingSite().subscribe(
      (res: HttpResponse<WorkingSite[]>) => {
          // tslint:disable-next-line:no-string-literal
          const sites1 = res.body['contents'];
          this.sites = [];
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < sites1.length; i++) {
            // console.log('skill name: ', skills1[i].name);
            // this.skills.push({value: skills1[i].id, label: skills1[i].name});
            this.sites.push({id: sites1[i].id, text: sites1[i].name});
          }
      }
    );

    this.locationService.getAllLocation().subscribe(
      (res: HttpResponse<Location[]>) => {
          // tslint:disable-next-line:no-string-literal
          const locs1 = res.body['contents'];
          this.locs = [];
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < locs1.length; i++) {
            // console.log('skill name: ', skills1[i].name);
            // this.skills.push({value: skills1[i].id, label: skills1[i].name});
            this.locs.push({id: locs1[i].id, text: locs1[i].city});
          }
      }
    );

    this.showFormEdit = true;
    this.id = id;
    this.showDiv = false;
    this.showFormAdd = false;
    const nId = this.id.toString();
    this.status = !this.status;
    if (this.status) {
      document.getElementById(nId).style.display = 'none';
    } else {
      document.getElementById(nId).style.display = 'block';
    }
    this.showBtnEdit = false;
    this.showBtnAdd = false;
    this.getVacancy(id);
  }

  getVacancy(ids) {
    this.vacancyService.getVacancy({
      id: ids
    })
    .subscribe(
        (res: HttpResponse<CompanyVacancy>) => {
            this.successLoadUserVacancy(res.body);
        }
    );
  }

  private successLoadUserVacancy(data) {
    this.vacancy = data.contents;

    this.selectedJobType = this.vacancy.jobTypeID.toString();
    this.selectedCareerLevel = this.vacancy.careerLevelID.toString();
    this.selectedSpecialization = this.vacancy.specializationID.toString();
    this.selectedWorkingSite = this.vacancy.workingSiteID.toString();
    this.selectedlocation = this.vacancy.locationID.toString();

    this.locationID = this.vacancy.locationID.toString();
    this.jobTypeID = this.vacancy.jobTypeID.toString();
    this.careerLevelID = this.vacancy.careerLevelID.toString();
    this.specializationID = this.vacancy.specializationID.toString();
    this.workingSiteID = this.vacancy.workingSiteID.toString();

    // if (this.vacancy.salaryShow === false) {
    //   this.showSalary = 'No';
    // } else {
    //   this.showSalary = 'Yes';
    // }

    if (this.vacancy.salaryShow === false) {
      this.showSalary = '0';
    } else {
      this.showSalary = '1';
    }

    const tamp = this.currencyPipe.transform(this.vacancy.salaryStart);
    const tamps1 = tamp.toString().replace(/\./g, '|').replace(/\,/g, '.').replace(/\|/g, ',').replace(/\$/g, '');
    this.vacancy.startSalary = tamps1;

    const tamp2 = this.currencyPipe.transform(this.vacancy.salaryEnd);
    const tamps2 = tamp2.toString().replace(/\./g, '|').replace(/\,/g, '.').replace(/\|/g, ',').replace(/\$/g, '');
    this.vacancy.endSalary = tamps2;

    this.formattedAmountStart = this.vacancy.startSalary;
    this.formattedAmountEnd = this.vacancy.endSalary;


    this.vacancyEditForm.get('responbility').setValue(this.vacancy.responbility);
    this.vacancyEditForm.get('name').setValue(this.vacancy.name);
    this.vacancyEditForm.get('requirement').setValue(this.vacancy.requirement);
    this.vacancyEditForm.get('description').setValue(this.vacancy.description);

    this.selectedProcessType = this.vacancy.processType;
    this.selectedWorkType = this.vacancy.workType;
    // this.selectedPriorityJob = this.vacancy.priorityJob;

    if (this.vacancy.openJob === true) {
      this.selectedOpenJob = '1';
    } else {
      this.selectedOpenJob = '0';
    }

    this.salaryShow = this.salaryShow;
    this.workTypes = this.workTypes;
    this.openJobs = this.openJobs;
    this.processTypes = this.processTypes;

    if (this.vacancy.closedDate !== '') {
      this.formattedDate = new Date(this.vacancy.closedDate);
      this.closedDates = this.formattedDate.toISOString().split('T')[0];
    }
  }

  update(ids) {
    const salaryStart = this.formattedAmountStart.slice(0, -2);
    this.vacancy.salaryStart = +salaryStart.toString().replace(/\./g, '').replace(/\,/g, '').replace(/\|/g, '').replace(/\$/g, '');

    const salaryEnd = this.formattedAmountEnd.slice(0, -2);
    this.vacancy.salaryEnd = +salaryEnd.toString().replace(/\./g, '').replace(/\,/g, '').replace(/\|/g, '').replace(/\$/g, '');

    console.log('salary show: ', this.selectedShowSalary);

    if (+this.selectedShowSalary === 0) {
      this.shwSalary = false;
    } else {
      this.shwSalary = true;
    }

    if (this.vacancy.salaryEnd < this.vacancy.salaryStart) {
      Swal.fire('Failed', 'Salary sampai harus lebih besar dari salary dari', 'error');
      return;
    }

    console.log('shw salary: ', this.shwSalary);

    if (this.selectedOpenJob === '1') {
      this.vacancy.openJob = true;
    } else {
      this.vacancy.openJob = false;
    }

    this.vacancy.name = this.vacancyEditForm.get('name').value;
    this.vacancy.responbility = this.vacancyEditForm.get('responbility').value;
    this.vacancy.jobTypeID = +this.selectedJobType;
    this.vacancy.careerLevelID = +this.selectedCareerLevel;
    this.vacancy.specializationID = +this.selectedSpecialization;
    this.vacancy.workingSiteID = +this.selectedWorkingSite;
    this.vacancy.salaryShow = this.shwSalary;
    this.vacancy.id = ids;
    this.vacancy.locationID = +this.selectedlocation;
    this.vacancy.requirement = this.vacancyEditForm.get('requirement').value;
    this.vacancy.description = this.vacancyEditForm.get('description').value;
    this.vacancy.workType = this.selectedWorkType;
    this.vacancy.processType = this.selectedProcessType;
    this.vacancy.closedDate = this.closedDates;

    console.log('vacancy: ', this.vacancy);

    this.vacancyService.update(this.vacancy).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
            Swal.fire('Success', 'Vacancy berhasil disimpan', 'success');
            this.action.next('refresh');
            this.getAllVacancy();
            this.showFormEdit = false;
            this.showDiv = true;
            this.status = false;
            this.showFormAdd = false;
            this.showBtnEdit = true;
            this.showBtnAdd = true;
            this.vacancyEditForm.reset();
      } else {
          Swal.fire('Failed', 'Vacancy tidak berhasil disimpan', 'error');
      }
      this.action.next('refresh');
    });
  }

  getAllLocation() {
    this.locationService.getAllLocation()
        .subscribe(
            (res: HttpResponse<Location[]>) => {
                console.log('res.body', res.body);
                this.successLoadUserLocation(res.body);
            }
        );
  }

  private successLoadUserLocation(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.locations = data.contents;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.locations.length; i++) {
      this.locs.push({id: this.locations[i].id, text: this.locations[i].city});
    }
  }

  public changedOpenJob(e: any): void {
    this.selectedOpenJob = e.value;
  }

  public changedWorkType(e: any): void {
    this.selectedWorkType = e.value;
  }

  public changedPriorityJob(e: any): void {
    this.selectedPriorityJob = e.value;
  }

  public changedProcessType(e: any): void {
    this.selectedProcessType = e.value;
  }

}
