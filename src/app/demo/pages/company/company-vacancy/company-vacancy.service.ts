import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { CompanyVacancy } from './company-vacancy.model';

@Injectable({
  providedIn: 'root'
})
export class CompanyVacancyService {

 // public subject = new Subject<any>();

 private dataSource = new BehaviorSubject<any>(new CompanyVacancy());
 data = this.dataSource.asObservable();
 // data = this.subject.asObservable();
 SERVER: string;
 constructor(private http: HttpClient,
             private configService: AppConfigService) {
       this.SERVER = this.configService.getSavedServerPath();
   }

 updatedDataSelection(data: CompanyVacancy) {
     this.dataSource.next(data);
 }

 getAllCompanyVacancy(): Observable<HttpResponse<CompanyVacancy[]>> {
     let newResourceUrl = null;
     let result = null;
     const search = {};

     newResourceUrl = this.SERVER + `/api/company/vacancy/all`;

     result = this.http.post<CompanyVacancy[]>(newResourceUrl, search, { observe: 'response' })
         .pipe(
             tap(
                 // console.log('sdad')
                 results => console.log('raw', results)
             )
         );
     // console.log(result);
     return result;
 }

 add(companyVacancy: CompanyVacancy): Observable<HttpResponse<CompanyVacancy>> {
   const copy = Object.assign({}, companyVacancy);

   copy.salaryEnd = +copy.salaryEnd;
   copy.salaryStart = +copy.salaryStart;
   copy.companyUserID = +copy.companyUserID;
   copy.id = +copy.id;

   const newResourceUrl = this.SERVER + `/api/company/vacancy/`;

   return this.http.post<CompanyVacancy>(newResourceUrl, copy, { observe: 'response'})
       .pipe(map((res: HttpResponse<CompanyVacancy>) => Object.assign({}, res)));
 }

 getCompanyVacancy(req ?: any): Observable<HttpResponse<CompanyVacancy>> {
   let newResourceUrl = null;
   let result = null;
   let id = null;
   const search = {};

   Object.keys(req).forEach((key) => {
     if (key === 'id') {
         id = req[key];
     }
  });

   newResourceUrl = this.SERVER + `/api/company/vacancy/${id}`;

   result = this.http.post<CompanyVacancy[]>(newResourceUrl, search, { observe: 'response' })
       .pipe(
           tap(
               // console.log('sdad')
               results => console.log('raw', results)
           )
       );
   // console.log(result);
   return result;
 }

 update(companyVacancy: CompanyVacancy): Observable<HttpResponse<CompanyVacancy>> {
   const copy = Object.assign({}, companyVacancy);

   copy.salaryEnd = +copy.salaryEnd;
   copy.salaryStart = +copy.salaryStart;
   copy.id = + copy.id;

   const newResourceUrl = this.SERVER + `/api/company/vacancy/`;

   return this.http.put<CompanyVacancy>(newResourceUrl, copy, { observe: 'response' })
       .pipe(map((res: HttpResponse<CompanyVacancy>) => Object.assign({}, res)));
 }

 getVacancy(req ?: any): Observable<HttpResponse<CompanyVacancy>> {
    let newResourceUrl = null;
    let result = null;
    let id = null;
    const search = {};

    Object.keys(req).forEach((key) => {
      if (key === 'id') {
          id = req[key];
      }
  });

    newResourceUrl = this.SERVER + `/api/company/vacancy/${id}`;

    result = this.http.post<CompanyVacancy>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

}
