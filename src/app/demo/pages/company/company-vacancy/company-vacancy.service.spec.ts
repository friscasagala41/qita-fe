/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CompanyVacancyService } from './company-vacancy.service';

describe('Service: CompanyVacancy', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CompanyVacancyService]
    });
  });

  it('should ...', inject([CompanyVacancyService], (service: CompanyVacancyService) => {
    expect(service).toBeTruthy();
  }));
});
