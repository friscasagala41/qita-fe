import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';

import { CompanyVacancyRoutingModule } from './company-vacancy-routing.module';
import { CompanyVacancyComponent } from './company-vacancy.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { SelectModule } from 'ng-uikit-pro-standard';
import { NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import { Select2Module } from 'ng2-select2';


@NgModule({
  declarations: [CompanyVacancyComponent],
  imports: [
    CommonModule,
    CompanyVacancyRoutingModule,
    SharedModule,
    SelectModule,
    NgbProgressbarModule,
    Select2Module
  ],
  providers: [CurrencyPipe]
})
export class CompanyVacancyModule { }
