import { CareerLevel } from '../../career-levels/career-level/career-level.model';
import { JobType } from '../../job-types/job-type/job-type.model';
import { Specialization } from '../../specializations/specialization/specialization.model';
import { WorkingSite } from '../../working-sites/working-site/working-site.model';
import { Location } from '../company-location/company-location.model';
import { CompanyProfile } from '../company-profile/company-profile.model';

export class CompanyVacancy {
    constructor(
        public id?: number,
        public name?: string,
        public responbility?: string,
        public salaryStart?: number,
        public salaryEnd?: number,
        public companyUserID?: number,
        public jobTypeID?: number,
        public careerLevelID?: number,
        public specializationID?: number,
        public jobType?: JobType,
        public careerLevel?: CareerLevel,
        public specialization?: Specialization,
        public workingSiteID?: number,
        public workingSite?: WorkingSite,
        public salaryShow?: boolean,
        public errCode?: string,
        public errDesc?: string,
        public startSalary?: string,
        public endSalary?: string,
        public company?: CompanyProfile,
        public createdAt?: string,
        public locationID?: number,
        public companyLocation?: Location,
        public requirement?: string,
        public description?: string,
        public workType?: string,
        public openJob?: boolean,
        public closedDate?: string,
        public processType?: string
    ) {}
}


export class JobList {
    constructor(
        public id?: number,
        public name?: string,
        public showSalary?: boolean,
        public salaryStart?: number,
        public salaryEnd?: number,
        public companyUserID?: number,
        public jobLevel?: string,
        public companyName?: string,
        public careerLevel?: string,
        public specialization?: string,
        public location?: string,
        public closedDate?: string,
        public processType?: string,
        public openJob?: boolean,
        public workType?: string
    ) {}
}
