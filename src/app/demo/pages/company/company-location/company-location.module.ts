import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyLocationRoutingModule } from './company-location-routing.module';
import { CompanyLocationComponent } from './company-location.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import {NgbDropdownModule, NgbModalModule, NgbModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [CompanyLocationComponent],
  imports: [
    CommonModule,
    CompanyLocationRoutingModule,
    SharedModule,
    NgbDropdownModule,
    NgbModalModule,
    NgbPaginationModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class CompanyLocationModule { }
