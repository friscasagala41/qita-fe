import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyLocationModalComponent } from './company-location-modal.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CompanyLocationModalComponent]
})
export class CompanyLocationModalModule { }
