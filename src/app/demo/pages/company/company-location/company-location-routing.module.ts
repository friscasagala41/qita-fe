import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyLocationComponent } from './company-location.component';


const routes: Routes = [
  {
    path: '',
    component: CompanyLocationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyLocationRoutingModule { }
