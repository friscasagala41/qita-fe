/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CompanyLocationService } from './company-location.service';

describe('Service: CompanyLocation', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CompanyLocationService]
    });
  });

  it('should ...', inject([CompanyLocationService], (service: CompanyLocationService) => {
    expect(service).toBeTruthy();
  }));
});
