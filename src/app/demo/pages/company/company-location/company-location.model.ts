export class Location {
    constructor(
        public id?: number,
        public city?: string,
        public country?: string,
        public long?: string,
        public lat?: string,
        public errCode?: string,
        public errDesc?: string
    ) {}
}
