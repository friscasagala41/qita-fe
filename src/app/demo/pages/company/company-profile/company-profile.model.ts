import { BusinessUnit } from '../../business-units/business-unit/business-unit.model';
import { ZipCode } from '../../user/profile/profile.model';

export class CompanyProfile {
    constructor(
        public id?: number,
        public name?: string,
        public website?: string,
        public shortDescription?: string,
        public address?: string,
        public companySize?: string,
        public industry?: string,
        public userID?: number,
        public avatar?: string,
        public errCode?: string,
        public errDesc?: string,
        public kodePosID?: number,
        public kodePos?: ZipCode,
        public businessUnit?: BusinessUnit
    ) {}
}
