import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import Swal from 'sweetalert2';
import { BusinessUnit } from '../../business-units/business-unit/business-unit.model';
import { BusinessUnitService } from '../../business-units/business-unit/business-unit.service';
import { ZipCode } from '../../user/profile/profile.model';
import { ProfileService } from '../../user/profile/profile.service';
import { CompanyProfile } from './company-profile.model';
import { CompanyProfileService } from './company-profile.service';

@Component({
  selector: 'app-company-profile',
  templateUrl: './company-profile.component.html',
  styleUrls: ['./company-profile.component.css']
})
export class CompanyProfileComponent implements OnInit {

  showForm: boolean;
  cvEnglish: File;
  cvBahasa: File;
  avatar: File;
  profile: CompanyProfile;

  profileForm: FormData;
  sprofileForm: FormGroup;
  action = new Subject();

  subscription: Subscription;

  pos1: any;
  cities = [];
  provinces = [];
  villages = [];
  districts = [];
  socialInterests = [];

  socialMedias = [];

  selectedDistrict = '';
  selectedCity = '';
  selectedVillage = '';

  selectedNumberOfEmployee = '';

  numberOfEmployee = '';

  numberOfEmployees = [
    {
      id: '1-6',
      text: '1-6'
    },
    {
      id: '<250',
      text: '<250'
    },
    {
      id: '<500',
      text: '<500'
    },
    {
      id: '<1,000',
      text: '<1,000'
    },
    {
      id: '1,001 or more',
      text: '1,001 or more'
    }
  ];

  mBusiness: any;
  businessUnits = [];
  selectedIndustry = '';
  businessUnit = '';

  constructor(
    private fb: FormBuilder,
    private companyProfileService: CompanyProfileService,
    private router: Router,
    private profileService: ProfileService,
    private businessService: BusinessUnitService
  ) {
    this.sForm();
  }

  sForm() {
    this.sprofileForm = this.fb.group({
        id: [{ value: '', disabled: true }],
        names: ['', Validators.required],
        shortDescription: ['', Validators.required],
        address: ['', Validators.required],
        industry: ['', Validators.required],
        companySize: ['', Validators.required],
        avatar: [''],
        website: ['', Validators.required],
        kodePos: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.getUser();
    this.selectedIndustry = this.profile.industry;
    this.showForm = false;
  }

  getUser() {
    this.companyProfileService.getUserByID()
        .subscribe(
            (res: HttpResponse<CompanyProfile>) => {
                console.log('res.body', res.body);
                this.successLoadUser(res.body);
            }
        );
  }

  private successLoadUser(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.profile = data.contents;
  }

  edit(data: CompanyProfile) {
    this.profile = data;
    this.showForm = true;
    this.selectedNumberOfEmployee = this.profile.companySize;
    this.selectedIndustry = this.profile.industry;
    this.numberOfEmployee = this.selectedNumberOfEmployee;
    this.numberOfEmployees = this.numberOfEmployees;
    if (this.profile.companySize !== '') {
      this.sprofileForm.get('companySize').setValue(this.profile.companySize);
    }
    this.getAllBusinessUnit();
  }

  getAllBusinessUnit() {
    this.businessService.getAllBusinessUnit()
        .subscribe(
            (res: HttpResponse<BusinessUnit[]>) => {
                console.log('res.body', res.body);
                this.successLoadUserBusinessUnit(res.body);
            }
        );
  }

  private successLoadUserBusinessUnit(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.mBusiness = data.contents;
    this.businessUnits = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.mBusiness.length; i++) {
      // console.log('skill name: ', skills1[i].name);
      this.businessUnits.push({id: this.mBusiness[i].id, text: this.mBusiness[i].name});
      if (this.profile.industry !== '' && this.mBusiness[i].id === this.profile.industry) {
        this.selectedIndustry = String(this.profile.industry);
      }
    }
  }

  batal() {
    this.showForm = false;
    this.getUser();
  }

  processAvatar(event) {
    this.avatar = event.target.files[0];
    console.log('avatar: ', this.avatar);
  }

  save() {
    this.profile.name = this.sprofileForm.get('names').value;
    this.profile.shortDescription = this.sprofileForm.get('shortDescription').value;
    this.profile.address = this.sprofileForm.get('address').value;
    this.profile.website = this.sprofileForm.get('website').value;
    this.profile.industry = this.sprofileForm.get('industry').value;
    this.profile.kodePosID = +this.selectedDistrict;
    this.profile.companySize = this.selectedNumberOfEmployee;

    console.log('this profile: ', this.profile);
    this.companyProfileService.add(this.profile).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
            if (this.avatar != null) {
              if (this.avatar) {
                  this.uploadFileAvatar(result.body.id);
                  this.getUser();
              }
            }
            this.showForm = false;
            this.getUser();
            this.sprofileForm.reset();
            Swal.fire('Success', 'Company profile saved', 'success');
            this.action.next('refresh');
      } else {
          Swal.fire('Failed', 'Company profile not saved', 'error');
          return;
      }
      this.action.next('refresh');
    });
  }

  editForm() {
    console.log('update ==> ', this.profile);
    this.profile.companySize = this.selectedNumberOfEmployee;
    this.profile.industry = String(this.selectedIndustry);
    this.companyProfileService.update(this.profile).subscribe(result => {
        if (result.body.errCode === '00') {
          if (this.avatar != null) {
            if (this.avatar) {
                this.uploadFileAvatar(this.profile.id);
                this.getUser();
            }
          }
          this.showForm = false;
          this.getUser();
          this.sprofileForm.reset();
          Swal.fire('Success', 'Company profile saved', 'success');
          this.action.next('refresh');
        } else {
          Swal.fire('Failed', 'Company profile not saved', 'error');
          return;
        }
    });
  }

  uploadFileAvatar(id: number) {
    const formData = new FormData();
    formData.append('files', this.avatar);
    formData.append('type', 'avatar');
    formData.append('id', String(id));

    console.log('formData: ', formData);

    this.companyProfileService.upload(formData)
        .subscribe(res  => {
          // console.log('res: ', res);
          if (res.body.errCode !== '00') {
            Swal.fire('Failed', 'Avatar not saved', 'error');
          }
          this.getUser();
        });
  }

  // selectZipCodeEvent(item) {
  //   console.log('iteme: ', item);
  //   this.profile.kodePosID = item.kodePos;
  //   this.profile.kodePos = item;
  // }

  onChangeZipCode(events) {
    // const val = this.sprofileForm.get('kodePos').value;
    console.log('val: , ', events);
    const val = events;

    if (val !== '') {
      this.profileService.searchZipCode(val).subscribe(
            (res: HttpResponse<ZipCode[]>) => {
              this.successLoadUserKodePos(res.body);
            }
          );
    }
  }

  public changedCity(e: any): void {
    this.selectedDistrict = e.value;
    this.profileService.searchDistrict(this.selectedDistrict).subscribe(
      (res: HttpResponse<ZipCode[]>) => {
        this.successLoadUserDistrict(res.body);
      }
    );
    this.profile.kodePosID = +this.selectedDistrict;
  }

  private successLoadUserDistrict(data) {
    this.pos1 = data.data;
    // tslint:disable-next-line:prefer-for-of
    this.districts = [];
    this.cities = [];
    this.provinces = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.pos1.length; i++) {
      // console.log('skill name: ', skills1[i].name);
      this.districts.push({id: this.pos1[i].id, text: this.pos1[i].kecamatan});
      this.cities.push({id: this.pos1[i].id, text: this.pos1[i].kabupaten});
      this.provinces.push({id: this.pos1[i].id, text: this.pos1[i].provinsi});
    }
  }

  private successLoadUserKodePos(data) {
    this.pos1 = data.data;
    // tslint:disable-next-line:prefer-for-of
    this.cities = [];
    this.villages = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.pos1.length; i++) {
      this.villages.push({id: this.pos1[i].id, text: this.pos1[i].kelurahan});
      this.cities.push({id: this.pos1[i].id, text: this.pos1[i].kabupaten});

      // tslint:disable-next-line:curly
      if (this.profile.kodePos.kelurahan !== '' && this.pos1[i].id === this.profile.kodePosID) {
        this.selectedVillage = String(this.profile.kodePosID);
        this.selectedDistrict = String(this.profile.kodePosID);
        this.selectedCity = String(this.profile.kodePosID);
      }
    }

    console.log('POS1: ', this.pos1);
    console.log('selectedDistrict: ', this.selectedDistrict, ' villages : ', this.selectedVillage);
  }

  public changedNumberOfEmployees(e: any): void {
    this.selectedNumberOfEmployee = e.value;
    if (this.selectedNumberOfEmployee !== '') {
      this.sprofileForm.get('companySize').setValue(this.selectedNumberOfEmployee);
    }
  }

  public changedIndustry(e: any): void {
    console.log('e: ', !e.value);
    this.selectedIndustry = e.value;
    if (this.selectedIndustry !== '') {
      this.sprofileForm.get('industry').setValue(this.selectedIndustry);
    }
    console.log('industry: ', this.selectedIndustry);
  }

}
