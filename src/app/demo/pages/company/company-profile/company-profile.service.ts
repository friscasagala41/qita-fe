import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { CompanyProfile } from './company-profile.model';

@Injectable({
  providedIn: 'root'
})
export class CompanyProfileService {

  // public subject = new Subject<any>();

  private dataSource = new BehaviorSubject<any>(new CompanyProfile());
  data = this.dataSource.asObservable();
  // data = this.subject.asObservable();
  SERVER: string;
  constructor(private http: HttpClient,
              private configService: AppConfigService) {
        this.SERVER = this.configService.getSavedServerPath();
    }

  updatedDataSelection(data: CompanyProfile) {
      this.dataSource.next(data);
  }

  getUserByID(): Observable<HttpResponse<CompanyProfile>> {
      let newResourceUrl = null;
      let result = null;
      const search = {};

      newResourceUrl = this.SERVER + `/api/user/company/detail`;

      result = this.http.post<CompanyProfile>(newResourceUrl, search, { observe: 'response' })
          .pipe(
              tap(
                  // console.log('sdad')
                  results => console.log('raw', results)
              )
          );
      // console.log(result);
      return result;
  }

  upload(logo: FormData): Observable<HttpResponse<CompanyProfile>> {
    const newResourceUrl = this.SERVER + `/api/user/company/upload`;

    return this.http.post<CompanyProfile>(newResourceUrl, logo, { observe: 'response'})
        .pipe(map((res: HttpResponse<CompanyProfile>) => Object.assign({}, res)));
  }

  add(profile: CompanyProfile): Observable<HttpResponse<CompanyProfile>> {
    const copy = Object.assign({}, profile);

    copy.userID = +copy.userID;
    copy.id = + copy.id;

    const newResourceUrl = this.SERVER + `/api/user/company/`;

    return this.http.post<CompanyProfile>(newResourceUrl, copy, { observe: 'response'})
        .pipe(map((res: HttpResponse<CompanyProfile>) => Object.assign({}, res)));
  }

  update(profile: CompanyProfile): Observable<HttpResponse<CompanyProfile>> {
    const copy = Object.assign({}, profile);

    copy.userID = +copy.userID;
    copy.id = + copy.id;

    const newResourceUrl = this.SERVER + `/api/user/company/`;

    return this.http.put<CompanyProfile>(newResourceUrl, copy, { observe: 'response' })
        .pipe(map((res: HttpResponse<CompanyProfile>) => Object.assign({}, res)));
  }

}
