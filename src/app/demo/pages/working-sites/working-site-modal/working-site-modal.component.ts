import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MDBModalRef, MDBModalService, ToastService } from 'ng-uikit-pro-standard';
import { Subject, Subscription } from 'rxjs';
import Swal from 'sweetalert2';
import { WorkingSite } from '../working-site/working-site.model';
import { WorkingSiteService } from '../working-site/working-site.service';

@Component({
  selector: 'app-working-site-modal',
  templateUrl: './working-site-modal.component.html',
  styleUrls: ['./working-site-modal.component.scss']
})
export class WorkingSiteModalComponent implements OnInit {
  objedit: any;
  action = new Subject();
  workingSite: WorkingSite;
  messages;
  subscription: Subscription;
  dataFromGrid: MDBModalRef;
  nama: string;
  uneditable: boolean;
  workingSiteForm: FormGroup;

  constructor(
    private modalService: MDBModalService,
    public modalRef: MDBModalRef,
    public workingSiteService: WorkingSiteService,
    private toastrService: ToastService,
    private fb: FormBuilder
  ) {
    this.rForm();
  }

  rForm() {
    this.workingSiteForm = this.fb.group({
      id: [{value: '', disabled: true}],
      name: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.subscription = this.workingSiteService.data.subscribe(data => {
      console.log('data behavierrrr', data);
      this.workingSite = data;
    });
    console.log('on init modal ');
    console.log('user modal ', this.objedit);
  }

  closeForm() {
    this.action.next('refresh');
    this.modalService._hideModal(1);
  }

  save() {
    this.workingSite.name = this.workingSiteForm.get('name').value;
    console.log('save ==> ', this.workingSite);
    this.workingSiteService.add(this.workingSite).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
        if (result.body.errDesc !== 'SUCCESS..') {
          // const options = { progressBar: true, extendedTimeOut: 3000, timeOut: 5000 };
          // this.toastrService.error(result.body.errDesc, 'New Password', options);
          // alert('Password ' + );
          Swal.fire('Error', result.body.errDesc, 'error');
        } else {
          // this.toastrService.success('Save success !', 'save');
          Swal.fire('Success', 'Working site saved', 'success');
        }
        this.action.next('refresh');
        this.closeForm();
      } else {
        alert('error');
      }
    });
  }

  edit() {
    console.log('update ==> ', this.workingSite);
    this.workingSiteService.update(this.workingSite).subscribe(result => {
      console.log(result);
      // if (result.body.errCode !== '00') {
      if (result.body.errCode !== '00') {
        // const options = { progressBar: true, extendedTimeOut: 3000, timeOut: 5000 };
        // this.toastrService.error(result.body.errDesc, 'New Password', options);
        Swal.fire('Error', result.body.errDesc, 'error');
        // alert('Password ' + );
      } else {
        // this.toastrService.success('Update success !');
        Swal.fire('Success', 'Working site saved', 'success');
      }
      this.action.next('refresh');
      this.closeForm();
      // } else {
      //   alert('error');
      // }
    });
  }

}
