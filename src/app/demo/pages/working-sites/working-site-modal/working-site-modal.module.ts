import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkingSiteModalComponent } from './working-site-modal.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [WorkingSiteModalComponent]
})
export class WorkingSiteModalModule { }
