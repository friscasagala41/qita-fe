import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { WorkingSite } from './working-site.model';

@Injectable({
  providedIn: 'root'
})
export class WorkingSiteService {

  // public subject = new Subject<any>();

  private dataSource = new BehaviorSubject<any>(new WorkingSite());
  data = this.dataSource.asObservable();
  // data = this.subject.asObservable();
  SERVER: string;
  constructor(private http: HttpClient,
              private configService: AppConfigService) {
        this.SERVER = this.configService.getSavedServerPath();
    }

  updatedDataSelection(data: WorkingSite) {
      this.dataSource.next(data);
  }

  filter(req?: any): Observable<HttpResponse<WorkingSite[]>> {
    let pageNumber = null;
    let pageCount = null;
    let newResourceUrl = null;
    let result = null;
    const search = {
        name: ''
    };

    Object.keys(req).forEach((key) => {
        if (key === 'page') {
            pageNumber = req[key];
        }
        if (key === 'count') {
            pageCount = req[key];
        }
        if (key === 'name') {
            search.name = req[key];
        }
    });

    newResourceUrl = this.SERVER + `/api/working-site/filter/page/${pageNumber}/count/${pageCount}`;

    result = this.http.post<WorkingSite[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  getAllWorkingSite(): Observable<HttpResponse<WorkingSite[]>> {
      let newResourceUrl = null;
      let result = null;
      const search = {};

      newResourceUrl = this.SERVER + `/api/working-site/all`;

      result = this.http.post<WorkingSite[]>(newResourceUrl, search, { observe: 'response' })
          .pipe(
              tap(
                  // console.log('sdad')
                  results => console.log('raw', results)
              )
          );
      // console.log(result);
      return result;
  }

  add(workingSite: WorkingSite): Observable<HttpResponse<WorkingSite>> {
    const copy = Object.assign({}, workingSite);

    copy.id = +copy.id;

    const newResourceUrl = this.SERVER + `/api/working-site/`;

    return this.http.post<WorkingSite>(newResourceUrl, copy, { observe: 'response'})
        .pipe(map((res: HttpResponse<WorkingSite>) => Object.assign({}, res)));
  }

  getWorkingSite(req ?: any): Observable<HttpResponse<WorkingSite>> {
    let newResourceUrl = null;
    let result = null;
    let id = null;
    const search = {};

    Object.keys(req).forEach((key) => {
      if (key === 'id') {
          id = req[key];
      }
  });

    newResourceUrl = this.SERVER + `/api/working-site/${id}`;

    result = this.http.post<WorkingSite[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  update(workingSite: WorkingSite): Observable<HttpResponse<WorkingSite>> {
    const copy = Object.assign({}, workingSite);

    copy.id = + copy.id;

    const newResourceUrl = this.SERVER + `/api/working-site/`;

    return this.http.put<WorkingSite>(newResourceUrl, copy, { observe: 'response' })
        .pipe(map((res: HttpResponse<WorkingSite>) => Object.assign({}, res)));
  }
}
