import { HttpResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MDBModalRef, MDBModalService } from 'ng-uikit-pro-standard';
import { Subject } from 'rxjs';
import { TOTAL_RECORD_PER_PAGE } from 'src/app/theme/shared/constant/base.constant';
import { WorkingSiteModalComponent } from '../working-site-modal/working-site-modal.component';
import { WorkingSite } from './working-site.model';
import { WorkingSiteService } from './working-site.service';

@Component({
  selector: 'app-working-site',
  templateUrl: './working-site.component.html',
  styleUrls: ['./working-site.component.scss']
})
export class WorkingSiteComponent implements OnInit, OnDestroy {
  searchName;
  workingSites: WorkingSite[];

  maxTotalpage = 1;
  curPage: number;
  totalItems: any;
  itemsPerPage: any;
  previousPage: any;
  // total = [];
  nameCareer = '';
  description: string;
  workingSite: WorkingSite = new WorkingSite();
  workingSiteForm: FormGroup;
  action = new Subject();
  closeResult: string;
  modalRef: MDBModalRef;

  constructor(
    private workingSiteService: WorkingSiteService,
    private fb: FormBuilder,
    private modalService: MDBModalService,
  ) { }

  ngOnInit() {
    this.curPage = 1;
    this.filter(this.curPage);
  }

  rForm() {
    this.workingSiteForm = this.fb.group({
      name: ['']
    });
  }


  filter(curpage) {
    console.log('searc ', this.searchName);
    this.workingSiteService.filter({
        page: curpage,
        count: 10,
        name: this.searchName,
    })
        .subscribe(
            (res: HttpResponse<WorkingSite[]>) => {
                console.log('res.body', res.body);
                this.successLoadUser(res.body);

            }
        );
  }

  private successLoadUser(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.workingSites = data.contents;
    this.maxTotalpage = Math.ceil(data.totalRow / TOTAL_RECORD_PER_PAGE);
    this.totalItems = data.totalRow;
    this.itemsPerPage = TOTAL_RECORD_PER_PAGE;
  }

  ngOnDestroy() {
    // this.userService.clearMessages();
  }

  loadPage(curPage) {
      if (this.curPage !== this.previousPage) {
        this.previousPage = this.curPage;
        this.filter(this.curPage);
        console.log('filter', this.filter);
      }
  }

  open(status: string, data: WorkingSite) {

    if (status === 'Add') {
        data = new WorkingSite();
        data.id = 0;
        data.name = '';
    }
    console.log('isi dataaa ', data);

    this.workingSiteService.updatedDataSelection(data);

    const modalOptions = {
        backdrop: true,
        keyboard: true,
        focus: true,
        show: false,
        ignoreBackdropClick: false,
        class: 'modal-lg',
        containerClass: 'right',
        animated: true,
        data: {
            objedit: data,
            uneditable: false
        }
    };

    if (status === 'detail') {
        modalOptions.data.uneditable = true;
    }

    // console.log()
    this.modalRef = this.modalService.show(WorkingSiteModalComponent, modalOptions);
    this.modalRef.content.action.subscribe(
        (result: any) => {
            console.log(result);
            if (result === 'refresh') {
                this.curPage = 1;
                this.filter(this.curPage);
            }
        });
  }

}
