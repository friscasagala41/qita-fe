export class WorkingSite {
    constructor(
        public id?: number,
        public name?: string,
        public errCode?: string,
        public errDesc?: string
    ) {}
}
