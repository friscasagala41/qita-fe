import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkingSiteComponent } from './working-site.component';


const routes: Routes = [
  {
    path: '',
    component: WorkingSiteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkingSiteRoutingModule { }
