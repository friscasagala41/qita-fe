/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { WorkingSiteService } from './working-site.service';

describe('Service: WorkingSite', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkingSiteService]
    });
  });

  it('should ...', inject([WorkingSiteService], (service: WorkingSiteService) => {
    expect(service).toBeTruthy();
  }));
});
