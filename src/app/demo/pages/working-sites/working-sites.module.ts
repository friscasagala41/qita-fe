import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkingSitesRoutingModule } from './working-sites-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    WorkingSitesRoutingModule
  ]
})
export class WorkingSitesModule { }
