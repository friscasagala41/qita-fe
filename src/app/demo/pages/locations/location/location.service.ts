import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { Location } from './location.model';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

 // public subject = new Subject<any>();

 private dataSource = new BehaviorSubject<any>(new Location());
 data = this.dataSource.asObservable();
 // data = this.subject.asObservable();
 SERVER: string;
 constructor(private http: HttpClient,
             private configService: AppConfigService) {
       this.SERVER = this.configService.getSavedServerPath();
   }

 updatedDataSelection(data: Location) {
     this.dataSource.next(data);
 }

 filter(req?: any): Observable<HttpResponse<Location[]>> {
   let pageNumber = null;
   let pageCount = null;
   let newResourceUrl = null;
   let result = null;
   const search = {
       name: ''
   };

   Object.keys(req).forEach((key) => {
       if (key === 'page') {
           pageNumber = req[key];
       }
       if (key === 'count') {
           pageCount = req[key];
       }
       if (key === 'name') {
           search.name = req[key];
       }
   });

   newResourceUrl = this.SERVER + `/api/location/filter/page/${pageNumber}/count/${pageCount}`;

   result = this.http.post<Location[]>(newResourceUrl, search, { observe: 'response' })
       .pipe(
           tap(
               // console.log('sdad')
               results => console.log('raw', results)
           )
       );
   // console.log(result);
   return result;
 }

 getAllLocation(): Observable<HttpResponse<Location[]>> {
     let newResourceUrl = null;
     let result = null;
     const search = {};

     newResourceUrl = this.SERVER + `/api/location/all`;

     result = this.http.post<Location[]>(newResourceUrl, search, { observe: 'response' })
         .pipe(
             tap(
                 // console.log('sdad')
                 results => console.log('raw', results)
             )
         );
     // console.log(result);
     return result;
 }

 add(location: Location): Observable<HttpResponse<Location>> {
   const copy = Object.assign({}, location);

   copy.id = +copy.id;

   const newResourceUrl = this.SERVER + `/api/location/`;

   return this.http.post<Location>(newResourceUrl, copy, { observe: 'response'})
       .pipe(map((res: HttpResponse<Location>) => Object.assign({}, res)));
 }

 getLocation(req ?: any): Observable<HttpResponse<Location>> {
   let newResourceUrl = null;
   let result = null;
   let id = null;
   const search = {};

   Object.keys(req).forEach((key) => {
     if (key === 'id') {
         id = req[key];
     }
  });

   newResourceUrl = this.SERVER + `/api/location/${id}`;

   result = this.http.post<Location[]>(newResourceUrl, search, { observe: 'response' })
       .pipe(
           tap(
               // console.log('sdad')
               results => console.log('raw', results)
           )
       );
   // console.log(result);
   return result;
 }

 update(location: Location): Observable<HttpResponse<Location>> {
   const copy = Object.assign({}, location);

   copy.id = + copy.id;

   const newResourceUrl = this.SERVER + `/api/location/`;

   return this.http.put<Location>(newResourceUrl, copy, { observe: 'response' })
       .pipe(map((res: HttpResponse<Location>) => Object.assign({}, res)));
 }

}
