import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MDBModalRef, MDBModalService, ToastService } from 'ng-uikit-pro-standard';
import { Subject, Subscription } from 'rxjs';
import Swal from 'sweetalert2';
import { Location } from '../location/location.model';
import { LocationService } from '../location/location.service';

@Component({
  selector: 'app-location-modal',
  templateUrl: './location-modal.component.html',
  styleUrls: ['./location-modal.component.scss']
})
export class LocationModalComponent implements OnInit {
  objedit: any;
  action = new Subject();
  location: Location;
  messages;
  subscription: Subscription;
  dataFromGrid: MDBModalRef;
  nama: string;
  uneditable: boolean;
  locationForm: FormGroup;

  constructor(
    private modalService: MDBModalService,
    public modalRef: MDBModalRef,
    public locationService: LocationService,
    private toastrService: ToastService,
    private fb: FormBuilder
  ) {
    this.rForm();
  }

  rForm() {
    this.locationForm = this.fb.group({
      id: [{value: '', disabled: true}],
      city: ['', Validators.required],
      country: ['', Validators.required],
      long: [''],
      lat: ['']
    });
  }

  ngOnInit() {
    this.subscription = this.locationService.data.subscribe(data => {
      console.log('data behavierrrr', data);
      this.location = data;
    });
    console.log('on init modal ');
    console.log('user modal ', this.objedit);
  }

  closeForm() {
    this.action.next('refresh');
    this.modalService._hideModal(1);
  }

  save() {
    this.location.city = this.locationForm.get('city').value;
    this.location.country = this.locationForm.get('country').value;
    this.location.long = this.locationForm.get('long').value;
    this.location.lat = this.locationForm.get('lat').value;

    console.log('save ==> ', this.location);
    this.locationService.add(this.location).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
        if (result.body.errDesc !== 'SUCCESS..') {
          // const options = { progressBar: true, extendedTimeOut: 3000, timeOut: 5000 };
          // this.toastrService.error(result.body.errDesc, 'New Password', options);
          // alert('Password ' + );
          Swal.fire('Error', result.body.errDesc, 'error');
        } else {
          // this.toastrService.success('Save success !', 'save');
          Swal.fire('Success', 'Location saved', 'success');
        }
        this.action.next('refresh');
        this.closeForm();
      } else {
        alert('error');
      }
    });
  }

  edit() {
    console.log('update ==> ', this.location);
    this.locationService.update(this.location).subscribe(result => {
      console.log(result);
      // if (result.body.errCode !== '00') {
      if (result.body.errCode !== '00') {
        // const options = { progressBar: true, extendedTimeOut: 3000, timeOut: 5000 };
        // this.toastrService.error(result.body.errDesc, 'New Password', options);
        Swal.fire('Error', result.body.errDesc, 'error');
        // alert('Password ' + );
      } else {
        // this.toastrService.success('Update success !');
        Swal.fire('Success', 'Location saved', 'success');
      }
      this.action.next('refresh');
      this.closeForm();
      // } else {
      //   alert('error');
      // }
    });
  }
}
