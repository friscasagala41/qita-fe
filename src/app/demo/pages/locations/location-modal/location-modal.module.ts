import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocationModalComponent } from './location-modal.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [LocationModalComponent]
})
export class LocationModalModule { }
