import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { Role } from './role.model';
import { map, tap } from 'rxjs/operators';
import { Observable, Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  // public subject = new Subject<any>();

  private dataSource = new BehaviorSubject<any>(new Role());
  data = this.dataSource.asObservable();
  // data = this.subject.asObservable();
  SERVER: string;
  constructor(private http: HttpClient,
              private configService: AppConfigService) {
        this.SERVER = this.configService.getSavedServerPath();
    }

  updatedDataSelection(data: Role) {
      this.dataSource.next(data);
  }

  filter(req?: any): Observable<HttpResponse<Role[]>> {
      let pageNumber = null;
      let pageCount = null;
      let newResourceUrl = null;
      let result = null;
      const search = {
          name: ''
      };

      Object.keys(req).forEach((key) => {
          if (key === 'page') {
              pageNumber = req[key];
          }
          if (key === 'count') {
              pageCount = req[key];
          }
          if (key === 'name') {
              search.name = req[key];
          }
      });

      newResourceUrl = this.SERVER + `/api/role/filter/page/${pageNumber}/count/${pageCount}`;

      result = this.http.post<Role[]>(newResourceUrl, search, { observe: 'response' })
          .pipe(
              tap(
                  // console.log('sdad')
                  results => console.log('raw', results)
              )
          );
      // console.log(result);
      return result;
  }

  add(role: Role): Observable<HttpResponse<Role>> {
      const copy = Object.assign({}, role);
      const newResourceUrl = this.SERVER + `/api/role/`;

      return this.http.post<Role>(newResourceUrl, copy, { observe: 'response' })
          .pipe(map((res: HttpResponse<Role>) => Object.assign({}, res)));
  }

  update(role: Role): Observable<HttpResponse<Role>> {
      const copy = Object.assign({}, role);
      const newResourceUrl = this.SERVER + `/api/role/`;

      return this.http.put<Role>(newResourceUrl, copy, { observe: 'response' })
          .pipe(map((res: HttpResponse<Role>) => Object.assign({}, res)));
  }

}
