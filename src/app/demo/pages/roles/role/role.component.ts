import { HttpResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';
import { Role } from './role.model';
import { RoleService } from './role.service';
import { Subject } from 'rxjs';
import { MDBModalService, MDBModalRef } from 'ng-uikit-pro-standard';
import { RoleModalComponent } from '../role-modal/role-modal.component';
import { TOTAL_RECORD_PER_PAGE } from 'src/app/theme/shared/constant/base.constant';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit, OnDestroy {

  roles: Role[];
  searchName;
  maxTotalpage = 1;
  curPage: number;
  totalItems: any;
  itemsPerPage: any;
  previousPage: any;
  // total = [];
  nameRole = '';
  description: string;
  role: Role = new Role();
  roleForm: FormGroup;
  action = new Subject();
  closeResult: string;
  modalRef: MDBModalRef;

  constructor(
      private roleService: RoleService,
      private fb: FormBuilder,
      private modalService: MDBModalService,
  ) {
    this.rForm();
  }

  rForm() {
    this.roleForm = this.fb.group({
      nameRole: [''],
      description: ['']
    });
  }

  ngOnInit() {
      this.curPage = 1;
      this.filter(this.curPage);
      this.role.name = '';

  }

  open(status: string, data: Role) {

    if (status === 'Add') {
        data = new Role();
        data.id = 0;
        data.name = '';
        data.description = '';
    }
    console.log('isi dataaa ', data);

    this.roleService.updatedDataSelection(data);

    const modalOptions = {
        backdrop: true,
        keyboard: true,
        focus: true,
        show: false,
        ignoreBackdropClick: false,
        class: 'modal-lg',
        containerClass: 'right',
        animated: true,
        data: {
            objedit: data,
            uneditable: false
        }
    };

    if (status === 'detail') {
        modalOptions.data.uneditable = true;
    }

    // console.log()
    this.modalRef = this.modalService.show(RoleModalComponent, modalOptions);
    this.modalRef.content.action.subscribe(
        (result: any) => {
            console.log(result);
            if (result === 'refresh') {
                this.curPage = 1;
                this.filter(this.curPage);
            }
        });
  }

  filter(curpage) {
      console.log('searc ', this.searchName);
      this.roleService.filter({
          page: curpage,
          count: 10,
          name: this.searchName,
      })
          .subscribe(
              (res: HttpResponse<Role[]>) => {
                  console.log('res.body', res.body);
                  this.successLoadUser(res.body);

              }
          );
  }

  save() {
      // tslint:disable-next-line:no-string-literal
      this.role.name = this.roleForm.get('nameRole').value;
      this.role.description = this.roleForm.get('description').value;
      this.roleService.add(this.role).subscribe(result => {
        console.log(result);
        if (result.body.errCode === '00') {
          if (result.body.errDesc !== 'SUCCESS..') {
            // const options = { progressBar: true, extendedTimeOut: 3000, timeOut: 5000 };
            // this.toastrService.error(result.body.errDesc, 'New Password', options);
            // alert('Password ' + );
            Swal.fire('Error', result.body.errDesc, 'error');
          } else {
            // this.toastrService.success('Save success !', 'save');
            Swal.fire('Success', result.body.errDesc, 'success');
          }
          window.location.reload();
        } else {
          alert('error');
        }
      });
  }

  private successLoadUser(data) {
      console.log('succesloaduser ', data);
      if (data.totalRow < 0) {
          return;
      }
      console.log(data);
      this.roles = data.contents;
      this.maxTotalpage = Math.ceil(data.totalRow / TOTAL_RECORD_PER_PAGE);
      this.totalItems = data.totalRow;
      this.itemsPerPage = TOTAL_RECORD_PER_PAGE;
      // this.total = []
      // for (let i = 0; (i + 1) <= this.maxTotalpage; i++) {
      //     this.total[i] = i + 1;
      // }
  }

  ngOnDestroy() {
      // this.userService.clearMessages();
  }

  loadPage(curPage) {
      if (this.curPage !== this.previousPage) {
        this.previousPage = this.curPage;
        this.filter(this.curPage);
        console.log('filter', this.filter);
      }
  }

}
