import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RolesRoutingModule } from './roles-routing.module';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { RoleModalComponent } from './role-modal/role-modal.component';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RolesRoutingModule,
    SharedModule
  ],
  entryComponents: [
  ]
})
export class RolesModule { }
