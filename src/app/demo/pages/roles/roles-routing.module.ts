import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./role/role.module').then(module => module.RoleModule)
      },
      {
        path: 'role-modal',
        loadChildren: () => import('./role-modal/role-modal.module').then(module => module.RoleModalModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RolesRoutingModule { }
