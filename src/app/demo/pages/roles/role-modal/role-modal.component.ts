import { Component, OnInit } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { MDBModalRef, MDBModalService, ToastService } from 'ng-uikit-pro-standard';
import { Role } from '../role/role.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RoleService } from '../role/role.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-role-modal',
  templateUrl: './role-modal.component.html',
  styleUrls: ['./role-modal.component.scss']
})
export class RoleModalComponent implements OnInit {
  objedit: any;
  action = new Subject();
  role: Role;
  messages;
  subscription: Subscription;
  dataFromGrid: MDBModalRef;
  nama: string;
  uneditable: boolean;
  roleForm: FormGroup;

  constructor(
    private modalService: MDBModalService,
    public modalRef: MDBModalRef,
    public roleService: RoleService,
    private toastrService: ToastService,
    private fb: FormBuilder
  ) {
    this.rForm();
  }

  rForm() {
    this.roleForm = this.fb.group({
      id: [{value: '', disabled: true}],
      name: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.subscription = this.roleService.data.subscribe(data => {
      console.log('data behavierrrr', data);
      this.role = data;
    });
    console.log('on init modal ');
    console.log('user modal ', this.objedit);
  }

  closeForm() {
    this.action.next('refresh');
    this.modalService._hideModal(1);
  }

  save() {
    this.role.name = this.roleForm.get('name').value;
    this.role.description = this.roleForm.get('description').value;
    console.log('save ==> ', this.role);
    this.roleService.add(this.role).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
        if (result.body.errDesc !== 'SUCCESS..') {
          // const options = { progressBar: true, extendedTimeOut: 3000, timeOut: 5000 };
          // this.toastrService.error(result.body.errDesc, 'New Password', options);
          // alert('Password ' + );
          Swal.fire('Error', result.body.errDesc, 'error');
        } else {
          // this.toastrService.success('Save success !', 'save');
          Swal.fire('Success', 'Role saved', 'success');
        }
        this.action.next('refresh');
        this.closeForm();
      } else {
        alert('error');
      }
    });
  }

  edit() {
    console.log('update ==> ', this.role);
    this.roleService.update(this.role).subscribe(result => {
      console.log(result);
      // if (result.body.errCode !== '00') {
      if (result.body.errCode !== '00') {
        // const options = { progressBar: true, extendedTimeOut: 3000, timeOut: 5000 };
        // this.toastrService.error(result.body.errDesc, 'New Password', options);
        Swal.fire('Error', result.body.errDesc, 'error');
        // alert('Password ' + );
      } else {
        // this.toastrService.success('Update success !');
        Swal.fire('Success', 'Role saved', 'success');
      }
      this.action.next('refresh');
      this.closeForm();
      // } else {
      //   alert('error');
      // }
    });
  }

}
