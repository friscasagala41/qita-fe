import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleModalComponent } from './role-modal.component';


const routes: Routes = [

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoleModalRoutingModule { }
