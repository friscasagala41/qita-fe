import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoleModalRoutingModule } from './role-modal-routing.module';
import { RoleModalComponent } from './role-modal.component';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RoleModalRoutingModule
  ]
})
export class RoleModalModule { }
