import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CvListsRoutingModule } from './cv-lists-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CvListsRoutingModule
  ]
})
export class CvListsModule { }
