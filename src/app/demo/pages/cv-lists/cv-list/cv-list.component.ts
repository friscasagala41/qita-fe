import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { TOTAL_RECORD_PER_PAGE } from 'src/app/theme/shared/constant/base.constant';
import Swal from 'sweetalert2';
import { Profile, ProfileDownload } from '../../user/profile/profile.model';
import { ProfileService } from '../../user/profile/profile.service';
import { CvListService } from './cv-list.service';

@Component({
  selector: 'app-cv-list',
  templateUrl: './cv-list.component.html',
  styleUrls: ['./cv-list.component.scss']
})
export class CvListComponent implements OnInit {

  seekers: Profile[];
  maxTotalpage = 1;
  curPage: number;
  totalItems: any;
  itemsPerPage: any;
  previousPage: any;

  action = new Subject();

  constructor(
    private cvListService: CvListService,
    public router: Router,
    private profileService: ProfileService
  ) {
  }

  ngOnInit() {
    this.curPage = 1;
    this.getAllSeeker(this.curPage);
  }

  getAllSeeker(curpage) {
    this.cvListService.getAllSeeker({
      page: curpage,
      count: 10
  })
        .subscribe(
            (res: HttpResponse<Profile[]>) => {
                console.log('res.body', res.body);
                this.successLoadAllSeeker(res.body);
            }
        );
  }

  private successLoadAllSeeker(data) {
    console.log('successLoadAllSeeker ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.seekers = data.contents;
    this.maxTotalpage = Math.ceil(data.totalRow / TOTAL_RECORD_PER_PAGE);
    this.totalItems = data.totalRow;
    this.itemsPerPage = TOTAL_RECORD_PER_PAGE;
  }

  loadPage(curPage) {
    if (this.curPage !== this.previousPage) {
      this.previousPage = this.curPage;
      this.getAllSeeker(this.curPage);
    }
  }

  download(id: number, files: string, fileName: string) {
    // console.log('profile: ', data);

    const pDownloads = new ProfileDownload();

    pDownloads.id = id;
    pDownloads.typeFile = files;

    console.log('profile:', pDownloads);
    if (files === 'cv_bahasa') {
      fileName = 'cv_bahasa_' + fileName;
    }

    if (files === 'cv_english') {
      fileName = 'cv_english_' + fileName;
    }

    this.profileService.downloadCV(pDownloads).subscribe(dataBlob => {
        console.log('data blob ==> ', dataBlob);
        if (dataBlob.type === 'text/plain') {
            Swal.fire('File Not Found', 'Failed Download', 'error');
            return;
        }
        const newBlob = new Blob([dataBlob], { type: 'application/text/pdf' });

        const link = document.createElement('a');
        link.href = URL.createObjectURL(newBlob);
        link.download = fileName;
        document.body.append(link);
        link.click();
        link.remove();
        window.addEventListener('focus', e => URL.revokeObjectURL(link.href), {once: true});

    });
  }

  delete(ids) {
    this.cvListService.deleteCV({id: ids}).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
        Swal.fire('Success', 'CV has deleted.', 'success');
        this.action.next('refresh');
        window.location.reload();
      } else {
        Swal.fire('Error', 'CV has failed delete.', 'error');
        return;
      }
    });
  }

}
