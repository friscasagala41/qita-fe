import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { Profile } from '../../user/profile/profile.model';

@Injectable({
  providedIn: 'root'
})
export class CvListService {
  // public subject = new Subject<any>();

  private dataSource = new BehaviorSubject<any>(new Profile());
  data = this.dataSource.asObservable();
  // data = this.subject.asObservable();
  SERVER: string;
  constructor(private http: HttpClient,
              private configService: AppConfigService) {
        this.SERVER = this.configService.getSavedServerPath();
    }

  updatedDataSelection(data: Profile) {
      this.dataSource.next(data);
  }

  getAllSeeker(req?: any): Observable<HttpResponse<Profile[]>> {
    let pageNumber = null;
    let pageCount = null;
    let newResourceUrl = null;
    let result = null;
    const search = {};

    Object.keys(req).forEach((key) => {
        if (key === 'page') {
            pageNumber = req[key];
        }
        if (key === 'count') {
            pageCount = req[key];
        }
    });

    newResourceUrl = this.SERVER + `/api/trx-user-vacancy/all/seeker/page/${pageNumber}/count/${pageCount}`;

    result = this.http.post<Profile[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                results => console.log('raw', results)
            )
        );
    return result;
  }

  deleteCV(req?: any): Observable<HttpResponse<Profile>> {
    let ids = null;
    const search = {};

    Object.keys(req).forEach((key) => {
      if (key === 'id') {
          ids = req[key];
      }
    });

    const newResourceUrl = this.SERVER + `/api/trx-user-vacancy/delete/${ids}`;

    return this.http.put<Profile>(newResourceUrl, search, { observe: 'response' })
        .pipe(map((res: HttpResponse<Profile>) => Object.assign({}, res)));
  }

}
