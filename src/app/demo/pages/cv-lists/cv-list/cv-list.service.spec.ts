/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CvListService } from './cv-list.service';

describe('Service: CvList', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CvListService]
    });
  });

  it('should ...', inject([CvListService], (service: CvListService) => {
    expect(service).toBeTruthy();
  }));
});
