import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CvListRoutingModule } from './cv-list-routing.module';
import { CvListComponent } from './cv-list.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [CvListComponent],
  imports: [
    CommonModule,
    CvListRoutingModule,
    SharedModule,
    NgbPaginationModule
  ]
})
export class CvListModule { }
