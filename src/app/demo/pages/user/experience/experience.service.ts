import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { Experience } from './experience.model';

@Injectable({
  providedIn: 'root'
})
export class ExperienceService {

  // public subject = new Subject<any>();

  private dataSource = new BehaviorSubject<any>(new Experience());
  data = this.dataSource.asObservable();
  // data = this.subject.asObservable();
  SERVER: string;
  constructor(private http: HttpClient,
              private configService: AppConfigService) {
        this.SERVER = this.configService.getSavedServerPath();
    }

  updatedDataSelection(data: Experience) {
      this.dataSource.next(data);
  }

  getAllExperience(): Observable<HttpResponse<Experience[]>> {
      let newResourceUrl = null;
      let result = null;
      const search = {};

      newResourceUrl = this.SERVER + `/api/user/experience/all`;

      result = this.http.post<Experience[]>(newResourceUrl, search, { observe: 'response' })
          .pipe(
              tap(
                  // console.log('sdad')
                  results => console.log('raw', results)
              )
          );
      // console.log(result);
      return result;
  }

  add(experience: Experience): Observable<HttpResponse<Experience>> {
    const copy = Object.assign({}, experience);

    copy.userSeekerID = +copy.userSeekerID;
    copy.id = +copy.id;
    copy.salary = +copy.salary;

    const newResourceUrl = this.SERVER + `/api/user/experience/`;

    return this.http.post<Experience>(newResourceUrl, copy, { observe: 'response'})
        .pipe(map((res: HttpResponse<Experience>) => Object.assign({}, res)));
  }

  getExperience(req ?: any): Observable<HttpResponse<Experience>> {
    let newResourceUrl = null;
    let result = null;
    let id = null;
    const search = {};

    Object.keys(req).forEach((key) => {
      if (key === 'id') {
          id = req[key];
      }
  });

    newResourceUrl = this.SERVER + `/api/user/experience/${id}`;

    result = this.http.post<Experience[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  update(experience: Experience): Observable<HttpResponse<Experience>> {
    const copy = Object.assign({}, experience);

    copy.userSeekerID = +copy.userSeekerID;
    copy.id = +copy.id;
    copy.salary = +copy.salary;

    const newResourceUrl = this.SERVER + `/api/user/experience/`;

    return this.http.put<Experience>(newResourceUrl, copy, { observe: 'response' })
        .pipe(map((res: HttpResponse<Experience>) => Object.assign({}, res)));
  }
}
