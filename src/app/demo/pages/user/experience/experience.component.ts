import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2';
import { Experience } from './experience.model';
import { ExperienceService } from './experience.service';
import { Select2OptionData } from 'ng2-select2';
import { CurrencyPipe, getLocaleCurrencySymbol } from '@angular/common';
import { CountProgress } from '../profile/profile.model';
import { ProfileService } from '../profile/profile.service';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.css']
})
export class ExperienceComponent implements OnInit {

  selectedYear: number;
  years = [];
  experiences: Experience[];
  showFormEdit: boolean;
  id: number;
  showDiv: boolean;
  status: boolean;
  showFormAdd: boolean;
  showBtnEdit: boolean;
  showBtnAdd: boolean;
  experience: Experience;
  experienceForm: FormGroup;

  startDate: any;
  endDate: any;

  action = new Subject();

  yearEnd = '';
  selectedOptionYE = { value: '', label: 'Choose Supplier' };

  yearStart = '';
  selectedOptionYS = { value: '', label: 'Choose Supplier' };

  monthEnd = '';
  selectedOptionME = { value: '', label: 'Choose Supplier' };

  monthStart = '';
  selectedOptionMS = { value: '', label: 'Choose Supplier' };

  expEditForm: FormGroup;

  selectedYS = '';
  selectedMS = '';

  selectedYE = '';
  selectedME = '';

  formattedAmount;
  salary = '';

  shwErrorYearStart = false;
  shwErrorMonthStart = false;
  shwErrorYearEnd = false;
  shwErrorMonthEnd = false;

  tamps1 = '';

  constructor(
    private experienceService: ExperienceService,
    private fb: FormBuilder,
    private currencyPipe: CurrencyPipe,
    private profileService: ProfileService
  ) {
    this.sExpForm();
    this.sexpEditForm();
  }

  months = [
    {
      id: '01',
      text: 'January',
    },
    {
      id: '02',
      text: 'February',
    },
    {
      id: '03',
      text: 'March',
    },
    {
      id: '04',
      text: 'April',
    },
    {
      id: '05',
      text: 'May',
    },
    {
      id: '06',
      text: 'June',
    },
    {
      id: '07',
      text: 'July',
    },
    {
      id: '08',
      text: 'August',
    },
    {
      id: '09',
      text: 'September',
    },
    {
      id: '10',
      text: 'October',
    },
    {
      id: '11',
      text: 'November',
    },
    {
      id: '12',
      text: 'December',
    }

  ];

  countProgres: CountProgress;
  tProgress: number;

  ngOnInit() {
    this.tProgress = 0;

    this.getCountUserProgress();

    this.selectedYear = new Date().getFullYear();
    for (let year = this.selectedYear; year >= 1995; year--) {
      this.years.push({id: year.toString(), text: year.toString()});
    }

    this.months = this.months;
    this.getAllExperience();
    this.showFormEdit = false;
    this.showDiv = true;
    this.status = false;
    this.showFormAdd = false;
    this.showBtnEdit = true;
    this.showBtnAdd = true;
  }

  getCountUserProgress() {
    this.profileService.getCountUserProgress()
        .subscribe(
            (res: HttpResponse<CountProgress>) => {
                console.log('res.body', res.body);
                this.successLoadCountUserProgress(res.body);
            }
        );
  }

  private successLoadCountUserProgress(data) {
    console.log('succesloaduser ', data);
    this.countProgres = data.contents;
    this.tProgress = (+this.countProgres / 4) * 100;
    console.log('count progress: ', this.tProgress);
  }

  sExpForm() {
    this.experienceForm = this.fb.group({
        id: [{ value: '', disabled: true }],
        position: ['', Validators.required],
        salary: [''],
        description: ['', Validators.required],
        company: ['', Validators.required],
        monthStart: ['', Validators.required],
        yearStart: ['', Validators.required],
        monthEnd: ['', Validators.required],
        yearEnd: ['', Validators.required]
    });
  }

  sexpEditForm() {
    this.expEditForm = this.fb.group({
        id: [{ value: '', disabled: true }],
        position: ['', Validators.required],
        salary: [''],
        description: ['', Validators.required],
        company: ['', Validators.required],
        monthStart: ['', Validators.required],
        yearStart: ['', Validators.required],
        monthEnd: ['', Validators.required],
        yearEnd: ['', Validators.required]
    });
  }

  add() {
    this.formattedAmount = 0;
    this.selectedYear = new Date().getFullYear();
    for (let year = this.selectedYear; year >= 1995; year--) {
      this.years.push({id: year.toString(), text: year.toString()});
    }

    this.showFormAdd = true;
    this.showBtnEdit = false;
    this.showBtnAdd = false;
    this.experience = new Experience();
    this.yearStart = '';
    this.yearEnd = '';
    this.monthEnd = '';
    this.monthStart = '';
    this.experienceForm.reset();
  }

  save() {
    const tgl = new Date().getUTCDate();

    if (this.selectedYS === '') {
      Swal.fire('Error', 'start date empty', 'error');
      return;
    }

    if (this.selectedMS === '') {
      Swal.fire('Error', 'start date empty', 'error');
      return;
    }

    if (this.selectedYE === '') {
      Swal.fire('Error', 'end date empty', 'error');
      return;
    }

    if (this.selectedME === '') {
      Swal.fire('Error', 'end date empty', 'error');
      return;
    }

    this.startDate = this.selectedYS + '-' + this.selectedMS + '-' + tgl;
    this.endDate = this.selectedYE + '-' + this.selectedME + '-' + tgl;

    const sDate = new Date(this.startDate);
    const eDate = new Date(this.endDate);

    if (sDate > eDate) {
        Swal.fire('Error', 'end date >= start date', 'error');
        return;
    }

    if (this.formattedAmount === 0) {
      this.experience.salary = 0;
    } else {
      const salary = this.formattedAmount.slice(0, -2);
      this.experience.salary = +salary.toString().replace(/\./g, '').replace(/\,/g, '').replace(/\|/g, '').replace(/\$/g, '');
    }

    this.experience.position = this.experienceForm.get('position').value;
    this.experience.company = this.experienceForm.get('company').value;
    this.experience.description = this.experienceForm.get('description').value;
    this.experience.startDate = this.startDate;
    this.experience.endDate = this.endDate;

    console.log('experience: ', this.experience);

    this.experienceService.add(this.experience).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
            Swal.fire('Success', 'Experience saved', 'success');
            this.action.next('refresh');
            this.getAllExperience();
            this.showFormEdit = false;
            this.showDiv = true;
            this.status = false;
            this.showFormAdd = false;
            this.showBtnEdit = true;
            this.showBtnAdd = true;
            this.experienceForm.reset();
      } else {
          Swal.fire('Failed', 'Experience not saved', 'error');
      }
      this.action.next('refresh');
    });
  }

  public changedYearStart(e: any): void {
    this.selectedYS = e.value;
    if (this.selectedYS !== '') {
      this.experienceForm.get('yearStart').setValue(this.selectedYS);
      this.shwErrorYearStart = false;
    }
  }

  public changedMonthStart(e: any): void {
    this.selectedMS = e.value;
    if (this.selectedMS !== '') {
      this.experienceForm.get('monthStart').setValue(this.selectedMS);
      this.shwErrorMonthStart = false;
    }
  }

  public changedYearEnd(e: any): void {
    this.selectedYE = e.value;
    if (this.selectedYE !== '') {
      this.experienceForm.get('yearEnd').setValue(this.selectedYE);
      this.shwErrorYearEnd = false;
    }
  }

  public changedMonthEnd(e: any): void {
    this.selectedME = e.value;
    if (this.selectedME !== '') {
      this.experienceForm.get('monthEnd').setValue(this.selectedME);
      this.shwErrorMonthEnd = false;
    }
  }

  public clickYearStart(event) {
    console.log('year start: ', event);
    if (event.value === undefined) {
      if (this.selectedYS === '') {
        this.shwErrorYearStart = true;
      } else {
        this.shwErrorYearStart = false;
      }
    }
  }

  public clickMonthStart(event) {
    if (event.value === undefined) {
      if (this.selectedMS === '') {
        this.shwErrorMonthStart = true;
      } else {
        this.shwErrorMonthStart = false;
      }
    }
  }

  public clickYearEnd(event) {
    if (event.value === undefined) {
      if (this.selectedYE === '') {
        this.shwErrorYearEnd = true;
      } else {
        this.shwErrorYearEnd = false;
      }
    }
  }

  public clickMonthEnd(event) {
    if (event.value === undefined) {
      if (this.selectedME === '') {
        this.shwErrorMonthEnd = true;
      } else {
        this.shwErrorMonthEnd = false;
      }
    }
  }

  batal() {
    this.getAllExperience();
    this.showFormEdit = false;
    this.showDiv = true;
    this.status = false;
    this.showFormAdd = false;
    this.showBtnEdit = true;
    this.showBtnAdd = true;
  }

  getAllExperience() {
    this.experienceService.getAllExperience()
        .subscribe(
            (res: HttpResponse<Experience[]>) => {
                console.log('res.body', res.body);
                this.successLoadUser(res.body);
            }
        );
  }

  private successLoadUser(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.experiences = data.contents;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.experiences.length; i++) {
      const tamp = this.currencyPipe.transform(this.experiences[i].salary);
      const tamps1 = tamp.toString().replace(/\./g, '|').replace(/\,/g, '.').replace(/\|/g, ',').replace(/\$/g, '');
      this.experiences[i].salarys = tamps1;
    }
  }

  editForm(id: number) {
    this.selectedYear = new Date().getFullYear();
    for (let year = this.selectedYear; year >= 1995; year--) {
      this.years.push({id: year.toString(), text: year.toString()});
    }

    this.showFormEdit = true;
    this.id = id;
    this.showDiv = false;
    this.showFormAdd = false;
    const nId = this.id.toString();
    this.status = !this.status;
    if (this.status) {
      document.getElementById(nId).style.display = 'none';
    } else {
      document.getElementById(nId).style.display = 'block';
    }
    this.showBtnEdit = false;
    this.showBtnAdd = false;
    this.getExperience(id);
  }

  getExperience(ids) {
    this.experienceService.getExperience({
      id: ids
    })
    .subscribe(
        (res: HttpResponse<Experience>) => {
            this.successLoadUserExperience(res.body);
        }
    );
  }

  private successLoadUserExperience(data) {
    this.experience = data.contents;

    const months1 = new Date(this.experience.startDate).getMonth() + 1;
    console.log(months1, this.experience.startDate);
    if (months1.toString().length > 2) {
      this.monthStart = months1.toString();
    } else {
      this.monthStart = '0' + months1.toString();
    }

    const months2 = new Date(this.experience.endDate).getMonth() + 1;
    if (months2.toString().length > 2) {
      this.monthEnd = months2.toString();
    } else {
      this.monthEnd = '0' + months2.toString();
    }

    const year1 = new Date(this.experience.startDate).getFullYear();
    this.yearStart = year1.toString();

    const year2 = new Date(this.experience.endDate).getFullYear();
    this.yearEnd = year2.toString();

    console.log('monthStart: ', this.monthStart);

    if (this.experience.salary !== 0) {
      const tamp = this.currencyPipe.transform(this.experience.salary);
      this.tamps1 = tamp.toString().replace(/\./g, '|').replace(/\,/g, '.').replace(/\|/g, ',').replace(/\$/g, '');
    } else {
      this.tamps1 = String(0);
    }

    this.expEditForm.get('position').setValue(this.experience.position);
    this.expEditForm.get('company').setValue(this.experience.company);
    this.expEditForm.get('description').setValue(this.experience.description);
    this.expEditForm.get('salary').setValue(this.tamps1);
    this.expEditForm.get('yearStart').setValue(this.yearStart);
    this.expEditForm.get('monthStart').setValue(this.monthStart);
    this.expEditForm.get('yearEnd').setValue(this.yearEnd);
    this.expEditForm.get('monthEnd').setValue(this.monthEnd);

    console.log('month: ', this.monthStart);
    console.log('experience', this.experience);
  }

  updateForm(ids) {
    const tgl = new Date().getUTCDate();

    if (this.selectedYS === '') {
      Swal.fire('Error', 'start date empty', 'error');
      return;
    }

    if (this.selectedMS === '') {
      Swal.fire('Error', 'start date empty', 'error');
      return;
    }

    if (this.selectedYE === '') {
      Swal.fire('Error', 'end date empty', 'error');
      return;
    }

    if (this.selectedME === '') {
      Swal.fire('Error', 'end date empty', 'error');
      return;
    }

    console.log('monthstart: ', this.expEditForm.get('monthStart').value);

    this.startDate = this.selectedYS + '-' + this.selectedMS + '-' + tgl;
    this.endDate = this.selectedYE + '-' + this.selectedME + '-' + tgl;

    const sDate = new Date(this.startDate);
    const eDate = new Date(this.endDate);

    if (sDate > eDate) {
        Swal.fire('Error', 'end date >= start date', 'error');
        return;
    }

    if (this.expEditForm.get('salary').value !== '0') {
      const salary = this.expEditForm.get('salary').value.slice(0, -2);
      this.experience.salary = salary.toString().replace(/\./g, '').replace(/\,/g, '').replace(/\|/g, '').replace(/\$/g, '');
    } else {
      this.experience.salary = 0;
    }


    this.experience.position = this.expEditForm.get('position').value;
    this.experience.company = this.expEditForm.get('company').value;
    this.experience.description = this.expEditForm.get('description').value;
    this.experience.startDate = this.startDate;
    this.experience.endDate = this.endDate;
    this.experience.id = ids;

    console.log('experience: ', this.experience);

    this.experienceService.update(this.experience).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
            Swal.fire('Success', 'Experience saved', 'success');
            this.action.next('refresh');
            this.getAllExperience();
            this.showFormEdit = false;
            this.showDiv = true;
            this.status = false;
            this.showFormAdd = false;
            this.showBtnEdit = true;
            this.showBtnAdd = true;
            this.expEditForm.reset();
      } else {
          Swal.fire('Failed', 'Experience not saved', 'error');
      }
      this.action.next('refresh');
    });
  }

  transformAmount(element) {
    console.log('element: ', this.formattedAmount);

    if (this.formattedAmount === 0) {
      this.formattedAmount = 0;
    } else {

      const tamps1 = this.formattedAmount.toString().replace(/\./g, '').replace(/\,/g, '').replace(/\|/g, '').replace(/\$/g, '');
      this.formattedAmount = this.currencyPipe.transform(tamps1);
      const tamp = this.formattedAmount.toString().replace(/\./g, '|').replace(/\,/g, '.').replace(/\|/g, ',').replace(/\$/g, '');

      this.formattedAmount = tamp;
    }
  }


}
