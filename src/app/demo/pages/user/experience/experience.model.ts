export class Experience {
    constructor(
        public id?: number,
        public userSeekerID?: number,
        public position?: string,
        public company?: string,
        public startDate?: string,
        public endDate?: string,
        public description?: string,
        public salary?: number,
        public salarys?: string,
        public errCode?: string,
        public errDesc?: string
    ) {}
}
