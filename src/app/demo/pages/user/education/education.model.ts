export class Education {
    constructor(
        public id?: number,
        public userSeekerID?: number,
        public institution?: string,
        public degree?: string,
        public gradeScore?: string,
        public startDate?: string,
        public endDate?: string,
        public errCode?: string,
        public errDesc?: string,
        public prodi?: string
    ) {}
}
