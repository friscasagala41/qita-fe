import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { Education } from './education.model';

@Injectable({
  providedIn: 'root'
})
export class EducationService {

  // public subject = new Subject<any>();

  private dataSource = new BehaviorSubject<any>(new Education());
  data = this.dataSource.asObservable();
  // data = this.subject.asObservable();
  SERVER: string;
  constructor(private http: HttpClient,
              private configService: AppConfigService) {
        this.SERVER = this.configService.getSavedServerPath();
    }

  updatedDataSelection(data: Education) {
      this.dataSource.next(data);
  }

  getAllEducation(): Observable<HttpResponse<Education[]>> {
      let newResourceUrl = null;
      let result = null;
      const search = {};

      newResourceUrl = this.SERVER + `/api/user/education/all`;

      result = this.http.post<Education[]>(newResourceUrl, search, { observe: 'response' })
          .pipe(
              tap(
                  // console.log('sdad')
                  results => console.log('raw', results)
              )
          );
      // console.log(result);
      return result;
  }

  add(education: Education): Observable<HttpResponse<Education>> {
    const copy = Object.assign({}, education);

    copy.userSeekerID = +copy.userSeekerID;
    copy.id = +copy.id;

    const newResourceUrl = this.SERVER + `/api/user/education/`;

    return this.http.post<Education>(newResourceUrl, copy, { observe: 'response'})
        .pipe(map((res: HttpResponse<Education>) => Object.assign({}, res)));
  }

  getEducation(req ?: any): Observable<HttpResponse<Education>> {
    let newResourceUrl = null;
    let result = null;
    let id = null;
    const search = {};

    Object.keys(req).forEach((key) => {
      if (key === 'id') {
          id = req[key];
      }
  });

    newResourceUrl = this.SERVER + `/api/user/education/${id}`;

    result = this.http.post<Education[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  update(education: Education): Observable<HttpResponse<Education>> {
    const copy = Object.assign({}, education);

    copy.userSeekerID = +copy.userSeekerID;
    copy.id = + copy.id;

    const newResourceUrl = this.SERVER + `/api/user/education/`;

    return this.http.put<Education>(newResourceUrl, copy, { observe: 'response' })
        .pipe(map((res: HttpResponse<Education>) => Object.assign({}, res)));
  }

}
