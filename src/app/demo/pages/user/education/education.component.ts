import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Select2OptionData } from 'ng2-select2';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2';
import { CountProgress } from '../profile/profile.model';
import { ProfileService } from '../profile/profile.service';
import { Education } from './education.model';
import { EducationService } from './education.service';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class EducationComponent implements OnInit {
  showFormEdit: boolean;
  id: number;
  showDiv: boolean;
  status: boolean;
  showFormAdd: boolean;
  showBtnEdit: boolean;
  showBtnAdd: boolean;

  educations: Education[];
  eduAddForm: FormGroup;
  eduEditForm: FormGroup;

  education: Education;

  startDate: any;
  endDate: any;

  action = new Subject();

  yearEnd = '';
  selectedOptionYE = { value: '', label: 'Choose Supplier' };

  yearStart = '';
  selectedOptionYS = { value: '', label: 'Choose Supplier' };

  monthEnd = '';
  selectedOptionME = { value: '', label: 'Choose Supplier' };

  monthStart = '';
  selectedOptionMS = { value: '', label: 'Choose Supplier' };

  expEditForm: FormGroup;

  selectedYear: number;

  years = [];

  selectedYS = '';
  selectedMS = '';

  selectedYE = '';
  selectedME = '';

  selectedDegree = '';
  degree = '';

  countProgres: CountProgress;
  tProgress: number;

  prodies1 = [];
  prodies2 = [];

  shwErrorDegree = false;
  shwErrorFaculty = false;

  selectedFaculty = '';
  prodi = '';

  constructor(
    private eduService: EducationService,
    private fb: FormBuilder,
    private profileService: ProfileService
  ) {
    this.sEduAddForm();
    this.sEduEditForm();
  }

  months = [
    {
      id: '01',
      text: 'January',
    },
    {
      id: '02',
      text: 'February',
    },
    {
      id: '03',
      text: 'March',
    },
    {
      id: '04',
      text: 'April',
    },
    {
      id: '05',
      text: 'May',
    },
    {
      id: '06',
      text: 'June',
    },
    {
      id: '07',
      text: 'July',
    },
    {
      id: '08',
      text: 'August',
    },
    {
      id: '09',
      text: 'September',
    },
    {
      id: '10',
      text: 'October',
    },
    {
      id: '11',
      text: 'November',
    },
    {
      id: '12',
      text: 'December',
    }

  ];

  faculty = [
    {
      id: 'System Information',
      text: 'System Information',
    },
    {
      id: 'Computer Engineer',
      text: 'Computer Engineer',
    }
  ];

  degrees = [
    {
      id: 'SMU/SMK/STM',
      text: 'SMU/SMK/STM'
    },
    {
      id: 'D3 (Diploma)',
      text: 'D3 (Diploma)'
    },
    {
      id: 'Sarjana (S1)',
      text: 'Sarjana (S1)'
    },
    {
      id: 'Magister (S2)',
      text: 'Magister (S2)'
    },
    {
      id: 'Doctor (S3)',
      text: 'Doctor (S3)'
    }
  ];

  sEduAddForm() {
    this.eduAddForm = this.fb.group({
      id: [{ value: '', disabled: true }],
      institution: ['', Validators.required],
      degree: ['', Validators.required],
      gradeScore: ['', Validators.required],
      monthStart: [''],
      yearStart: [''],
      monthEnd: [''],
      yearEnd: [''],
      faculty: ['']
    });
  }

  sEduEditForm() {
    this.eduEditForm = this.fb.group({
      id: [{ value: '', disabled: true }],
      institution: ['', Validators.required],
      degree: ['', Validators.required],
      gradeScore: [''],
      monthStart: [''],
      yearStart: [''],
      monthEnd: [''],
      yearEnd: ['']
    });
  }

  ngOnInit() {
    this.tProgress = 0;

    for (let year = this.selectedYear; year >= 1995; year--) {
      this.years.push({id: year.toString(), text: year.toString()});
    }

    this.showFormEdit = false;
    this.showDiv = true;
    this.status = false;
    this.showFormAdd = false;
    this.showBtnEdit = true;
    this.showBtnAdd = true;
    this.getAllEducation();
    this.getCountUserProgress();
  }

  getCountUserProgress() {
    this.profileService.getCountUserProgress()
        .subscribe(
            (res: HttpResponse<CountProgress>) => {
                console.log('res.body', res.body);
                this.successLoadCountUserProgress(res.body);
            }
        );
  }

  private successLoadCountUserProgress(data) {
    console.log('succesloaduser ', data);
    this.countProgres = data.contents;
    this.tProgress = (+this.countProgres / 4) * 100;
    console.log('count progress: ', this.tProgress);
  }

  getAllEducation() {
    this.eduService.getAllEducation()
        .subscribe(
            (res: HttpResponse<Education[]>) => {
                console.log('res.body', res.body);
                this.successLoadUser(res.body);
            }
        );
  }

  private successLoadUser(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.educations = data.contents;
    console.log(this.educations);
  }

  add() {
    this.selectedYear = new Date().getFullYear();
    for (let year = this.selectedYear; year >= 1995; year--) {
      this.years.push({id: year.toString(), text: year.toString()});
    }
    this.showFormAdd = true;
    this.showBtnEdit = false;
    this.showBtnAdd = false;
    this.education = new Education();
    this.yearStart = '';
    this.yearEnd = '';
    this.monthEnd = '';
    this.monthStart = '';
    this.eduAddForm.reset();
  }

  public changedYearStart(e: any): void {
    this.selectedYS = e.value;
  }

  public changedMonthStart(e: any): void {
    this.selectedMS = e.value;
  }

  public changedYearEnd(e: any): void {
    this.selectedYE = e.value;
  }

  public changedMonthEnd(e: any): void {
    this.selectedME = e.value;
    console.log('selectedME: ', this.selectedME);
  }

  public changedDegree(e: any): void {
    this.selectedDegree = e.value;
    if (this.selectedDegree !== '') {
      this.eduAddForm.get('degree').setValue(this.selectedDegree);
    }
  }

  public clickDegree(event) {
    if (event.value === undefined) {
      this.shwErrorDegree = true;
    }
  }

  public changedFaculty(e: any): void {
    this.selectedFaculty = e.value;
    console.log(this.selectedFaculty);
    if (this.selectedFaculty !== '') {
      this.eduAddForm.get('faculty').setValue(this.selectedFaculty);
    }
  }

  public clickFaculty(event) {
    if (event.value === undefined) {
      this.shwErrorFaculty = true;
    } else {
      this.shwErrorFaculty = false;
    }
  }

  save() {
    const tgl = new Date().getUTCDate();

    if (this.selectedDegree === '') {
      Swal.fire('Error', 'qualification empty', 'error');
      return;
    }

    if (this.selectedYS === '') {
      Swal.fire('Error', 'start date empty', 'error');
      return;
    }

    if (this.selectedMS === '') {
      Swal.fire('Error', 'start date empty', 'error');
      return;
    }

    if (this.selectedYE === '') {
      Swal.fire('Error', 'end date empty', 'error');
      return;
    }

    if (this.selectedME === '') {
      Swal.fire('Error', 'end date empty', 'error');
      return;
    }

    this.startDate = this.selectedYS + '-' + this.selectedMS + '-' + tgl;
    this.endDate = this.selectedYE + '-' + this.selectedME + '-' + tgl;

    const sDate = new Date(this.startDate);
    const eDate = new Date(this.endDate);

    if (sDate > eDate) {
        Swal.fire('Error', 'end date >= start date', 'error');
        return;
    }

    this.education.institution = this.eduAddForm.get('institution').value;
    this.education.degree = this.selectedDegree;
    this.education.gradeScore = this.eduAddForm.get('gradeScore').value;
    this.education.startDate = this.startDate;
    this.education.endDate = this.endDate;
    this.education.prodi = this.selectedFaculty;

    console.log('education: ', this.education);

    if (+this.education.gradeScore > 4) {
      Swal.fire('Error', 'Grade score cannot more than 4');
      return;
    }

    this.eduService.add(this.education).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
            Swal.fire('Success', 'Education saved', 'success');
            this.action.next('refresh');
            this.getAllEducation();
            this.showFormEdit = false;
            this.showDiv = true;
            this.status = false;
            this.showFormAdd = false;
            this.showBtnEdit = true;
            this.showBtnAdd = true;
            this.eduAddForm.reset();
      } else {
          Swal.fire('Failed', 'Education not saved', 'error');
      }
      this.action.next('refresh');
    });
  }

  batal() {
    this.getAllEducation();
    this.showFormEdit = false;
    this.showDiv = true;
    this.status = false;
    this.showFormAdd = false;
    this.showBtnEdit = true;
    this.showBtnAdd = true;
  }

  editForm(id: number) {
    this.showFormEdit = true;
    this.id = id;
    this.showDiv = false;
    this.showFormAdd = false;
    const nId = this.id.toString();
    this.status = !this.status;
    if (this.status) {
      document.getElementById(nId).style.display = 'none';
    } else {
      document.getElementById(nId).style.display = 'block';
    }

    this.showBtnEdit = false;
    this.showBtnAdd = false;

    console.log('education: ' + this.id);

    this.getEducation(id);

    this.selectedYear = new Date().getFullYear();
    for (let year = this.selectedYear; year >= 1995; year--) {
      this.years.push({id: year.toString(), text: year.toString()});
    }

    this.prodi = this.education.prodi;
    this.selectedFaculty = this.education.prodi;
    console.log('education:: ', this.education);
  }

  getEducation(ids) {
    this.eduService.getEducation({
      id: ids
    })
    .subscribe(
        (res: HttpResponse<Education>) => {
            this.successLoadUserEducation(res.body);
        }
    );
  }

  private successLoadUserEducation(data) {
    this.education = data.contents;

    const months1 = new Date(this.education.startDate).getMonth() + 1;
    console.log(months1, this.education.startDate);
    if (months1.toString().length > 2) {
      this.monthStart = months1.toString();
    } else {
      this.monthStart = '0' + months1.toString();
    }

    const months2 = new Date(this.education.endDate).getMonth() + 1;
    if (months2.toString().length > 2) {
      this.monthEnd = months2.toString();
    } else {
      this.monthEnd = '0' + months2.toString();
    }

    const year1 = new Date(this.education.startDate).getFullYear();
    this.yearStart = year1.toString();

    const year2 = new Date(this.education.endDate).getFullYear();
    this.yearEnd = year2.toString();

    this.degree = this.education.degree;
    this.prodi = this.education.prodi;


    this.eduEditForm.get('institution').setValue(this.education.institution);
    this.eduEditForm.get('gradeScore').setValue(this.education.gradeScore);

    this.faculty = this.faculty;
    this.selectedFaculty = this.education.prodi;

    console.log('selectedF', this.selectedFaculty);
  }

  updateForm(ids) {
    const tgl = new Date().getUTCDate();

    this.startDate = this.selectedYS + '-' + this.selectedMS + '-' + tgl;
    this.endDate = this.selectedYE + '-' + this.selectedME + '-' + tgl;

    const sDate = new Date(this.startDate);
    const eDate = new Date(this.endDate);

    if (sDate > eDate) {
      Swal.fire('Error', 'end date >= start date', 'error');
      return;
    }

    if (this.selectedDegree === '') {
      Swal.fire('Error', 'qualification empty', 'error');
      return;
    }

    if (this.selectedYS === '') {
      Swal.fire('Error', 'start date empty', 'error');
      return;
    }

    if (this.selectedMS === '') {
      Swal.fire('Error', 'start date empty', 'error');
      return;
    }

    if (this.selectedYE === '') {
      Swal.fire('Error', 'end date empty', 'error');
      return;
    }

    if (this.selectedME === '') {
      Swal.fire('Error', 'end date empty', 'error');
      return;
    }

    this.education.institution = this.eduEditForm.get('institution').value;
    this.education.gradeScore = this.eduEditForm.get('gradeScore').value;
    this.education.startDate = this.startDate;
    this.education.endDate = this.endDate;
    this.education.degree = this.selectedDegree;
    this.education.id = ids;
    this.education.prodi = this.selectedFaculty;

    console.log('education: ', this.education);

    if (+this.education.gradeScore > 4) {
      Swal.fire('Error', 'Grade score cannot more than 4');
      return;
    }

    this.eduService.update(this.education).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
            Swal.fire('Success', 'Education saved', 'success');
            this.action.next('refresh');
            this.getAllEducation();
            this.showFormEdit = false;
            this.showDiv = true;
            this.status = false;
            this.showFormAdd = false;
            this.showBtnEdit = true;
            this.showBtnAdd = true;
            this.eduEditForm.reset();
      } else {
          Swal.fire('Failed', 'Education not saved', 'error');
      }
      this.action.next('refresh');
    });
  }

}
