import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { CountProgress, ZipCode, Profile, ProfileDownload } from './profile.model';
import { ProfileService } from './profile.service';
import { Subject, Subscription } from 'rxjs';
import { formatDate } from '@angular/common';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { event } from 'jquery';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  showForm: boolean;
  cvEnglish: File;
  cvBahasa: File;
  avatar: File;
  profile: Profile;

  profileForm: FormData;
  sprofileForm: FormGroup;
  action = new Subject();

  subscription: Subscription;

  formattedDate: Date;
  birthDates: string;

  countProgres: CountProgress;
  tProgress: number;

  pDownload: ProfileDownload;

  pos1: any;
  cities = [];
  provinces = [];
  villages = [];
  districts = [];
  socialInterests = [];

  keyword = 'name';
  selectedDistrict = '';
  selectedCity = '';
  selectedVillage = '';

  gender = '';
  shwErrorGender = false;
  shwErrorCvBahasa = false;
  shwErrorAvatar = false;

  kodePosID = '';

  constructor(
    private fb: FormBuilder,
    private profileService: ProfileService,
    private router: Router
  ) {
    this.sForm();
  }


  genders = [
    {
      id: 'Female',
      text: 'Female',
    },
    {
      id: 'Male',
      text: 'Male',
    }
  ];

  sForm() {
    this.sprofileForm = this.fb.group({
        id: [{ value: '', disabled: true }],
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        phoneNumber: ['', Validators.required],
        address: ['', Validators.required],
        cvBahasa: ['', Validators.required],
        cvEnglish: [''],
        avatar: ['', Validators.required],
        birthDate: ['', Validators.required],
        gender: ['', Validators.required],
        activities: [''],
        license: [''],
        kodePos: ['', Validators.required],
        socialInterest: ['']
    });
  }

  ngOnInit() {
    this.tProgress = 0;

    this.getUser();
    this.getCountUserProgress();
    this.showForm = false;
  }

  getCountUserProgress() {
    this.profileService.getCountUserProgress()
        .subscribe(
            (res: HttpResponse<CountProgress>) => {
                console.log('res.body', res.body);
                this.successLoadCountUserProgress(res.body);
            }
        );
  }

  private successLoadCountUserProgress(data) {
    console.log('succesloaduser ', data);
    this.countProgres = data.contents;
    this.tProgress = (+this.countProgres / 4) * 100;
    console.log('count progress: ', this.tProgress);
  }

  getUser() {
    this.profileService.getUserByID()
        .subscribe(
            (res: HttpResponse<Profile>) => {
                console.log('res.body', res.body);
                this.successLoadUser(res.body);
            }
        );
  }

  private successLoadUser(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.profile = data.contents;
    this.selectedDistrict = String(this.profile.kodePosID);
  }

  edit(data: Profile) {
    this.profile = data;

    if (this.profile !== undefined) {
      this.sprofileForm.get('gender').setValue(this.profile.gender);
      this.sprofileForm.get('avatar').setValue(this.profile.avatar);
      this.sprofileForm.get('cvBahasa').setValue(this.profile.cvBahasa);
    }

    if (this.profile.birthDate !== '') {
      this.formattedDate = new Date(this.profile.birthDate);
      this.birthDates = this.formattedDate.toISOString().split('T')[0];
    }

    this.showForm = true;
    console.log('gender: ', this.profile.gender);
    if (this.profile.gender !== '') {
      this.sprofileForm.get('gender').setValue(this.profile.gender);
      this.gender = this.profile.gender;
    }
  }

  // selectZipCodeEvent(item) {
  //   console.log('iteme: ', item);
  //   this.profile.kodePosID = item.kodePos;
  //   this.profile.kodePos = item;
  // }

  onChangeZipCode(events) {
    // const val = this.sprofileForm.get('kodePos').value;
    console.log('val: , ', events);
    const val = events;

    if (val !== '') {
      this.profileService.searchZipCode(val).subscribe(
            (res: HttpResponse<ZipCode[]>) => {
              this.successLoadUserKodePos(res.body);
            }
          );
    }
  }

  public changedCity(e: any): void {
    this.selectedDistrict = e.value;
    this.profileService.searchDistrict(this.selectedDistrict).subscribe(
      (res: HttpResponse<ZipCode[]>) => {
        this.successLoadUserDistrict(res.body);
      }
    );
    this.profile.kodePosID = +this.selectedDistrict;
  }

  private successLoadUserDistrict(data) {
    this.pos1 = data.data;
    // tslint:disable-next-line:prefer-for-of
    this.districts = [];
    this.cities = [];
    this.provinces = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.pos1.length; i++) {
      // console.log('skill name: ', skills1[i].name);
      this.districts.push({id: this.pos1[i].id, text: this.pos1[i].kecamatan});
      this.cities.push({id: this.pos1[i].id, text: this.pos1[i].kabupaten});
      this.provinces.push({id: this.pos1[i].id, text: this.pos1[i].provinsi});
    }
  }

  private successLoadUserKodePos(data) {
    this.pos1 = data.data;
    // tslint:disable-next-line:prefer-for-of
    this.cities = [];
    this.villages = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.pos1.length; i++) {
      this.villages.push({id: this.pos1[i].id, text: this.pos1[i].kelurahan});
      this.cities.push({id: this.pos1[i].id, text: this.pos1[i].kabupaten});

      // tslint:disable-next-line:curly
      if (this.profile.kodePos.kelurahan !== '' && this.pos1[i].id === this.profile.kodePosID) {
        this.selectedVillage = String(this.profile.kodePosID);
        this.selectedDistrict = String(this.profile.kodePosID);
        this.selectedCity = String(this.profile.kodePosID);
      }
    }

    console.log('POS1: ', this.pos1);
    console.log('selectedDistrict: ', this.selectedDistrict, ' villages : ', this.selectedVillage);
  }

  private formatDate(date) {
    const d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    const year = d.getFullYear();
    if (month.length < 2) {
      month = '0' + month;
    }
    if (day.length < 2) {
      day = '0' + day;
    }
    return [year, month, day].join('-');
  }

  batal() {
    this.showForm = false;
    this.getUser();
  }

  processCVBahasa(events) {
    this.cvBahasa = events.target.files[0];

    const str1 = this.cvBahasa.name.split('.');
    if (str1[str1.length - 1] !== 'pdf') {
      Swal.fire('Failed', 'Extension file not pdf', 'error');
      this.sprofileForm.get('cvBahasa').setValue('');
      return;
    }

    const fileSize = this.cvBahasa.size / 1024 / 1024;
    if (fileSize > 5) {
      Swal.fire('Failed', 'File size exceeds 5 MB', 'error');
      this.sprofileForm.get('cvBahasa').setValue('');
      return;
    }
  }

  processCVEnglish(events) {
    this.cvEnglish = events.target.files[0];
    console.log('cvEnglish: ', this.cvEnglish);
  }

  processAvatar(events) {
    this.avatar = events.target.files[0];

    const str1 = this.avatar.name.split('.');
    if (str1[str1.length - 1] !== 'png' && str1[str1.length - 1] !== 'jpeg') {
      Swal.fire('Failed', 'Extension file not png', 'error');
      this.sprofileForm.get('avatar').setValue('');
      return;
    }

    const fileSize = this.avatar.size / 1024 / 1024;
    if (fileSize > 5) {
      Swal.fire('Failed', 'File size exceeds 5 MB', 'error');
      this.sprofileForm.get('avatar').setValue('');
      return;
    }
  }

  save() {
    this.profile.firstName = this.sprofileForm.get('firstName').value;
    this.profile.lastName = this.sprofileForm.get('lastName').value;
    this.profile.birthDate = this.sprofileForm.get('birthDate').value;
    this.profile.address = this.sprofileForm.get('address').value;
    this.profile.phoneNumber = this.sprofileForm.get('phoneNumber').value;
    this.profile.gender = this.sprofileForm.get('gender').value;
    this.profile.socialInterest = this.sprofileForm.get('socialInterest').value;

    console.log('this profile save: ', this.profile);
    this.profileService.add(this.profile).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
            if (this.cvBahasa != null) {
                if (this.cvBahasa) {
                    this.uploadFileCvBahasa(result.body.id);
                    this.getUser();
                    this.getCountUserProgress();
                }
            }

            if (this.cvEnglish != null) {
              if (this.cvEnglish) {
                  this.uploadFileCvEnglish(result.body.id);
                  this.getUser();
                  this.getCountUserProgress();
              }
            }

            if (this.avatar != null) {
              if (this.avatar) {
                  this.uploadFileAvatar(result.body.id);
                  this.getUser();
                  this.getCountUserProgress();
              }
            }
            this.showForm = false;
            this.getUser();
            this.getCountUserProgress();
            Swal.fire('Success', 'Profile saved', 'success');
            this.action.next('refresh');
      } else {
          Swal.fire('Failed', 'Profile not saved', 'error');
          return;
      }
      this.action.next('refresh');
    });
  }

  editForm() {
    console.log('update ==> ', this.profile);
    this.profile.gender = this.gender;
    if (this.cvBahasa != null) {
      if (this.cvBahasa) {
          console.log('sni');
      }
    }

    if (this.profile.kodePosID === 0) {
      this.kodePosID = '';
    }

    this.profileService.update(this.profile).subscribe(result => {
        if (result.body.errCode === '00') {
          if (this.cvBahasa != null) {
            if (this.cvBahasa) {
                this.uploadFileCvBahasa(this.profile.id);
                this.getUser();
                this.getCountUserProgress();
            }
          }

          if (this.cvEnglish != null) {
            if (this.cvEnglish) {
                this.uploadFileCvEnglish(this.profile.id);
                this.getUser();
                this.getCountUserProgress();
            }
          }

          if (this.avatar != null) {
            if (this.avatar) {
                this.uploadFileAvatar(this.profile.id);
                this.getUser();
                this.getCountUserProgress();
            }
          }
          this.showForm = false;
          this.getUser();
          this.getCountUserProgress();
          Swal.fire('Success', 'Profile saved', 'success');
          this.action.next('refresh');
        } else {
            Swal.fire('Failed', 'Profile not saved', 'error');
        }
    });
  }

  uploadFileCvBahasa(id: number) {
    const formData = new FormData();
    formData.append('files', this.cvBahasa);
    formData.append('type', 'cv_bahasa');
    formData.append('id', String(id));

    console.log('formData: ', formData);

    this.profileService.upload(formData)
        .subscribe(res  => {
          // console.log('res: ', res);
          if (res.body.errCode !== '00') {
            Swal.fire('Failed', 'CV in Bahasa not saved', 'error');
            return;
          }
          this.getUser();
        });
  }

  uploadFileCvEnglish(id: number) {
    const formData = new FormData();
    formData.append('files', this.cvEnglish);
    formData.append('type', 'cv_english');
    formData.append('id', String(id));

    console.log('formData: ', formData);

    this.profileService.upload(formData)
        .subscribe(res  => {
          // console.log('res: ', res);
          if (res.body.errCode !== '00') {
            Swal.fire('Failed', 'CV in English not saved', 'error');
            return;
          }
          this.getUser();
        });
  }

  uploadFileAvatar(id: number) {
    const formData = new FormData();
    formData.append('files', this.avatar);
    formData.append('type', 'avatar');
    formData.append('id', String(id));

    console.log('formData: ', formData);

    this.profileService.upload(formData)
        .subscribe(res  => {
          // console.log('res: ', res);
          if (res.body.errCode !== '00') {
            Swal.fire('Failed', 'Avatar not saved', 'error');
          }
          this.getUser();
        });
  }

  download(id: number, files: string, fileName: string) {
    // console.log('profile: ', data);

    const pDownloads = new ProfileDownload();

    pDownloads.id = id;
    pDownloads.typeFile = files;

    console.log('profile:', pDownloads);
    if (files === 'cv_bahasa') {
      fileName = 'cv_bahasa_' + fileName;
    }

    if (files === 'cv_english') {
      fileName = 'cv_english_' + fileName;
    }

    this.profileService.downloadCV(pDownloads).subscribe(dataBlob => {
        console.log('data blob ==> ', dataBlob);
        if (dataBlob.type === 'text/plain') {
            Swal.fire('File Not Found', 'Failed Download', 'error');
            return;
        }
        const newBlob = new Blob([dataBlob], { type: 'application/text/pdf' });

        const link = document.createElement('a');
        link.href = URL.createObjectURL(newBlob);
        link.download = fileName;
        document.body.append(link);
        link.click();
        link.remove();
        window.addEventListener('focus', e => URL.revokeObjectURL(link.href), {once: true});

    });
  }

  public changedGender(e: any): void {
    console.log('e any: ', e);
    if (e.value === '') {
      this.shwErrorGender = true;
    }
    this.shwErrorGender = false;
    this.sprofileForm.get('gender').setValue(e.value);
    this.gender = e.value;
    console.log('gender: ',  this.gender);
  }

  public shwGender(evet) {
    console.log('gender: ', evet.value);
    if (evet.value === undefined) {
      this.shwErrorGender = true;
    }
    this.shwErrorGender = false;
  }

}
