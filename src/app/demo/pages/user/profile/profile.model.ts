export class Profile {
    constructor(
        public id?: number,
        public firstName?: string,
        public lastName?: string,
        public phoneNumber?: string,
        public birthDate?: string,
        public address?: string,
        public cvBahasa?: string,
        public cvEnglish?: string,
        public userID?: number,
        public avatar?: string,
        public errCode?: string,
        public errDesc?: string,
        public activities?: string,
        public kodePosID?: number,
        public kodePos?: ZipCode,
        public socialInterest?: string,
        public gender?: string,
        public license?: string
    ) {}
}

export class CountProgress {
    constructor(
        public count?: number,
        public errCode?: string,
        public errDesc?: string
    ) {}
}

export class ProfileDownload {
    constructor(
        public id?: number,
        public typeFile?: string
    ) {}
}


export class ZipCode {
    constructor(
        public id?: number,
        public kelurahan?: string,
        public kabupaten?: string,
        public kecamatan?: string,
        public provinsi?: string,
        public kodePos?: string,
        public errCode?: string,
        public errDesc?: string
    ) {}
}
