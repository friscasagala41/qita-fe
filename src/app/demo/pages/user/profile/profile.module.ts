import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { NgbDropdownModule, NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { Select2Module } from 'ng2-select2';
import { AutoCompleterModule } from 'ng-uikit-pro-standard';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';


@NgModule({
  declarations: [ProfileComponent],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    SharedModule,
    NgbDropdownModule,
    FormsModule,
    NgbProgressbarModule,
    Select2Module,
    AutoCompleterModule,
    AutocompleteLibModule
  ]
})
export class ProfileModule { }
