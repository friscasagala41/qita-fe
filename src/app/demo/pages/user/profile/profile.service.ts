import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { CountProgress, Profile, ZipCode } from './profile.model';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  // public subject = new Subject<any>();

  private dataSource = new BehaviorSubject<any>(new Profile());
  data = this.dataSource.asObservable();
  // data = this.subject.asObservable();
  SERVER: string;
  constructor(private http: HttpClient,
              private configService: AppConfigService) {
        this.SERVER = this.configService.getSavedServerPath();
    }

  updatedDataSelection(data: Profile) {
      this.dataSource.next(data);
  }

  getUserByID(): Observable<HttpResponse<Profile>> {
      let newResourceUrl = null;
      let result = null;
      const search = {};

      newResourceUrl = this.SERVER + `/api/user/seeker/detail`;

      result = this.http.post<Profile>(newResourceUrl, search, { observe: 'response' })
          .pipe(
              tap(
                  // console.log('sdad')
                  results => console.log('raw', results)
              )
          );
      // console.log(result);
      return result;
  }

  upload(logo: FormData): Observable<HttpResponse<Profile>> {
    const newResourceUrl = this.SERVER + `/api/user/seeker/upload`;

    return this.http.post<Profile>(newResourceUrl, logo, { observe: 'response'})
        .pipe(map((res: HttpResponse<Profile>) => Object.assign({}, res)));
  }

  add(profile: Profile): Observable<HttpResponse<Profile>> {
    const copy = Object.assign({}, profile);

    copy.userID = +copy.userID;
    copy.id = + copy.id;

    const newResourceUrl = this.SERVER + `/api/user/seeker/`;

    return this.http.post<Profile>(newResourceUrl, copy, { observe: 'response'})
        .pipe(map((res: HttpResponse<Profile>) => Object.assign({}, res)));
  }

  update(profile: Profile): Observable<HttpResponse<Profile>> {
    const copy = Object.assign({}, profile);

    copy.userID = +copy.userID;
    copy.id = + copy.id;

    const newResourceUrl = this.SERVER + `/api/user/seeker/`;

    return this.http.put<Profile>(newResourceUrl, copy, { observe: 'response' })
        .pipe(map((res: HttpResponse<Profile>) => Object.assign({}, res)));
  }

  getCountUserProgress(): Observable<HttpResponse<CountProgress>> {
    let newResourceUrl = null;
    let result = null;
    const search = {};

    newResourceUrl = this.SERVER + `/api/user/seeker/count`;

    result = this.http.post<Profile>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  downloadCV(data): Observable<Blob> {
    const newResourceUrl = this.SERVER + `/api/user/seeker/download`;

    return this.http.post(newResourceUrl, data, { responseType: 'blob' });
  }

  getAllKodePos(): Observable<HttpResponse<ZipCode[]>> {
    let newResourceUrl = null;
    let result = null;
    const search = {};

    newResourceUrl = this.SERVER + `/api/kode-pos/all`;

    result = this.http.post<ZipCode[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }


  searchZipCode(name?: string): Observable<HttpResponse<ZipCode[]>> {
    let newResourceurl = null;
    let result = null;

    newResourceurl = this.SERVER + `/api/kode-pos?name=` + name;

    result = this.http.get<ZipCode[]>(newResourceurl, { observe: 'response' })
      .pipe(
        tap(
          // tslint:disable-next-line:no-shadowed-variable
          result => console.log('data', result)
        )
      );
    return result;
  }

  searchDistrict(id?: string): Observable<HttpResponse<ZipCode[]>> {
    let newResourceurl = null;
    let result = null;

    newResourceurl = this.SERVER + `/api/kode-pos/district?name=` + id;

    result = this.http.get<ZipCode[]>(newResourceurl, { observe: 'response' })
      .pipe(
        tap(
          // tslint:disable-next-line:no-shadowed-variable
          result => console.log('data', result)
        )
      );
    return result;
  }

}
