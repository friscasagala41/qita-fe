import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { Skill, UserSkill } from './skill.model';

@Injectable({
  providedIn: 'root'
})
export class SkillService {

  // public subject = new Subject<any>();

  private dataSource = new BehaviorSubject<any>(new UserSkill());
  data = this.dataSource.asObservable();
  // data = this.subject.asObservable();
  SERVER: string;
  constructor(private http: HttpClient,
              private configService: AppConfigService) {
        this.SERVER = this.configService.getSavedServerPath();
    }

  updatedDataSelection(data: UserSkill) {
      this.dataSource.next(data);
  }

  getAllSkill(): Observable<HttpResponse<UserSkill[]>> {
      let newResourceUrl = null;
      let result = null;
      const search = {};

      newResourceUrl = this.SERVER + `/api/user/skill/all`;

      result = this.http.post<UserSkill[]>(newResourceUrl, search, { observe: 'response' })
          .pipe(
              tap(
                  // console.log('sdad')
                  results => console.log('raw', results)
              )
          );
      // console.log(result);
      return result;
  }

  add(skill: UserSkill): Observable<HttpResponse<UserSkill>> {
    const copy = Object.assign({}, skill);

    copy.userSeekerID = +copy.userSeekerID;
    copy.id = +copy.id;
    copy.skillID = +copy.skillID;

    const newResourceUrl = this.SERVER + `/api/user/skill/`;

    return this.http.post<UserSkill>(newResourceUrl, copy, { observe: 'response'})
        .pipe(map((res: HttpResponse<UserSkill>) => Object.assign({}, res)));
  }

  getSkill(req ?: any): Observable<HttpResponse<UserSkill>> {
    let newResourceUrl = null;
    let result = null;
    let id = null;
    const search = {};

    Object.keys(req).forEach((key) => {
      if (key === 'id') {
          id = req[key];
      }
  });

    newResourceUrl = this.SERVER + `/api/user/skill/${id}`;

    result = this.http.post<UserSkill[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  update(skill: UserSkill): Observable<HttpResponse<UserSkill>> {
    const copy = Object.assign({}, skill);

    copy.userSeekerID = +copy.userSeekerID;
    copy.id = + copy.id;

    const newResourceUrl = this.SERVER + `/api/user/skill/`;

    return this.http.put<UserSkill>(newResourceUrl, copy, { observe: 'response' })
        .pipe(map((res: HttpResponse<UserSkill>) => Object.assign({}, res)));
  }

  skills(): Observable<HttpResponse<Skill[]>> {
    let newResourceUrl = null;
    let result = null;

    newResourceUrl = this.SERVER + `/api/skill`;
    result = this.http.get<Skill[]>(newResourceUrl, { observe: 'response' })
        .pipe(tap(
            results => console.log('raw =>', results)
        )
        );
    return result;
  }

}
