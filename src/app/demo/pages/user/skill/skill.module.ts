import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SkillRoutingModule } from './skill-routing.module';
import { SkillComponent } from './skill.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { SelectModule } from 'ng-uikit-pro-standard';
import { NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import { Select2Module } from 'ng2-select2';


@NgModule({
  declarations: [SkillComponent],
  imports: [
    CommonModule,
    SkillRoutingModule,
    SharedModule,
    SelectModule,
    NgbProgressbarModule,
    Select2Module
  ]
})
export class SkillModule { }
