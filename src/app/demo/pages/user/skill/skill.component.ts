import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Select2OptionData } from 'ng2-select2';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2';
import { CountProgress } from '../profile/profile.model';
import { ProfileService } from '../profile/profile.service';
import { Skill, UserSkill } from './skill.model';
import { SkillService } from './skill.service';

@Component({
  selector: 'app-skill',
  templateUrl: './skill.component.html',
  styleUrls: ['./skill.component.css']
})
export class SkillComponent implements OnInit {
  userSkills: UserSkill[];
  userSkill: UserSkill;
  showFormEdit: boolean;
  id: number;
  showDiv: boolean;
  status: boolean;
  showFormAdd: boolean;
  showBtnEdit: boolean;
  showBtnAdd: boolean;

  skills = [];

  optionSkill: any;
  selecetedSkill: any;

  action = new Subject();
  userSkillForm: FormGroup;
  userSkillEditForm: FormGroup;

  public exampleData: Array<Select2OptionData>;

  public skillID: string;
  public selected: string;

  countProgres: CountProgress;
  tProgress: number;

  shwErrorSkill = false;
  shwBtn = true;

  constructor(
    private skillService: SkillService,
    private fb: FormBuilder,
    private profileService: ProfileService
  ) {
    this.sUserSkillForm();
    this.sUserSkillEditForm();
  }

  sUserSkillForm() {
    this.userSkillForm = this.fb.group({
        id: [{ value: '', disabled: true }],
        skillID: ['']
    });
  }

  sUserSkillEditForm() {
    this.userSkillEditForm = this.fb.group({
        id: [{ value: '', disabled: true }],
        skillID: ['']
    });
  }

  ngOnInit() {
    this.tProgress = 0;

    this.getCountUserProgress();

    this.getAllSkill();

    this.showFormEdit = false;
    this.showDiv = true;
    this.status = false;
    this.showFormAdd = false;
    this.showBtnEdit = true;
    this.showBtnAdd = true;
    this.shwBtn = true;
  }

  getCountUserProgress() {
    this.profileService.getCountUserProgress()
        .subscribe(
            (res: HttpResponse<CountProgress>) => {
                console.log('res.body', res.body);
                this.successLoadCountUserProgress(res.body);
            }
        );
  }

  private successLoadCountUserProgress(data) {
    console.log('succesloaduser ', data);
    this.countProgres = data.contents;
    this.tProgress = (+this.countProgres / 4) * 100;
    console.log('count progress: ', this.tProgress);
  }

  getAllSkill() {
    this.skillService.getAllSkill()
        .subscribe(
            (res: HttpResponse<UserSkill[]>) => {
                console.log('res.body', res.body);
                this.successLoadUser(res.body);
            }
        );
  }

  private successLoadUser(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.userSkills = data.contents;
  }

  add() {
    this.skillService.skills().subscribe(
      (res: HttpResponse<Skill[]>) => {
          // tslint:disable-next-line:no-string-literal
          const skills1 = res.body['contents'];
          this.skills = [];
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < skills1.length; i++) {
            // console.log('skill name: ', skills1[i].name);
            // this.skills.push({value: skills1[i].id, label: skills1[i].name});
            this.skills.push({id: skills1[i].id, text: skills1[i].name});
            this.skillID = skills1[0].id;
          }
      }
    );
    console.log('skills: ', this.skills);
    this.showFormAdd = true;
    this.showBtnEdit = false;
    this.showBtnAdd = false;
    this.userSkill = new UserSkill();
  }

  optionSelect(event) {
    this.optionSkill = event;
    this.selecetedSkill = this.optionSkill.value;
    console.log('select : ', event);
  }

  batal() {
    this.getAllSkill();
    this.showFormEdit = false;
    this.showDiv = true;
    this.status = false;
    this.showFormAdd = false;
    this.showBtnEdit = true;
    this.showBtnAdd = true;
  }

  save() {
    if (this.selecetedSkill === '') {
      Swal.fire('Failed', 'skill empty', 'error');
      return;
    }

    this.userSkill.skillID = +this.selected;

    console.log('user skill: ', this.userSkill, ' skillId: ', this.skillID);

    this.skillService.add(this.userSkill).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
            Swal.fire('Success', 'skill saved', 'success');
            this.action.next('refresh');
            this.getAllSkill();
            this.showFormEdit = false;
            this.showDiv = true;
            this.status = false;
            this.showFormAdd = false;
            this.showBtnEdit = true;
            this.showBtnAdd = true;
            this.userSkillForm.reset();
      } else {
          Swal.fire('Failed', 'skill not saved', 'error');
      }
      this.action.next('refresh');
    });
  }

  editForm(id: number) {
    this.showFormEdit = true;
    this.id = id;
    this.showDiv = false;
    this.showFormAdd = false;
    const nId = this.id.toString();
    this.status = !this.status;
    if (this.status) {
      document.getElementById(nId).style.display = 'none';
    } else {
      document.getElementById(nId).style.display = 'block';
    }
    this.showBtnEdit = false;
    this.showBtnAdd = false;
    console.log('skill: ' + this.id);
    this.getSkill(id);
    this.skillService.skills().subscribe(
      (res: HttpResponse<Skill[]>) => {
          // tslint:disable-next-line:no-string-literal
          const skills1 = res.body['contents'];
          this.skills = [];
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < skills1.length; i++) {
            console.log('skill name: ', skills1[i].name);
            this.skills.push({id: skills1[i].id, text: skills1[i].name});
          }
      }
    );
  }

  getSkill(ids) {
    this.skillService.getSkill({
      id: ids
    })
    .subscribe(
        (res: HttpResponse<UserSkill>) => {
            this.successLoadUserSkill(res.body);
        }
    );
  }

  private successLoadUserSkill(data) {
    this.userSkill = data.contents;
    this.skillID = this.userSkill.skillID.toString();
    console.log(this.userSkill.skillID);
    // this.userSkillEditForm.get('skillID').setValue(this.userSkill.skillID);

    console.log('userskill: ', this.userSkill);
  }

  public changed(e: any): void {
    this.selected = e.value;
    if (this.selected !== '') {
      this.shwBtn = false;
    }
  }

  updateForm(ids) {
    if (this.selecetedSkill === '') {
      Swal.fire('Failed', 'skill empty', 'error');
      return;
    }

    this.userSkill.id = ids;
    this.userSkill.skillID = +this.selected;

    this.skillService.update(this.userSkill).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
            Swal.fire('Success', 'Skill saved', 'success');
            this.action.next('refresh');
            this.getAllSkill();
            this.showFormEdit = false;
            this.showDiv = true;
            this.status = false;
            this.showFormAdd = false;
            this.showBtnEdit = true;
            this.showBtnAdd = true;
            this.userSkillEditForm.reset();
      } else {
          Swal.fire('Failed', 'Skill not saved', 'error');
      }
      this.action.next('refresh');
    });
  }

  shwSkill(event) {
    if (event.value === undefined) {
      this.shwErrorSkill = true;
    } else {
      this.shwErrorSkill = false;
      this.shwBtn = false;
    }
  }

}
