export class UserSkill {
    constructor(
        public id?: number,
        public userSeekerID?: number,
        public skillID?: number,
        public skill?: Skill,
        public errCode?: string,
        public errDesc?: string
    ) {}
}

export class Skill {
    constructor(
        public id?: number,
        public name?: string,
        public errCode?: string,
        public errDesc?: string
    ) {}
}
