import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'profile',
        loadChildren: () => import('./profile/profile.module').then(module => module.ProfileModule)
      },
      {
        path: 'experience',
        loadChildren: () => import('./experience/experience.module').then(module => module.ExperienceModule)
      },
      {
        path: 'skill',
        loadChildren: () => import('./skill/skill.module').then(module => module.SkillModule)
      },
      {
        path: 'education',
        loadChildren: () => import('./education/education.module').then(module => module.EducationModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
