import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpecializationModalComponent } from './specialization-modal.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SpecializationModalComponent]
})
export class SpecializationModalModule { }
