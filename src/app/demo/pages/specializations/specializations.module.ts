import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SpecializationsRoutingModule } from './specializations-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SpecializationsRoutingModule
  ]
})
export class SpecializationsModule { }
