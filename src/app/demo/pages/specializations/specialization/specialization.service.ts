import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { Specialization } from './specialization.model';

@Injectable({
  providedIn: 'root'
})
export class SpecializationService {

  // public subject = new Subject<any>();

  private dataSource = new BehaviorSubject<any>(new Specialization());
  data = this.dataSource.asObservable();
  // data = this.subject.asObservable();
  SERVER: string;
  constructor(private http: HttpClient,
              private configService: AppConfigService) {
        this.SERVER = this.configService.getSavedServerPath();
    }

  updatedDataSelection(data: Specialization) {
      this.dataSource.next(data);
  }

  filter(req?: any): Observable<HttpResponse<Specialization[]>> {
    let pageNumber = null;
    let pageCount = null;
    let newResourceUrl = null;
    let result = null;
    const search = {
        name: ''
    };

    Object.keys(req).forEach((key) => {
        if (key === 'page') {
            pageNumber = req[key];
        }
        if (key === 'count') {
            pageCount = req[key];
        }
        if (key === 'name') {
            search.name = req[key];
        }
    });

    newResourceUrl = this.SERVER + `/api/specialization/filter/page/${pageNumber}/count/${pageCount}`;

    result = this.http.post<Specialization[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  getAllSpecialization(): Observable<HttpResponse<Specialization[]>> {
      let newResourceUrl = null;
      let result = null;
      const search = {};

      newResourceUrl = this.SERVER + `/api/specialization/all`;

      result = this.http.post<Specialization[]>(newResourceUrl, search, { observe: 'response' })
          .pipe(
              tap(
                  // console.log('sdad')
                  results => console.log('raw', results)
              )
          );
      // console.log(result);
      return result;
  }

  add(specialization: Specialization): Observable<HttpResponse<Specialization>> {
    const copy = Object.assign({}, specialization);

    copy.id = +copy.id;

    const newResourceUrl = this.SERVER + `/api/specialization/`;

    return this.http.post<Specialization>(newResourceUrl, copy, { observe: 'response'})
        .pipe(map((res: HttpResponse<Specialization>) => Object.assign({}, res)));
  }

  getSpecialization(req ?: any): Observable<HttpResponse<Specialization>> {
    let newResourceUrl = null;
    let result = null;
    let id = null;
    const search = {};

    Object.keys(req).forEach((key) => {
      if (key === 'id') {
          id = req[key];
      }
  });

    newResourceUrl = this.SERVER + `/api/specialization/${id}`;

    result = this.http.post<Specialization[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  update(specialization: Specialization): Observable<HttpResponse<Specialization>> {
    const copy = Object.assign({}, specialization);

    copy.id = + copy.id;

    const newResourceUrl = this.SERVER + `/api/specialization/`;

    return this.http.put<Specialization>(newResourceUrl, copy, { observe: 'response' })
        .pipe(map((res: HttpResponse<Specialization>) => Object.assign({}, res)));
  }

}
