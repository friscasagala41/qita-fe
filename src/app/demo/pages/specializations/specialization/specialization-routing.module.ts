import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SpecializationComponent } from './specialization.component';


const routes: Routes = [
  {
    path: '',
    component: SpecializationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpecializationRoutingModule { }
