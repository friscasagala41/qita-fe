export class Specialization {
    constructor(
        public id?: number,
        public name?: string,
        public errCode?: string,
        public errDesc?: string
    ) {}
}
