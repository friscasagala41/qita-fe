import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SpecializationRoutingModule } from './specialization-routing.module';
import { SpecializationComponent } from './specialization.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import {NgbDropdownModule, NgbModalModule, NgbModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [SpecializationComponent],
  imports: [
    CommonModule,
    SpecializationRoutingModule,
    SharedModule,
    NgbDropdownModule,
    NgbModalModule,
    NgbPaginationModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class SpecializationModule { }
