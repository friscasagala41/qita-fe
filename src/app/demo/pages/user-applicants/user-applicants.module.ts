import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserApplicantsRoutingModule } from './user-applicants-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    UserApplicantsRoutingModule
  ]
})
export class UserApplicantsModule { }
