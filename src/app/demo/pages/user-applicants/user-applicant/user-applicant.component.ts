import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApplicationList } from '../../application-lists/application-list/applicatio-list.model';
import { UserApplicantService } from './user-applicant.service';

@Component({
  selector: 'app-user-applicant',
  templateUrl: './user-applicant.component.html',
  styleUrls: ['./user-applicant.component.scss']
})
export class UserApplicantComponent implements OnInit {

  applicationLists: ApplicationList[];

  constructor(
    private applicationListService: UserApplicantService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getAllTrxVacancy();
  }

  getAllTrxVacancy() {
    this.applicationListService.getAllTrxVacancy()
        .subscribe(
            (res: HttpResponse<ApplicationList[]>) => {
                console.log('res.body', res.body);
                this.successLoadUser(res.body);
            }
        );
  }

  private successLoadUser(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.applicationLists = data.contents;
    console.log('applicationLists: ', this.applicationLists);
  }


}
