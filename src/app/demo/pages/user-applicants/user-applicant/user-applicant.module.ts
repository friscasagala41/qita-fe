import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserApplicantRoutingModule } from './user-applicant-routing.module';
import { UserApplicantComponent } from './user-applicant.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';


@NgModule({
  declarations: [UserApplicantComponent],
  imports: [
    CommonModule,
    UserApplicantRoutingModule,
    SharedModule
  ]
})
export class UserApplicantModule { }
