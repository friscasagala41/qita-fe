import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserApplicantComponent } from './user-applicant.component';

describe('UserApplicantComponent', () => {
  let component: UserApplicantComponent;
  let fixture: ComponentFixture<UserApplicantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserApplicantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserApplicantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
