import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { ApplicationList, ProcessState } from '../../application-lists/application-list/applicatio-list.model';

@Injectable({
  providedIn: 'root'
})
export class UserApplicantService {

  // public subject = new Subject<any>();

  private dataSource = new BehaviorSubject<any>(new ApplicationList());
  data = this.dataSource.asObservable();
  // data = this.subject.asObservable();
  SERVER: string;
  constructor(private http: HttpClient,
              private configService: AppConfigService) {
        this.SERVER = this.configService.getSavedServerPath();
    }

  updatedDataSelection(data: ApplicationList) {
      this.dataSource.next(data);
  }

  getAllTrxVacancy(): Observable<HttpResponse<ApplicationList[]>> {
      let newResourceUrl = null;
      let result = null;
      const search = {};

      newResourceUrl = this.SERVER + `/api/trx-user-vacancy/seeker/list`;

      result = this.http.post<ApplicationList[]>(newResourceUrl, search, { observe: 'response' })
          .pipe(
              tap(
                  // console.log('sdad')
                  results => console.log('raw', results)
              )
          );
      // console.log(result);
      return result;
  }
}
