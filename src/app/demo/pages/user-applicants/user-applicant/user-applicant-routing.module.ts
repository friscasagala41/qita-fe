import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserApplicantComponent } from './user-applicant.component';


const routes: Routes = [
  {
    path: '',
    component: UserApplicantComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserApplicantRoutingModule { }
