/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { UserApplicantService } from './user-applicant.service';

describe('Service: UserApplicant', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserApplicantService]
    });
  });

  it('should ...', inject([UserApplicantService], (service: UserApplicantService) => {
    expect(service).toBeTruthy();
  }));
});
