import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserVacancyRoutingModule } from './user-vacancy-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    UserVacancyRoutingModule
  ]
})
export class UserVacancyModule { }
