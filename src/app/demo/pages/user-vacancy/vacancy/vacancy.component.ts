import { CurrencyPipe } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { TOTAL_RECORD_PER_PAGE } from 'src/app/theme/shared/constant/base.constant';
import Swal from 'sweetalert2';
import { CompanyVacancy, JobList } from '../../company/company-vacancy/company-vacancy.model';
import { CompanyVacancyService } from '../../company/company-vacancy/company-vacancy.service';
import { TrxUserVacancy } from '../vacancy-detail/vacancy-detail.model';
import { VacancyService } from './vacancy.service';

@Component({
  selector: 'app-vacancy',
  templateUrl: './vacancy.component.html',
  styleUrls: ['./vacancy.component.css']
})
export class VacancyComponent implements OnInit {

  vacancys: JobList[];
  // showFormEdit: boolean;
  id: number;
  // showDiv: boolean;
  // status: boolean;
  // showFormAdd: boolean;
  // showBtnEdit: boolean;
  // showBtnAdd: boolean;
  vacancy: CompanyVacancy;
  trxUserVacancy: TrxUserVacancy;

  formattedAmountStart;
  salaryStart = '';

  formattedAmountEnd;
  salaryEnd = '';

  names = '';

  searchForm: FormGroup;
  action = new Subject();

  specialization = '';
  jobLevel = '';
  industry = '';
  location = '';

  maxTotalpage = 1;
  curPage: number;
  totalItems: any;
  itemsPerPage: any;
  previousPage: any;

  constructor(
    private fb: FormBuilder,
    private currencyPipe: CurrencyPipe,
    private vacancyService: VacancyService,
    public router: Router,
  ) {
    this.rForm();
  }

  salaryShow = [
    {
      id: 1,
      text: 'Ya',
    },
    {
      id: 0,
      text: 'Tidak'
    }
  ];

  rForm() {
    this.searchForm = this.fb.group({
      names: ['']
    });
  }

  ngOnInit() {
    this.curPage = 1;
    this.getAllVacancy(this.curPage);

    // this.showFormEdit = false;
    // this.showDiv = true;
    // this.status = false;
    // this.showFormAdd = false;
    // this.showBtnEdit = true;
    // this.showBtnAdd = true;
  }

  getAllVacancy(curpage) {
    this.vacancyService.getAllUserVacancy({
      page: curpage,
      count: 10,
      specialization: this.specialization,
      jobLevel: this.jobLevel,
      industry: this.industry,
      location: this.location
    })
        .subscribe(
            (res: HttpResponse<JobList[]>) => {
                console.log('res.body', res.body);
                this.successLoadUser(res.body);
            }
        );
  }

  private successLoadUser(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.vacancys = data.contents;
    this.maxTotalpage = Math.ceil(data.totalRow / TOTAL_RECORD_PER_PAGE);
    this.totalItems = data.totalRow;
    this.itemsPerPage = TOTAL_RECORD_PER_PAGE;
    // tslint:disable-next-line:prefer-for-of
    // for (let i = 0; i < this.vacancys.length; i++) {
    //   const tamp = this.currencyPipe.transform(this.vacancys[i].salaryStart);
    //   const tamps1 = tamp.toString().replace(/\./g, '|').replace(/\,/g, '.').replace(/\|/g, ',').replace(/\$/g, '');
    //   this.vacancys[i].startSalary = tamps1;

    //   const tamp2 = this.currencyPipe.transform(this.vacancys[i].salaryEnd);
    //   const tamps2 = tamp2.toString().replace(/\./g, '|').replace(/\,/g, '.').replace(/\|/g, ',').replace(/\$/g, '');
    //   this.vacancys[i].endSalary = tamps2;
    // }
  }

  public save(id: number, action: string) {
    this.trxUserVacancy = new TrxUserVacancy();
    if ( action === 'save' ) {
      this.trxUserVacancy.isSave = true;
      this.trxUserVacancy.userCompanyVacancyID = id;
      console.log('user trx: ', this.trxUserVacancy);
      this.vacancyService.add(this.trxUserVacancy).subscribe(result => {
        console.log(result);
        if (result.body.errCode === '00') {
          if (result.body.errDesc !== 'SUCCESS..') {
            // const options = { progressBar: true, extendedTimeOut: 3000, timeOut: 5000 };
            // this.toastrService.error(result.body.errDesc, 'New Password', options);
            // alert('Password ' + );
            Swal.fire('Error', result.body.errDesc, 'error');
          } else {
            // this.toastrService.success('Save success !', 'save');
            Swal.fire('Success', 'Jobs saved', 'success');
            this.action.next('refresh');
          }
        } else {
          alert('error');
        }
      });
    }

    if ( action === 'apply' ) {
      this.trxUserVacancy.userCompanyVacancyID = id;
      this.trxUserVacancy.statusID = 1;
      this.vacancyService.add(this.trxUserVacancy).subscribe(result => {
        console.log(result);
        if (result.body.errCode === '00') {
          if (result.body.errDesc !== 'SUCCESS..') {
            // const options = { progressBar: true, extendedTimeOut: 3000, timeOut: 5000 };
            // this.toastrService.error(result.body.errDesc, 'New Password', options);
            // alert('Password ' + );
            Swal.fire('Error', result.body.errDesc, 'error');
          } else {
            // this.toastrService.success('Save success !', 'save');
            Swal.fire('Success', 'Jobs applied', 'success');
            this.action.next('refresh');
          }
        } else {
          alert('error');
        }
      });
    }
  }

  getAllVacancySearch(names) {
    this.vacancyService.getAllUserVacancyBySearch({name: names})
        .subscribe(
            (res: HttpResponse<CompanyVacancy[]>) => {
                console.log('res.body', res.body);
                this.successLoadUserSearch(res.body);
            }
        );
  }

  private successLoadUserSearch(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.vacancys = data.contents;
    // tslint:disable-next-line:prefer-for-of
    // for (let i = 0; i < this.vacancys.length; i++) {
    //   const tamp = this.currencyPipe.transform(this.vacancys[i].salaryStart);
    //   const tamps1 = tamp.toString().replace(/\,/g, '.');
    //   this.vacancys[i].startSalary = tamps1.substring(0, 2);

    //   const tamp2 = this.currencyPipe.transform(this.vacancys[i].salaryEnd);
    //   const tamps2 = tamp2.toString().replace(/\./g, '|').replace(/\,/g, '.').replace(/\|/g, ',').replace(/\$/g, '');
    //   this.vacancys[i].endSalary = tamps2;
    // }
  }

  vacancyCompanyDetail(id) {
    // this.router.navigate(['/vacancy/company/detail/' + id]);
    window.open('/vacancy/company/detail/' + id, '_blank');
  }

  changeIndustry(event) {
    console.log('event: ', event);
    this.getAllVacancy(this.curPage);
  }

  changeLocation(event) {
    console.log('event: ', event);
    this.getAllVacancy(this.curPage);
  }

  changeSpecialization(event) {
    console.log('event: ', event);
    this.getAllVacancy(this.curPage);
  }

  changeJobLevel(event) {
    console.log('event: ', event);
    this.getAllVacancy(this.curPage);
  }

  loadPage(curPage) {
    if (this.curPage !== this.previousPage) {
      this.previousPage = this.curPage;
      this.getAllVacancy(this.curPage);
      console.log('filter', this.getAllVacancy);
    }
  }
}
