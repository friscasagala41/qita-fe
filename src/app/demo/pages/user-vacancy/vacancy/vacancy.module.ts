import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';

import { VacancyRoutingModule } from './vacancy-routing.module';
import { VacancyComponent } from './vacancy.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [VacancyComponent],
  imports: [
    CommonModule,
    VacancyRoutingModule,
    SharedModule,
    NgbModule
  ],
  providers: [CurrencyPipe]
})
export class VacancyModule { }
