import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VacancyComponent } from './vacancy.component';


const routes: Routes = [
  {
    path: '',
    component: VacancyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VacancyRoutingModule { }
