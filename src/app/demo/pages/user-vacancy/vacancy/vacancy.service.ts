import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { CompanyVacancy, JobList } from '../../company/company-vacancy/company-vacancy.model';
import { TrxUserVacancy } from '../vacancy-detail/vacancy-detail.model';

@Injectable({
  providedIn: 'root'
})
export class VacancyService {
  // public subject = new Subject<any>();

 private dataSource = new BehaviorSubject<any>(new CompanyVacancy());
 data = this.dataSource.asObservable();
 // data = this.subject.asObservable();
 SERVER: string;
 constructor(private http: HttpClient,
             private configService: AppConfigService) {
       this.SERVER = this.configService.getSavedServerPath();
   }

 updatedDataSelection(data: CompanyVacancy) {
     this.dataSource.next(data);
 }

 getAllUserVacancy(req ?: any): Observable<HttpResponse<JobList[]>> {
    let pageNumber = null;
    let pageCount = null;
    let newResourceUrl = null;
    let result = null;
    const search = {
        specialization: '',
        jobLevel: '',
        industry: '',
        location: ''
    };

    Object.keys(req).forEach((key) => {
        if (key === 'page') {
            pageNumber = req[key];
        }
        if (key === 'count') {
            pageCount = req[key];
        }
        if (key === 'specialization') {
            search.specialization = req[key];
        }
        if (key === 'jobLevel') {
            search.jobLevel = req[key];
        }
        if (key === 'industry') {
            search.industry = req[key];
        }
        if (key === 'location') {
            search.location = req[key];
        }
    });


    newResourceUrl = this.SERVER + `/api/company/vacancy/all/user/page/${pageNumber}/count/${pageCount}`;

    result = this.http.post<JobList[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
 }

 getCompanyVacancy(req ?: any): Observable<HttpResponse<CompanyVacancy>> {
    let newResourceUrl = null;
    let result = null;
    let id = null;
    const search = {};

    Object.keys(req).forEach((key) => {
      if (key === 'id') {
          id = req[key];
      }
  });

    newResourceUrl = this.SERVER + `/api/company/vacancy/${id}`;

    result = this.http.post<CompanyVacancy[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  getAllUserVacancyBySearch(req ?: any): Observable<HttpResponse<CompanyVacancy[]>> {
    let newResourceUrl = null;
    let result = null;
    const search = {
        name : ''
    };

    Object.keys(req).forEach((key) => {
      if (key === 'name') {
          search.name = req[key];
      }
  });

    newResourceUrl = this.SERVER + `/api/company/vacancy/all/user/${search.name}`;

    result = this.http.post<CompanyVacancy[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  add(trxUserVacancy: TrxUserVacancy): Observable<HttpResponse<TrxUserVacancy>> {
    const copy = Object.assign({}, trxUserVacancy);

    copy.userSeekerID = +copy.userSeekerID;
    copy.id = +copy.id;
    copy.userCompanyVacancyID = +copy.userCompanyVacancyID;

    const newResourceUrl = this.SERVER + `/api/trx-user-vacancy/`;

    return this.http.post<TrxUserVacancy>(newResourceUrl, copy, { observe: 'response'})
        .pipe(map((res: HttpResponse<TrxUserVacancy>) => Object.assign({}, res)));
  }

  getTrxVacancyByVacancy(req ?: any): Observable<HttpResponse<TrxUserVacancy>> {
    let newResourceUrl = null;
    let result = null;
    let id = null;
    const search = {};

    Object.keys(req).forEach((key) => {
      if (key === 'id') {
          id = req[key];
      }
  });

    newResourceUrl = this.SERVER + `/api/trx-user-vacancy/vacancy/${id}`;

    result = this.http.post<TrxUserVacancy[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

}
