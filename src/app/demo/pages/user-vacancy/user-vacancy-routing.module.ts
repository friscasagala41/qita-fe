import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'list',
        loadChildren: () => import('./vacancy/vacancy.module').then(module => module.VacancyModule)
      },
      {
        path: 'detail/:id',
        loadChildren: () => import('./vacancy-detail/vacancy-detail.module').then(module => module.VacancyDetailModule)
      },
      {
        path: 'company/detail/:id',
        // tslint:disable-next-line:max-line-length
        loadChildren: () => import('./vacancy-company-detail/vacancy-company-detail.module').then(module => module.VacancyCompanyDetailModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserVacancyRoutingModule { }
