import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';

import { VacancyDetailRoutingModule } from './vacancy-detail-routing.module';
import { VacancyDetailComponent } from './vacancy-detail.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';


@NgModule({
  declarations: [VacancyDetailComponent],
  imports: [
    CommonModule,
    VacancyDetailRoutingModule,
    SharedModule
  ],
  providers: [CurrencyPipe]
})
export class VacancyDetailModule { }
