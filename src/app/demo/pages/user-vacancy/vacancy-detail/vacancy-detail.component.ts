import { CurrencyPipe } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { CompanyVacancy } from '../../company/company-vacancy/company-vacancy.model';
import { CompanyVacancyService } from '../../company/company-vacancy/company-vacancy.service';
import { CountProgress } from '../../user/profile/profile.model';
import { ProfileService } from '../../user/profile/profile.service';
import { VacancyService } from '../vacancy/vacancy.service';
import { TrxUserVacancy } from './vacancy-detail.model';

@Component({
  selector: 'app-vacancy-detail',
  templateUrl: './vacancy-detail.component.html',
  styleUrls: ['./vacancy-detail.component.scss']
})
export class VacancyDetailComponent implements OnInit {
  vacancy: CompanyVacancy;
  trxUserVacancy: TrxUserVacancy;

  countProgres: CountProgress;
  tProgress: number;

  constructor(
    private route: ActivatedRoute,
    private companyVacancyService: CompanyVacancyService,
    private currencyPipe: CurrencyPipe,
    private vacancyService: VacancyService,
    private profileService: ProfileService
  ) { }

  ngOnInit() {
    const vacancyId = this.route.snapshot.paramMap.get('id');
    this.getVacancy(vacancyId);
    this.getTrxUserVacancyByVacancy(vacancyId);
    this.getCountUserProgress();
    console.log(vacancyId);
  }

  getVacancy(ids) {
    this.companyVacancyService.getVacancy({
      id: ids
    })
    .subscribe(
        (res: HttpResponse<CompanyVacancy>) => {
            this.successLoadUserVacancy(res.body);
        }
    );
  }

  private successLoadUserVacancy(data) {
    this.vacancy = data.contents;

    const tamp = this.currencyPipe.transform(this.vacancy.salaryStart);
    const tamps1 = tamp.toString().replace(/\./g, '|').replace(/\,/g, '.').replace(/\|/g, ',').replace(/\$/g, '');
    this.vacancy.startSalary = tamps1;

    const tamp2 = this.currencyPipe.transform(this.vacancy.salaryEnd);
    const tamps2 = tamp2.toString().replace(/\./g, '|').replace(/\,/g, '.').replace(/\|/g, ',').replace(/\$/g, '');
    this.vacancy.endSalary = tamps2;
  }

  getTrxUserVacancyByVacancy(ids) {
    this.vacancyService.getTrxVacancyByVacancy({
      id: ids
    })
    .subscribe(
        (res: HttpResponse<TrxUserVacancy>) => {
            this.successLoadUserVacancyByVacancy(res.body);
        }
    );
  }

  private successLoadUserVacancyByVacancy(data) {
    this.trxUserVacancy = data.contents;
  }

  public save(id: number, action: string) {
    this.trxUserVacancy = new TrxUserVacancy();
    if ( action === 'add' ) {
      this.trxUserVacancy.isSave = true;
      this.trxUserVacancy.userCompanyVacancyID = id;
      console.log('user trx: ', this.trxUserVacancy);
      this.vacancyService.add(this.trxUserVacancy).subscribe(result => {
        console.log(result);
        if (result.body.errCode === '00') {
          if (result.body.errDesc !== 'SUCCESS..') {
            // const options = { progressBar: true, extendedTimeOut: 3000, timeOut: 5000 };
            // this.toastrService.error(result.body.errDesc, 'New Password', options);
            // alert('Password ' + );
            Swal.fire('Error', result.body.errDesc, 'error');
          } else {
            // this.toastrService.success('Save success !', 'save');
            Swal.fire('Success', 'Jobs saved', 'success');
            this.getVacancy(this.trxUserVacancy.userCompanyVacancyID);
            this.getTrxUserVacancyByVacancy(this.trxUserVacancy.userCompanyVacancyID);
            this.getCountUserProgress();
          }
        } else {
          alert('error');
        }
      });
    }

    if ( action === 'apply' ) {
      this.trxUserVacancy.userCompanyVacancyID = id;
      this.trxUserVacancy.statusID = 1;
      this.vacancyService.add(this.trxUserVacancy).subscribe(result => {
        console.log(result);
        if (result.body.errCode === '00') {
          if (result.body.errDesc !== 'SUCCESS..') {
            // const options = { progressBar: true, extendedTimeOut: 3000, timeOut: 5000 };
            // this.toastrService.error(result.body.errDesc, 'New Password', options);
            // alert('Password ' + );
            Swal.fire('Error', result.body.errDesc, 'error');
          } else {
            // this.toastrService.success('Save success !', 'save');
            Swal.fire('Success', 'Jobs applied', 'success');
          }
        } else {
          alert('error');
        }
      });
    }
  }

  getCountUserProgress() {
    this.profileService.getCountUserProgress()
        .subscribe(
            (res: HttpResponse<CountProgress>) => {
                console.log('res.body', res.body);
                this.successLoadCountUserProgress(res.body);
            }
        );
  }

  private successLoadCountUserProgress(data) {
    console.log('succesloaduser ', data);
    this.countProgres = data.contents;
    this.tProgress = (+this.countProgres / 4) * 100;
    console.log('count progress: ', this.tProgress);
  }

}
