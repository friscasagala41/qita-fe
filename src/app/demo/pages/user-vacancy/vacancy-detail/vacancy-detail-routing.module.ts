import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VacancyDetailComponent } from './vacancy-detail.component';


const routes: Routes = [
  {
    path: '',
    component: VacancyDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VacancyDetailRoutingModule { }
