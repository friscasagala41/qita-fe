export class TrxUserVacancy {
    constructor(
        public id?: number,
        public userSeekerID?: number,
        public userCompanyVacancyID?: number,
        public isSave?: boolean,
        public statusID?: number,
        public errCode?: string,
        public errDesc?: string
    ) {}
}
