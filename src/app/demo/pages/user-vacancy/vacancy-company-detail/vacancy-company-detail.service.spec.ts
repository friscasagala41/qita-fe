/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { VacancyCompanyDetailService } from './vacancy-company-detail.service';

describe('Service: VacancyCompanyDetail', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VacancyCompanyDetailService]
    });
  });

  it('should ...', inject([VacancyCompanyDetailService], (service: VacancyCompanyDetailService) => {
    expect(service).toBeTruthy();
  }));
});
