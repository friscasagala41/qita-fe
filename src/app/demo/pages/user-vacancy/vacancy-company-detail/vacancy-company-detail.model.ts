export class Jobs {
    constructor(
        public id?: number,
        public name?: string,
        public userCompanyID?: number,
        public responsibility?: string,
        public location?: string,
        public specializationName?: string,
        public salaryStart?: number,
        public salaryEnd?: number,
        public salaryShow?: boolean,
        public createdAt?: string
    ) {}
}
