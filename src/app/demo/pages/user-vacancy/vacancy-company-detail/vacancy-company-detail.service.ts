import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { CompanyProfile } from '../../company/company-profile/company-profile.model';
import { Jobs } from './vacancy-company-detail.model';

@Injectable({
  providedIn: 'root'
})
export class VacancyCompanyDetailService {
  // public subject = new Subject<any>();

  private dataSource = new BehaviorSubject<any>(new CompanyProfile());
  data = this.dataSource.asObservable();
  // data = this.subject.asObservable();
  SERVER: string;
  constructor(private http: HttpClient,
              private configService: AppConfigService) {
       this.SERVER = this.configService.getSavedServerPath();
   }

  updatedDataSelection(data: CompanyProfile) {
     this.dataSource.next(data);
  }

  getUserCompanyByVacancyID(req ?: any): Observable<HttpResponse<CompanyProfile>> {
    let newResourceUrl = null;
    let result = null;
    let id = null;
    const search = {};

    Object.keys(req).forEach((key) => {
      if (key === 'id') {
          id = req[key];
      }
  });

    newResourceUrl = this.SERVER + `/api/trx-user-vacancy/company/detail/${id}`;

    result = this.http.post<CompanyProfile>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                results => console.log('raw', results)
            )
        );
    return result;
  }

  getJobs(req?: any): Observable<HttpResponse<Jobs[]>> {
    let pageNumber = null;
    let pageCount = null;
    let newResourceUrl = null;
    let result = null;
    const search = {
        specialization: '',
        location: '',
        userCompanyID: 0
    };

    Object.keys(req).forEach((key) => {
        if (key === 'page') {
            pageNumber = req[key];
        }
        if (key === 'count') {
            pageCount = req[key];
        }
        if (key === 'specialization') {
            search.specialization = req[key];
        }
        if (key === 'location') {
          search.location = req[key];
        }
        if (key === 'userCompanyID') {
          search.userCompanyID = req[key];
        }
    });

    newResourceUrl = this.SERVER + `/api/trx-user-vacancy/jobs/filter/page/${pageNumber}/count/${pageCount}`;

    result = this.http.post<Jobs[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

}
