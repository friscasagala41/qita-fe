import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VacancyCompanyDetailComponent } from './vacancy-company-detail.component';


const routes: Routes = [
  {
    path: '',
    component: VacancyCompanyDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VacancyCompanyDetailRoutingModule { }
