import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VacancyCompanyDetailComponent } from './vacancy-company-detail.component';

describe('VacancyCompanyDetailComponent', () => {
  let component: VacancyCompanyDetailComponent;
  let fixture: ComponentFixture<VacancyCompanyDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VacancyCompanyDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VacancyCompanyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
