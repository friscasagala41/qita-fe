import { HttpResponse } from '@angular/common/http';
import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2';
import { CompanyProfile } from '../../company/company-profile/company-profile.model';
import { TrxUserVacancy } from '../vacancy-detail/vacancy-detail.model';
import { VacancyService } from '../vacancy/vacancy.service';
import { Jobs } from './vacancy-company-detail.model';
import { VacancyCompanyDetailService } from './vacancy-company-detail.service';

@Component({
  selector: 'app-vacancy-company-detail',
  templateUrl: './vacancy-company-detail.component.html',
  styleUrls: ['./vacancy-company-detail.component.css']
})
export class VacancyCompanyDetailComponent implements OnInit {
  profile = new CompanyProfile();
  specialization = '';
  location = '';
  jobs: Jobs[];
  maxTotalpage = 1;
  curPage: number;
  totalItems: any;
  itemsPerPage: any;
  previousPage: any;
  jumlah = 0;
  job = new Jobs();
  ids = '';

  trxUserVacancy: TrxUserVacancy;

  action = new Subject();

  constructor(
    private vacanCompanyService: VacancyCompanyDetailService,
    private route: ActivatedRoute,
    private router: Router,
    private el: ElementRef,
    private vacancyService: VacancyService,
  ) { }

  ngOnInit() {
    this.ids = this.route.snapshot.paramMap.get('id');
    this.getUserCompanyProfile(this.ids);
    this.curPage = 1;
    this.jobsList(1, this.ids);
  }

  getUserCompanyProfile(ids) {
    this.vacanCompanyService.getUserCompanyByVacancyID({
      id: ids
    })
    .subscribe(
        (res: HttpResponse<CompanyProfile>) => {
            this.successLoadUserCompanyProfile(res.body);
        }
    );
  }

  private successLoadUserCompanyProfile(data) {
    this.profile = data.contents;
    console.log('profile: ', this.profile);
  }

  goToUrl(siteUrl) {
     window.location.href = 'https://' + siteUrl;
  }

  jobsList(curpage, ids) {
    console.log('specialization: ', this.specialization, ' locations: ', this.location);
    this.vacanCompanyService.getJobs({
        page: curpage,
        count: 3,
        specialization: this.specialization,
        location: this.location,
        userCompanyID: +ids
    })
        .subscribe(
            (res: HttpResponse<Jobs[]>) => {
                console.log('res.body', res.body);
                this.successLoadJobs(res.body);

            }
        );
  }

  private successLoadJobs(data) {
    const totalRecord = 3;
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log('jobs: ', data);
    this.jobs = data.contents;

    if (this.jobs.length === 0) {
      this.job = new Jobs();
    }

    this.jumlah = data.totalRow;
    this.maxTotalpage = Math.ceil(data.totalRow / totalRecord);
    this.totalItems = data.totalRow;
    this.itemsPerPage = totalRecord;

    if (this.jobs.length > 0) {
      this.job = this.jobs[0];
    }
  }

  detail(job: Jobs) {
    this.job = job;
  }

  changeSpecialization(event) {
    console.log('event: ', event);
    this.jobsList(1, this.ids);
  }

  changeLocation(event) {
    console.log('event: ', event);
    this.jobsList(1, this.ids);
  }

  loadPage(curPage) {
      if (this.curPage !== this.previousPage) {
        this.previousPage = this.curPage;
        this.jobsList(this.curPage, this.ids);
      }
  }

  public save(id: number, action: string) {
    this.trxUserVacancy = new TrxUserVacancy();
    if ( action === 'save' ) {
      this.trxUserVacancy.isSave = true;
      this.trxUserVacancy.userCompanyVacancyID = id;
      console.log('user trx: ', this.trxUserVacancy);
      this.vacancyService.add(this.trxUserVacancy).subscribe(result => {
        console.log(result);
        if (result.body.errCode === '00') {
          if (result.body.errDesc !== 'SUCCESS..') {
            // const options = { progressBar: true, extendedTimeOut: 3000, timeOut: 5000 };
            // this.toastrService.error(result.body.errDesc, 'New Password', options);
            // alert('Password ' + );
            Swal.fire('Error', result.body.errDesc, 'error');
          } else {
            // this.toastrService.success('Save success !', 'save');
            Swal.fire('Success', 'Jobs saved', 'success');
            this.action.next('refresh');
          }
        } else {
          alert('error');
        }
      });
    }

    if ( action === 'apply' ) {
      this.trxUserVacancy.userCompanyVacancyID = id;
      this.trxUserVacancy.statusID = 1;
      this.vacancyService.add(this.trxUserVacancy).subscribe(result => {
        console.log(result);
        if (result.body.errCode === '00') {
          if (result.body.errDesc !== 'SUCCESS..') {
            // const options = { progressBar: true, extendedTimeOut: 3000, timeOut: 5000 };
            // this.toastrService.error(result.body.errDesc, 'New Password', options);
            // alert('Password ' + );
            Swal.fire('Error', result.body.errDesc, 'error');
          } else {
            // this.toastrService.success('Save success !', 'save');
            Swal.fire('Success', 'Jobs applied', 'success');
            this.action.next('refresh');
          }
        } else {
          alert('error');
        }
      });
    }
  }

}
