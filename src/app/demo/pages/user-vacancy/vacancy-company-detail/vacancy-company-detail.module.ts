import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VacancyCompanyDetailRoutingModule } from './vacancy-company-detail-routing.module';
import { VacancyCompanyDetailComponent } from './vacancy-company-detail.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [VacancyCompanyDetailComponent],
  imports: [
    CommonModule,
    VacancyCompanyDetailRoutingModule,
    SharedModule,
    NgbPaginationModule
  ]
})
export class VacancyCompanyDetailModule { }
