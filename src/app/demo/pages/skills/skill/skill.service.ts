import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { Skill } from './skill.model';

@Injectable({
  providedIn: 'root'
})
export class SkillService {

  // public subject = new Subject<any>();

  private dataSource = new BehaviorSubject<any>(new Skill());
  data = this.dataSource.asObservable();
  // data = this.subject.asObservable();
  SERVER: string;
  constructor(private http: HttpClient,
              private configService: AppConfigService) {
        this.SERVER = this.configService.getSavedServerPath();
    }

  updatedDataSelection(data: Skill) {
      this.dataSource.next(data);
  }

  filter(req?: any): Observable<HttpResponse<Skill[]>> {
    let pageNumber = null;
    let pageCount = null;
    let newResourceUrl = null;
    let result = null;
    const search = {
        name: ''
    };

    Object.keys(req).forEach((key) => {
        if (key === 'page') {
            pageNumber = req[key];
        }
        if (key === 'count') {
            pageCount = req[key];
        }
        if (key === 'name') {
            search.name = req[key];
        }
    });

    newResourceUrl = this.SERVER + `/api/skill/filter/page/${pageNumber}/count/${pageCount}`;

    result = this.http.post<Skill[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  getAllSkill(): Observable<HttpResponse<Skill[]>> {
      let newResourceUrl = null;
      let result = null;
      const search = {};

      newResourceUrl = this.SERVER + `/api/skill/all`;

      result = this.http.post<Skill[]>(newResourceUrl, search, { observe: 'response' })
          .pipe(
              tap(
                  // console.log('sdad')
                  results => console.log('raw', results)
              )
          );
      // console.log(result);
      return result;
  }

  add(skill: Skill): Observable<HttpResponse<Skill>> {
    const copy = Object.assign({}, skill);

    copy.id = +copy.id;

    const newResourceUrl = this.SERVER + `/api/skill/`;

    return this.http.post<Skill>(newResourceUrl, copy, { observe: 'response'})
        .pipe(map((res: HttpResponse<Skill>) => Object.assign({}, res)));
  }

  getSkill(req ?: any): Observable<HttpResponse<Skill>> {
    let newResourceUrl = null;
    let result = null;
    let id = null;
    const search = {};

    Object.keys(req).forEach((key) => {
      if (key === 'id') {
          id = req[key];
      }
  });

    newResourceUrl = this.SERVER + `/api/skill/${id}`;

    result = this.http.post<Skill[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  update(skill: Skill): Observable<HttpResponse<Skill>> {
    const copy = Object.assign({}, skill);

    copy.id = + copy.id;

    const newResourceUrl = this.SERVER + `/api/skill/`;

    return this.http.put<Skill>(newResourceUrl, copy, { observe: 'response' })
        .pipe(map((res: HttpResponse<Skill>) => Object.assign({}, res)));
  }

  skills(): Observable<HttpResponse<Skill[]>> {
    let newResourceUrl = null;
    let result = null;

    newResourceUrl = this.SERVER + `/api/skill`;
    result = this.http.get<Skill[]>(newResourceUrl, { observe: 'response' })
        .pipe(tap(
            results => console.log('raw =>', results)
        )
        );
    return result;
  }

}
