import { HttpResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MDBModalRef, MDBModalService } from 'ng-uikit-pro-standard';
import { Subject } from 'rxjs';
import { TOTAL_RECORD_PER_PAGE } from 'src/app/theme/shared/constant/base.constant';
import { SkillModalComponent } from '../skill-modal/skill-modal.component';
import { Skill } from './skill.model';
import { SkillService } from './skill.service';

@Component({
  selector: 'app-skill',
  templateUrl: './skill.component.html',
  styleUrls: ['./skill.component.scss']
})
export class SkillComponent implements OnInit, OnDestroy {
  searchName;
  skills: Skill[];

  maxTotalpage = 1;
  curPage: number;
  totalItems: any;
  itemsPerPage: any;
  previousPage: any;
  // total = [];
  nameSkill = '';
  description: string;
  skill: Skill = new Skill();
  skillForm: FormGroup;
  action = new Subject();
  closeResult: string;
  modalRef: MDBModalRef;

  constructor(
    private skillService: SkillService,
    private fb: FormBuilder,
    private modalService: MDBModalService,
  ) { }

  ngOnInit() {
    this.curPage = 1;
    this.filter(this.curPage);
  }

  rForm() {
    this.skillForm = this.fb.group({
      nameSkill: ['']
    });
  }


  filter(curpage) {
    console.log('searc ', this.searchName);
    this.skillService.filter({
        page: curpage,
        count: 10,
        name: this.searchName,
    })
        .subscribe(
            (res: HttpResponse<Skill[]>) => {
                console.log('res.body', res.body);
                this.successLoadUser(res.body);

            }
        );
  }

  private successLoadUser(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.skills = data.contents;
    this.maxTotalpage = Math.ceil(data.totalRow / TOTAL_RECORD_PER_PAGE);
    this.totalItems = data.totalRow;
    this.itemsPerPage = TOTAL_RECORD_PER_PAGE;
  }

  ngOnDestroy() {
    // this.userService.clearMessages();
  }

  loadPage(curPage) {
      if (this.curPage !== this.previousPage) {
        this.previousPage = this.curPage;
        this.filter(this.curPage);
        console.log('filter', this.filter);
      }
  }

  open(status: string, data: Skill) {

    if (status === 'Add') {
        data = new Skill();
        data.id = 0;
        data.name = '';
    }
    console.log('isi dataaa ', data);

    this.skillService.updatedDataSelection(data);

    const modalOptions = {
        backdrop: true,
        keyboard: true,
        focus: true,
        show: false,
        ignoreBackdropClick: false,
        class: 'modal-lg',
        containerClass: 'right',
        animated: true,
        data: {
            objedit: data,
            uneditable: false
        }
    };

    if (status === 'detail') {
        modalOptions.data.uneditable = true;
    }

    // console.log()
    this.modalRef = this.modalService.show(SkillModalComponent, modalOptions);
    this.modalRef.content.action.subscribe(
        (result: any) => {
            console.log(result);
            if (result === 'refresh') {
                this.curPage = 1;
                this.filter(this.curPage);
            }
        });
  }

}
