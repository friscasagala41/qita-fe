import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SkillModalComponent } from './skill-modal.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SkillModalComponent]
})
export class SkillModalModule { }
