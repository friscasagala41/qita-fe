/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CareerLevelService } from './career-level.service';

describe('Service: CareerLevel', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CareerLevelService]
    });
  });

  it('should ...', inject([CareerLevelService], (service: CareerLevelService) => {
    expect(service).toBeTruthy();
  }));
});
