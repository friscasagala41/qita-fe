import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CareerLevelComponent } from './career-level.component';


const routes: Routes = [
  {
    path: '',
    component: CareerLevelComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CareerLevelRoutingModule { }
