import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { CareerLevel } from './career-level.model';

@Injectable({
  providedIn: 'root'
})
export class CareerLevelService {
  // public subject = new Subject<any>();

  private dataSource = new BehaviorSubject<any>(new CareerLevel());
  data = this.dataSource.asObservable();
  // data = this.subject.asObservable();
  SERVER: string;
  constructor(private http: HttpClient,
              private configService: AppConfigService) {
        this.SERVER = this.configService.getSavedServerPath();
    }

  updatedDataSelection(data: CareerLevel) {
      this.dataSource.next(data);
  }

  filter(req?: any): Observable<HttpResponse<CareerLevel[]>> {
    let pageNumber = null;
    let pageCount = null;
    let newResourceUrl = null;
    let result = null;
    const search = {
        name: ''
    };

    Object.keys(req).forEach((key) => {
        if (key === 'page') {
            pageNumber = req[key];
        }
        if (key === 'count') {
            pageCount = req[key];
        }
        if (key === 'name') {
            search.name = req[key];
        }
    });

    newResourceUrl = this.SERVER + `/api/career-level/filter/page/${pageNumber}/count/${pageCount}`;

    result = this.http.post<CareerLevel[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  getAllCareerLevel(): Observable<HttpResponse<CareerLevel[]>> {
      let newResourceUrl = null;
      let result = null;
      const search = {};

      newResourceUrl = this.SERVER + `/api/career-level/all`;

      result = this.http.post<CareerLevel[]>(newResourceUrl, search, { observe: 'response' })
          .pipe(
              tap(
                  // console.log('sdad')
                  results => console.log('raw', results)
              )
          );
      // console.log(result);
      return result;
  }

  add(careerLevel: CareerLevel): Observable<HttpResponse<CareerLevel>> {
    const copy = Object.assign({}, careerLevel);

    copy.id = +copy.id;

    const newResourceUrl = this.SERVER + `/api/career-level/`;

    return this.http.post<CareerLevel>(newResourceUrl, copy, { observe: 'response'})
        .pipe(map((res: HttpResponse<CareerLevel>) => Object.assign({}, res)));
  }

  getCareerLevel(req ?: any): Observable<HttpResponse<CareerLevel>> {
    let newResourceUrl = null;
    let result = null;
    let id = null;
    const search = {};

    Object.keys(req).forEach((key) => {
      if (key === 'id') {
          id = req[key];
      }
  });

    newResourceUrl = this.SERVER + `/api/career-level/${id}`;

    result = this.http.post<CareerLevel[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  update(careerLevel: CareerLevel): Observable<HttpResponse<CareerLevel>> {
    const copy = Object.assign({}, careerLevel);

    copy.id = + copy.id;

    const newResourceUrl = this.SERVER + `/api/career-level/`;

    return this.http.put<CareerLevel>(newResourceUrl, copy, { observe: 'response' })
        .pipe(map((res: HttpResponse<CareerLevel>) => Object.assign({}, res)));
  }
}
