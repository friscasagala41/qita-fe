import { HttpResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MDBModalRef, MDBModalService } from 'ng-uikit-pro-standard';
import { Subject } from 'rxjs';
import { TOTAL_RECORD_PER_PAGE } from 'src/app/theme/shared/constant/base.constant';
import { CareerLevelModalComponent } from '../career-level-modal/career-level-modal.component';
import { CareerLevel } from './career-level.model';
import { CareerLevelService } from './career-level.service';

@Component({
  selector: 'app-career-level',
  templateUrl: './career-level.component.html',
  styleUrls: ['./career-level.component.scss']
})
export class CareerLevelComponent implements OnInit, OnDestroy {
  searchName;
  careerLevels: CareerLevel[];

  maxTotalpage = 1;
  curPage: number;
  totalItems: any;
  itemsPerPage: any;
  previousPage: any;
  // total = [];
  nameCareer = '';
  description: string;
  careerLevel: CareerLevel = new CareerLevel();
  careerLevelForm: FormGroup;
  action = new Subject();
  closeResult: string;
  modalRef: MDBModalRef;

  constructor(
    private careerLevelService: CareerLevelService,
    private fb: FormBuilder,
    private modalService: MDBModalService,
  ) { }

  ngOnInit() {
    this.curPage = 1;
    this.filter(this.curPage);
  }

  rForm() {
    this.careerLevelForm = this.fb.group({
      nameCareer: ['']
    });
  }


  filter(curpage) {
    console.log('searc ', this.searchName);
    this.careerLevelService.filter({
        page: curpage,
        count: 10,
        name: this.searchName,
    })
        .subscribe(
            (res: HttpResponse<CareerLevel[]>) => {
                console.log('res.body', res.body);
                this.successLoadUser(res.body);

            }
        );
  }

  private successLoadUser(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.careerLevels = data.contents;
    this.maxTotalpage = Math.ceil(data.totalRow / TOTAL_RECORD_PER_PAGE);
    this.totalItems = data.totalRow;
    this.itemsPerPage = TOTAL_RECORD_PER_PAGE;
  }

  ngOnDestroy() {
    // this.userService.clearMessages();
  }

  loadPage(curPage) {
      if (this.curPage !== this.previousPage) {
        this.previousPage = this.curPage;
        this.filter(this.curPage);
        console.log('filter', this.filter);
      }
  }

  open(status: string, data: CareerLevel) {

    if (status === 'Add') {
        data = new CareerLevel();
        data.id = 0;
        data.name = '';
    }
    console.log('isi dataaa ', data);

    this.careerLevelService.updatedDataSelection(data);

    const modalOptions = {
        backdrop: true,
        keyboard: true,
        focus: true,
        show: false,
        ignoreBackdropClick: false,
        class: 'modal-lg',
        containerClass: 'right',
        animated: true,
        data: {
            objedit: data,
            uneditable: false
        }
    };

    if (status === 'detail') {
        modalOptions.data.uneditable = true;
    }

    // console.log()
    this.modalRef = this.modalService.show(CareerLevelModalComponent, modalOptions);
    this.modalRef.content.action.subscribe(
        (result: any) => {
            console.log(result);
            if (result === 'refresh') {
                this.curPage = 1;
                this.filter(this.curPage);
            }
        });
  }

}
