import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CareerLevelsRoutingModule } from './career-levels-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CareerLevelsRoutingModule
  ]
})
export class CareerLevelsModule { }
