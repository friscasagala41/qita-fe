import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MDBModalRef, MDBModalService, ToastService } from 'ng-uikit-pro-standard';
import { Subject, Subscription } from 'rxjs';
import Swal from 'sweetalert2';
import { CareerLevel } from '../career-level/career-level.model';
import { CareerLevelService } from '../career-level/career-level.service';

@Component({
  selector: 'app-career-level-modal',
  templateUrl: './career-level-modal.component.html',
  styleUrls: ['./career-level-modal.component.scss']
})
export class CareerLevelModalComponent implements OnInit {
  objedit: any;
  action = new Subject();
  careerLevel: CareerLevel;
  messages;
  subscription: Subscription;
  dataFromGrid: MDBModalRef;
  nama: string;
  uneditable: boolean;
  careerLevelForm: FormGroup;

  constructor(
    private modalService: MDBModalService,
    public modalRef: MDBModalRef,
    public careerLevelService: CareerLevelService,
    private toastrService: ToastService,
    private fb: FormBuilder
  ) {
    this.rForm();
  }

  rForm() {
    this.careerLevelForm = this.fb.group({
      id: [{value: '', disabled: true}],
      name: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.subscription = this.careerLevelService.data.subscribe(data => {
      console.log('data behavierrrr', data);
      this.careerLevel = data;
    });
    console.log('on init modal ');
    console.log('user modal ', this.objedit);
  }

  closeForm() {
    this.action.next('refresh');
    this.modalService._hideModal(1);
  }

  save() {
    this.careerLevel.name = this.careerLevelForm.get('name').value;
    console.log('save ==> ', this.careerLevel);
    this.careerLevelService.add(this.careerLevel).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
        if (result.body.errDesc !== 'SUCCESS..') {
          // const options = { progressBar: true, extendedTimeOut: 3000, timeOut: 5000 };
          // this.toastrService.error(result.body.errDesc, 'New Password', options);
          // alert('Password ' + );
          Swal.fire('Error', result.body.errDesc, 'error');
        } else {
          // this.toastrService.success('Save success !', 'save');
          Swal.fire('Success', 'Career Level saved', 'success');
        }
        this.action.next('refresh');
        this.closeForm();
      } else {
        alert('error');
      }
    });
  }

  edit() {
    console.log('update ==> ', this.careerLevel);
    this.careerLevelService.update(this.careerLevel).subscribe(result => {
      console.log(result);
      // if (result.body.errCode !== '00') {
      if (result.body.errCode !== '00') {
        // const options = { progressBar: true, extendedTimeOut: 3000, timeOut: 5000 };
        // this.toastrService.error(result.body.errDesc, 'New Password', options);
        Swal.fire('Error', result.body.errDesc, 'error');
        // alert('Password ' + );
      } else {
        // this.toastrService.success('Update success !');
        Swal.fire('Success', 'Career Level saved', 'success');
      }
      this.action.next('refresh');
      this.closeForm();
      // } else {
      //   alert('error');
      // }
    });
  }

}
