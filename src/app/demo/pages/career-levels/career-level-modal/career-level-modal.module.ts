import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CareerLevelModalComponent } from './career-level-modal.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CareerLevelModalComponent]
})
export class CareerLevelModalModule { }
