/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AuthSigninService } from './auth-signin.service';

describe('Service: AuthSignin', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthSigninService]
    });
  });

  it('should ...', inject([AuthSigninService], (service: AuthSigninService) => {
    expect(service).toBeTruthy();
  }));
});
