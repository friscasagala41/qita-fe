import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { AuthServerProvider } from 'src/app/theme/shared/auth-jwt/auth-jwt.service';
import { Principal } from 'src/app/theme/shared/auth-jwt/principal.service';
import { AuthSignin } from './auth-signin.model';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

export type EntityResponseType = HttpResponse<AuthSignin>;

@Injectable({
  providedIn: 'root'
})
export class AuthSigninService {

  SERVER: string;
  // private resourceUrl = SERVER + '/auth';

  constructor(
      // private languageService: JhiLanguageService,
      private principal: Principal,
      private router: Router,
      private http: HttpClient,
      private authServerProvider: AuthServerProvider,
      private configService: AppConfigService, ) {
          this.SERVER = this.configService.getSavedServerPath();
      }

  login(credentials): Observable<EntityResponseType> {
      const copy = this.convert(credentials);

      return this.http.post<AuthSignin>(`${this.SERVER}/auth/login`, copy, { observe: 'response'})
          .pipe(map((res: EntityResponseType) => this.convertResponse(res)));
  }

  private convert(login: AuthSignin): AuthSignin {
    const copy: AuthSignin = Object.assign({}, login);
    return copy;
  }

  private convertResponse(res: EntityResponseType): EntityResponseType {
      const body: AuthSignin =  Object.assign({}, res.body);
      return res.clone({body});
  }

  logout() {
    console.log('logout service..');
    localStorage.removeItem('token');
    this.authServerProvider.logout().subscribe();
    // this.principal.authenticate(null);

    // Temporary solution, still didn't know how to back to login page
    this.router.navigate(['']);
  }

  getUserDetail(): Observable<EntityResponseType> {
    return this.http.post<AuthSignin>(`${this.SERVER}/api/user/detail`, '', { observe: 'response'})
        .pipe(map((res: EntityResponseType) => this.convertResponse(res)));
  }

}
