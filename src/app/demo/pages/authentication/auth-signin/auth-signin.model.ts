export class AuthSignin  {
    constructor(
        public username?: string,
        public password?: string,
        public errCode?: string,
        public errDesc?: string,
        public token?: string,
        public contents?: number
    ) {
    }
}
