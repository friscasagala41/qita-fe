/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AuthSignupService } from './auth-signup.service';

describe('Service: AuthSignup', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthSignupService]
    });
  });

  it('should ...', inject([AuthSignupService], (service: AuthSignupService) => {
    expect(service).toBeTruthy();
  }));
});
