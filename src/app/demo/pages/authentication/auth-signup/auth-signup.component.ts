import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Role } from '../../roles/role/role.model';
import { AuthSignUp } from './auth-signup.model';
import { AuthSignupService } from './auth-signup.service';

@Component({
  selector: 'app-auth-signup',
  templateUrl: './auth-signup.component.html',
  styleUrls: ['./auth-signup.component.scss']
})
export class AuthSignupComponent implements OnInit {

  roleId = '';
  username = '';
  email = '';
  password = '';
  authSign: AuthSignUp = new AuthSignUp();

  roles: Role[];
  rolesOption = [];

  constructor(
    private router: Router,
    private authSignUpService: AuthSignupService
  ) { }

  ngOnInit() {
    this.getAllRoles();
  }

  signin() {
    this.router.navigate(['']);
  }

  register() {
    this.authSign.userName = this.username;
    this.authSign.password = this.password;
    this.authSign.email = this.email;
    this.authSign.roleId = +this.roleId;

    console.log('authSign : ', this.authSign);

    this.authSignUpService.add(this.authSign).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
        if (result.body.errDesc !== 'SUCCESS..') {
          // const options = { progressBar: true, extendedTimeOut: 3000, timeOut: 5000 };
          // this.toastrService.error(result.body.errDesc, 'New Password', options);
          // alert('Password ' + );
          Swal.fire('Error', result.body.errDesc, 'error');
        } else {
          // this.toastrService.success('Save success !', 'save');
          Swal.fire('Success', result.body.errDesc, 'success');
          this.router.navigate(['']);
        }
        // window.location.reload();
      } else {
        alert('error');
      }
    });
  }

  changeRole() {
    console.log('event: ', this.roleId);
    this.roleId = this.roleId;
  }

  getAllRoles() {
    this.authSignUpService.getAllRole()
    .subscribe(
        (res: HttpResponse<Role[]>) => {
            this.successLoadUserRole(res.body);
        }
    );
  }

  private successLoadUserRole(data) {
    this.roles = data.contents;
    // this.userSkillEditForm.get('skillID').setValue(this.userSkill.skillID);
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.roles.length; i++) {
      this.rolesOption.push({value: this.roles[i].id, name: this.roles[i].description});
    }

    console.log(this.rolesOption);
  }

}
