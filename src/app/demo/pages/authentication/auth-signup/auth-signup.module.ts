import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthSignupRoutingModule } from './auth-signup-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AuthSignupRoutingModule
  ],
  declarations: []
})
export class AuthSignupModule { }
