import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { AuthSignUp } from './auth-signup.model';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Role } from '../../roles/role/role.model';

@Injectable({
  providedIn: 'root'
})
export class AuthSignupService {

  // public subject = new Subject<any>();

  private dataSource = new BehaviorSubject<any>(new AuthSignUp());
  data = this.dataSource.asObservable();
  // data = this.subject.asObservable();
  SERVER: string;
  constructor(private http: HttpClient,
              private configService: AppConfigService) {
        this.SERVER = this.configService.getSavedServerPath();
    }

  updatedDataSelection(data: AuthSignUp) {
      this.dataSource.next(data);
  }

  add(role: AuthSignUp): Observable<HttpResponse<AuthSignUp>> {
    const copy = Object.assign({}, role);
    const newResourceUrl = this.SERVER + `/api/user/`;

    return this.http.post<AuthSignUp>(newResourceUrl, copy, { observe: 'response' })
        .pipe(map((res: HttpResponse<AuthSignUp>) => Object.assign({}, res)));
  }

  getAllRole(): Observable<HttpResponse<Role[]>> {
    let newResourceUrl = null;
    let result = null;
    const search = {};

    newResourceUrl = this.SERVER + `/api/role/all`;

    result = this.http.post<Role[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }
}
