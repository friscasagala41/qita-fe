import { Role } from '../../roles/role/role.model';

export class AuthSignUp  {
    constructor(
        public userName?: string,
        public password?: string,
        public email?: string,
        public roleId?: number,
        public isAdmin?: number,
        public role?: Role,
        public id?: number,
        public errCode?: string,
        public errDesc?: string,
        public token?: string,
        public firstName?: number,
        public lastName?: number,
        public status?: number,
    ) {
    }
}
