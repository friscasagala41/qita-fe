import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailApplicationRoutingModule } from './detail-application-routing.module';
import { DetailApplicationComponent } from './detail-application.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';


@NgModule({
  declarations: [DetailApplicationComponent],
  imports: [
    CommonModule,
    DetailApplicationRoutingModule,
    SharedModule
  ]
})
export class DetailApplicationModule { }
