import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2';
import { Experience } from '../../user/experience/experience.model';
import { UserSkill } from '../../user/skill/skill.model';
import { ProcessState, SeekerAppDetail } from '../application-list/applicatio-list.model';
import { ApplicationListService } from '../application-list/application-list.service';

@Component({
  selector: 'app-detail-application',
  templateUrl: './detail-application.component.html',
  styleUrls: ['./detail-application.component.scss']
})
export class DetailApplicationComponent implements OnInit {
  seeker: SeekerAppDetail;
  experiences: Experience[];
  skills: UserSkill[];
  status = 0;
  vacancy = 0;
  action = new Subject();
  processState = new ProcessState();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private applicantService: ApplicationListService
  ) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    const status = this.route.snapshot.paramMap.get('status');
    const vacancy = this.route.snapshot.paramMap.get('vacancy');
    this.status = +status;
    this.vacancy = +vacancy;
    console.log('status: ', this.status, ' vacancy: ', this.vacancy);
    this.getDetail(id);
    this.getExperience(id);
    this.getSeekerSkill(id);
  }

  back() {
    this.router.navigate(['/applicant-list']);
  }

  getDetail(ids) {
    this.applicantService.getDetail({
      id: ids
    })
        .subscribe(
            (res: HttpResponse<SeekerAppDetail>) => {
                console.log('res.body', res.body);
                this.successLoadUser(res.body);
            }
        );
  }

  private successLoadUser(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.seeker = data.contents;
    console.log('seekers: ', this.seeker);
  }

  getExperience(ids) {
    this.applicantService.getSeekerExperience({
      id: ids
    })
        .subscribe(
            (res: HttpResponse<Experience[]>) => {
                console.log('res.body', res.body);
                this.successLoadExperience(res.body);
            }
        );
  }

  private successLoadExperience(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.experiences = data.contents;
    console.log('seekers: ', this.seeker);
  }

  getSeekerSkill(ids) {
    this.applicantService.getSeekerSkill({
      id: ids
    })
        .subscribe(
            (res: HttpResponse<UserSkill[]>) => {
                console.log('res.body', res.body);
                this.successLoadSkill(res.body);
            }
        );
  }

  private successLoadSkill(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.skills = data.contents;
    console.log('skills: ', this.skills);
  }

  submission(status, userid, vacancyid) {
    this.processState.statusID = status;
    this.processState.userSeekerID = userid;
    this.processState.userCompanyVacancyID = vacancyid;

    this.applicantService.processState(this.processState).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
            Swal.fire('Success', 'status application updated', 'success');
            this.action.next('refresh');
      } else {
          Swal.fire('Failed', 'skill not saved', 'error');
      }
      this.action.next('refresh');
    });
  }

  docReview(status, userid, vacancyid) {
    this.processState.statusID = status;
    this.processState.userSeekerID = userid;
    this.processState.userCompanyVacancyID = vacancyid;

    this.applicantService.processState(this.processState).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
            Swal.fire('Success', 'status application updated', 'success');
            this.action.next('refresh');
      } else {
          Swal.fire('Failed', 'status application not updated', 'error');
      }
      this.action.next('refresh');
    });
  }

  evalution(status, userid, vacancyid) {
    this.processState.statusID = status;
    this.processState.userSeekerID = userid;
    this.processState.userCompanyVacancyID = vacancyid;

    this.applicantService.processState(this.processState).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
            Swal.fire('Success', 'sstatus application updated', 'success');
            this.action.next('refresh');
      } else {
          Swal.fire('Failed', 'status application not updated', 'error');
      }
      this.action.next('refresh');
    });
  }

  interview(status, userid, vacancyid) {
    this.processState.statusID = status;
    this.processState.userSeekerID = userid;
    this.processState.userCompanyVacancyID = vacancyid;

    this.applicantService.processState(this.processState).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
            Swal.fire('Success', 'status application updated', 'success');
            this.action.next('refresh');
      } else {
          Swal.fire('Failed', 'status application not updated', 'error');
      }
      this.action.next('refresh');
    });
  }

  reject(status, userid, vacancyid) {
    this.processState.statusID = status;
    this.processState.userSeekerID = userid;
    this.processState.userCompanyVacancyID = vacancyid;

    this.applicantService.processState(this.processState).subscribe(result => {
      console.log(result);
      if (result.body.errCode === '00') {
            Swal.fire('Success', 'status application updated', 'success');
            this.action.next('refresh');
      } else {
          Swal.fire('Failed', 'status application not updated', 'error');
      }
      this.action.next('refresh');
    });
  }

}
