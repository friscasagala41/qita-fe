import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailApplicationComponent } from './detail-application.component';


const routes: Routes = [
  {
    path: '',
    component: DetailApplicationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailApplicationRoutingModule { }
