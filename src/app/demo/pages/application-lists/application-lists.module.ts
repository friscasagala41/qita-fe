import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplicationListsRoutingModule } from './application-lists-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ApplicationListsRoutingModule
  ]
})
export class ApplicationListsModule { }
