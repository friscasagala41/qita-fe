import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./application-list/application-list.module').then(module => module.ApplicationListModule)
      },
      {
        path: 'save/:id',
        loadChildren: () => import('./save-application/save-application.module').then(module => module.SaveApplicationModule)
      },
      {
        path: 'apply/:id',
        loadChildren: () => import('./applied-application/applied-application.module').then(module => module.AppliedApplicationModule)
      },
      {
        path: 'detail/:id/:status/:vacancy',
        loadChildren: () => import('./detail-application/detail-application.module').then(module => module.DetailApplicationModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationListsRoutingModule { }
