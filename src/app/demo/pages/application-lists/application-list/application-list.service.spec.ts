/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ApplicationListService } from './application-list.service';

describe('Service: ApplicationList', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApplicationListService]
    });
  });

  it('should ...', inject([ApplicationListService], (service: ApplicationListService) => {
    expect(service).toBeTruthy();
  }));
});
