import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplicationListRoutingModule } from './application-list-routing.module';
import { ApplicationListComponent } from './application-list.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';


@NgModule({
  declarations: [ApplicationListComponent],
  imports: [
    CommonModule,
    ApplicationListRoutingModule,
    SharedModule
  ]
})
export class ApplicationListModule { }
