import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { Experience } from '../../user/experience/experience.model';
import { UserSkill } from '../../user/skill/skill.model';
import { ApplicationList, ProcessState, SeekerAppDetail, SeekerApplicant } from './applicatio-list.model';

@Injectable({
  providedIn: 'root'
})
export class ApplicationListService {

  // public subject = new Subject<any>();

  private dataSource = new BehaviorSubject<any>(new ApplicationList());
  data = this.dataSource.asObservable();
  // data = this.subject.asObservable();
  SERVER: string;
  constructor(private http: HttpClient,
              private configService: AppConfigService) {
        this.SERVER = this.configService.getSavedServerPath();
    }

  updatedDataSelection(data: ApplicationList) {
      this.dataSource.next(data);
  }

  getAllTrxVacancy(): Observable<HttpResponse<ApplicationList[]>> {
      let newResourceUrl = null;
      let result = null;
      const search = {};

      newResourceUrl = this.SERVER + `/api/trx-user-vacancy/all`;

      result = this.http.post<ApplicationList[]>(newResourceUrl, search, { observe: 'response' })
          .pipe(
              tap(
                  // console.log('sdad')
                  results => console.log('raw', results)
              )
          );
      // console.log(result);
      return result;
  }

  getAllApplicantSave(req ?: any): Observable<HttpResponse<SeekerApplicant[]>> {
    let newResourceUrl = null;
    let result = null;
    let id = null;
    const search = {
        kelurahan : ''
    };

    Object.keys(req).forEach((key) => {
      if (key === 'id') {
          id = req[key];
      }

      if (key === 'kelurahan') {
        search.kelurahan = req[key];
      }
  });

    newResourceUrl = this.SERVER + `/api/trx-user-vacancy/save/applicant/${id}`;

    result = this.http.post<SeekerApplicant[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  getDetail(req ?: any): Observable<HttpResponse<SeekerAppDetail>> {
    let newResourceUrl = null;
    let result = null;
    let id = null;
    const search = {};

    Object.keys(req).forEach((key) => {
      if (key === 'id') {
          id = req[key];
      }
  });

    newResourceUrl = this.SERVER + `/api/trx-user-vacancy/detail/applicant/${id}`;

    result = this.http.post<SeekerAppDetail>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  getSeekerExperience(req ?: any): Observable<HttpResponse<Experience[]>> {
    let newResourceUrl = null;
    let result = null;
    let id = null;
    const search = {};

    Object.keys(req).forEach((key) => {
      if (key === 'id') {
          id = req[key];
      }
  });

    newResourceUrl = this.SERVER + `/api/trx-user-vacancy/seeker/experience/${id}`;

    result = this.http.post<Experience[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  getSeekerSkill(req ?: any): Observable<HttpResponse<UserSkill[]>> {
    let newResourceUrl = null;
    let result = null;
    let id = null;
    const search = {};

    Object.keys(req).forEach((key) => {
      if (key === 'id') {
          id = req[key];
      }
  });

    newResourceUrl = this.SERVER + `/api/trx-user-vacancy/seeker/skill/${id}`;

    result = this.http.post<UserSkill[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  getAllApplicantApply(req ?: any): Observable<HttpResponse<SeekerApplicant[]>> {
    let newResourceUrl = null;
    let result = null;
    let id = null;
    let status = null;
    const search = {
        kelurahan: ''
    };

    Object.keys(req).forEach((key) => {
      if (key === 'id') {
          id = req[key];
      }
      if (key === 'status') {
        status = req[key];
      }
      if (key === 'kelurahan') {
          search.kelurahan = req[key];
      }
    });

    newResourceUrl = this.SERVER + `/api/trx-user-vacancy/apply/applicant/${id}/${status}`;

    result = this.http.post<SeekerApplicant[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

    processState(processState: ProcessState): Observable<HttpResponse<ProcessState>> {
        const copy = Object.assign({}, processState);

        copy.userSeekerID = +copy.userSeekerID;
        copy.userCompanyVacancyID = +copy.userCompanyVacancyID;
        copy.statusID = +copy.statusID;

        const newResourceUrl = this.SERVER + `/api/trx-user-vacancy/process`;

        return this.http.post<ProcessState>(newResourceUrl, copy, { observe: 'response'})
            .pipe(map((res: HttpResponse<ProcessState>) => Object.assign({}, res)));
    }

}
