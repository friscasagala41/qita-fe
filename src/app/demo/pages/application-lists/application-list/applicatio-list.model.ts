export class ApplicationList {
    constructor(
        public vacancyID?: number,
        public vacancyName?: string,
        public status?: string
    ) {}
}

export class SeekerApplicant {
    constructor(
        public id?: number,
        public name?: string,
        public provinsi?: string,
        public kelurahan?: string,
        public education?: string,
        public userID?: number,
        public statusID?: number
    ) {}
}

export class Experience {
    constructor(
        public id?: number,
        public position?: string,
        public companyName?: string
    ) {

    }
}

export class SeekerAppDetail {
    constructor(
        public id?: number,
        public image?: string,
        public jobTitle?: string,
        public specialization?: string,
        public education?: string,
        public kelurahan?: string,
        public provinsi?: string,
        public firstName?: string,
        public lastName?: string
    ) {}
}

export class ProcessState {
    constructor(
        public statusID?: number,
        public userSeekerID?: number,
        public userCompanyVacancyID?: number,
        public errCode?: string,
        public errDesc?: string
    ) {}
}
