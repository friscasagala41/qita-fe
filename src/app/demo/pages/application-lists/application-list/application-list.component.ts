import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApplicationList } from './applicatio-list.model';
import { ApplicationListService } from './application-list.service';

@Component({
  selector: 'app-application-list',
  templateUrl: './application-list.component.html',
  styleUrls: ['./application-list.component.scss']
})
export class ApplicationListComponent implements OnInit {
  applicationLists: ApplicationList[];

  constructor(
    private applicationListService: ApplicationListService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getAllTrxVacancy();
  }

  getAllTrxVacancy() {
    this.applicationListService.getAllTrxVacancy()
        .subscribe(
            (res: HttpResponse<ApplicationList[]>) => {
                console.log('res.body', res.body);
                this.successLoadUser(res.body);
            }
        );
  }

  private successLoadUser(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.applicationLists = data.contents;
    console.log('applicationLists: ', this.applicationLists);
  }

  save(data: ApplicationList) {
    console.log(data.vacancyID);
    this.router.navigate(['/applicant-list/save/' + data.vacancyID]);
  }

  apply(data: ApplicationList) {
    this.router.navigate(['/applicant-list/apply/' + data.vacancyID]);
  }

}
