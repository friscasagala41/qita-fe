import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppliedApplicationRoutingModule } from './applied-application-routing.module';
import { AppliedApplicationComponent } from './applied-application.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
@NgModule({
  declarations: [AppliedApplicationComponent],
  imports: [
    CommonModule,
    AppliedApplicationRoutingModule,
    SharedModule,
    NgbModule
  ]
})
export class AppliedApplicationModule { }
