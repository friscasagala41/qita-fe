import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppliedApplicationComponent } from './applied-application.component';


const routes: Routes = [
  {
    path: '',
    component: AppliedApplicationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppliedApplicationRoutingModule { }
