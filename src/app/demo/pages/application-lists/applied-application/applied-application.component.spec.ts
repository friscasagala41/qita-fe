import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppliedApplicationComponent } from './applied-application.component';

describe('AppliedApplicationComponent', () => {
  let component: AppliedApplicationComponent;
  let fixture: ComponentFixture<AppliedApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppliedApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppliedApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
