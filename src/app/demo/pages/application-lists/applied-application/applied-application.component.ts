import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SeekerApplicant } from '../application-list/applicatio-list.model';
import { ApplicationListService } from '../application-list/application-list.service';

@Component({
  selector: 'app-applied-application',
  templateUrl: './applied-application.component.html',
  styleUrls: ['./applied-application.component.scss']
})
export class AppliedApplicationComponent implements OnInit {
  seekers: SeekerApplicant[];
  vacancyId = '';
  location = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private applicantService: ApplicationListService
  ) { }

  ngOnInit() {
    this.vacancyId = this.route.snapshot.paramMap.get('id');
    this.getAllApplicantApply(this.vacancyId, 1);
  }

  back() {
    this.router.navigate(['/applicant-list']);
  }

  getAllApplicantApply(ids, statusID) {
    this.applicantService.getAllApplicantApply({
      id: ids, status: statusID, kelurahan: this.location
    })
        .subscribe(
            (res: HttpResponse<SeekerApplicant[]>) => {
                console.log('res.body', res.body);
                this.successLoadUser(res.body);
            }
        );
  }

  private successLoadUser(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.seekers = data.contents;
    console.log('seekers: ', this.seekers);
  }

  detail(userID, statusID) {
    this.router.navigate(['/applicant-list/detail/' + userID + '/' + statusID + '/' + this.vacancyId]);
  }

  changeLocation(event) {
    console.log('vacancyID: ', this.vacancyId);
    this.location = event;
    this.getAllApplicantApply(this.vacancyId, 1);
  }
}
