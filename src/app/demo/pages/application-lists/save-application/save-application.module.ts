import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SaveApplicationRoutingModule } from './save-application-routing.module';
import { SaveApplicationComponent } from './save-application.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';


@NgModule({
  declarations: [SaveApplicationComponent],
  imports: [
    CommonModule,
    SaveApplicationRoutingModule,
    SharedModule
  ]
})
export class SaveApplicationModule { }
