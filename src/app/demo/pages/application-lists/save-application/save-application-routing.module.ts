import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SaveApplicationComponent } from './save-application.component';


const routes: Routes = [
  {
    path: '',
    component: SaveApplicationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SaveApplicationRoutingModule { }
