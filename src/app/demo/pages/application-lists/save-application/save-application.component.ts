import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SeekerApplicant } from '../application-list/applicatio-list.model';
import { ApplicationListService } from '../application-list/application-list.service';

@Component({
  selector: 'app-save-application',
  templateUrl: './save-application.component.html',
  styleUrls: ['./save-application.component.scss']
})
export class SaveApplicationComponent implements OnInit {
  seekers: SeekerApplicant[];
  location = '';
  vacancyId = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private applicantService: ApplicationListService
  ) { }

  ngOnInit() {
    this.vacancyId = this.route.snapshot.paramMap.get('id');
    this.getAllApplicantSave(this.vacancyId);
  }

  back() {
    this.router.navigate(['/applicant-list']);
  }

  getAllApplicantSave(ids) {
    this.applicantService.getAllApplicantSave({
      id: ids, kelurahan: this.location
    })
        .subscribe(
            (res: HttpResponse<SeekerApplicant[]>) => {
                console.log('res.body', res.body);
                this.successLoadUser(res.body);
            }
        );
  }

  private successLoadUser(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.seekers = data.contents;
    console.log('seekers: ', this.seekers);
  }

  changeLocation(event) {
    console.log('vacancyID: ', this.vacancyId);
    this.location = event;
    this.getAllApplicantSave(this.vacancyId);
  }

  detail(userID, statusID) {
    this.router.navigate(['/applicant-list/detail/' + userID + '/' + statusID + '/' + this.vacancyId]);
  }

}
