import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SocialMediasRoutingModule } from './social-medias-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SocialMediasRoutingModule
  ]
})
export class SocialMediasModule { }
