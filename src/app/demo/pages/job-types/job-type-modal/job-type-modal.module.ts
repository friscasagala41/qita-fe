import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobTypeModalComponent } from './job-type-modal.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [JobTypeModalComponent]
})
export class JobTypeModalModule { }
