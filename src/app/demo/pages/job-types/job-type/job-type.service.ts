import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { JobType } from './job-type.model';

@Injectable({
  providedIn: 'root'
})
export class JobTypeService {
  // public subject = new Subject<any>();

  private dataSource = new BehaviorSubject<any>(new JobType());
  data = this.dataSource.asObservable();
  // data = this.subject.asObservable();
  SERVER: string;
  constructor(private http: HttpClient,
              private configService: AppConfigService) {
        this.SERVER = this.configService.getSavedServerPath();
    }

  updatedDataSelection(data: JobType) {
      this.dataSource.next(data);
  }

  filter(req?: any): Observable<HttpResponse<JobType[]>> {
    let pageNumber = null;
    let pageCount = null;
    let newResourceUrl = null;
    let result = null;
    const search = {
        name: ''
    };

    Object.keys(req).forEach((key) => {
        if (key === 'page') {
            pageNumber = req[key];
        }
        if (key === 'count') {
            pageCount = req[key];
        }
        if (key === 'name') {
            search.name = req[key];
        }
    });

    newResourceUrl = this.SERVER + `/api/job-type/filter/page/${pageNumber}/count/${pageCount}`;

    result = this.http.post<JobType[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  getAllJobType(): Observable<HttpResponse<JobType[]>> {
      let newResourceUrl = null;
      let result = null;
      const search = {};

      newResourceUrl = this.SERVER + `/api/job-type/all`;

      result = this.http.post<JobType[]>(newResourceUrl, search, { observe: 'response' })
          .pipe(
              tap(
                  // console.log('sdad')
                  results => console.log('raw', results)
              )
          );
      // console.log(result);
      return result;
  }

  add(jobType: JobType): Observable<HttpResponse<JobType>> {
    const copy = Object.assign({}, jobType);

    copy.id = +copy.id;

    const newResourceUrl = this.SERVER + `/api/job-type/`;

    return this.http.post<JobType>(newResourceUrl, copy, { observe: 'response'})
        .pipe(map((res: HttpResponse<JobType>) => Object.assign({}, res)));
  }

  getJobType(req ?: any): Observable<HttpResponse<JobType>> {
    let newResourceUrl = null;
    let result = null;
    let id = null;
    const search = {};

    Object.keys(req).forEach((key) => {
      if (key === 'id') {
          id = req[key];
      }
    });

    newResourceUrl = this.SERVER + `/api/job-type/${id}`;

    result = this.http.post<JobType[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

  update(jobType: JobType): Observable<HttpResponse<JobType>> {
    const copy = Object.assign({}, jobType);

    copy.id = + copy.id;

    const newResourceUrl = this.SERVER + `/api/job-type/`;

    return this.http.put<JobType>(newResourceUrl, copy, { observe: 'response' })
        .pipe(map((res: HttpResponse<JobType>) => Object.assign({}, res)));
  }

}
