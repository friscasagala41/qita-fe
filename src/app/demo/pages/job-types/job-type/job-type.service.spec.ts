/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { JobTypeService } from './job-type.service';

describe('Service: JobType', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JobTypeService]
    });
  });

  it('should ...', inject([JobTypeService], (service: JobTypeService) => {
    expect(service).toBeTruthy();
  }));
});
