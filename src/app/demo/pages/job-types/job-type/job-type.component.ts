import { HttpResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MDBModalRef, MDBModalService } from 'ng-uikit-pro-standard';
import { Subject } from 'rxjs';
import { TOTAL_RECORD_PER_PAGE } from 'src/app/theme/shared/constant/base.constant';
import { JobTypeModalComponent } from '../job-type-modal/job-type-modal.component';
import { JobType } from './job-type.model';
import { JobTypeService } from './job-type.service';

@Component({
  selector: 'app-job-type',
  templateUrl: './job-type.component.html',
  styleUrls: ['./job-type.component.scss']
})
export class JobTypeComponent implements OnInit, OnDestroy {
  searchName;
  jobTypes: JobType[];

  maxTotalpage = 1;
  curPage: number;
  totalItems: any;
  itemsPerPage: any;
  previousPage: any;
  // total = [];
  nameJobType = '';
  jobType: JobType = new JobType();
  jobTypeForm: FormGroup;
  action = new Subject();
  closeResult: string;
  modalRef: MDBModalRef;

  constructor(
    private jobTypeService: JobTypeService,
    private fb: FormBuilder,
    private modalService: MDBModalService,
  ) { }

  ngOnInit() {
    this.curPage = 1;
    this.filter(this.curPage);
  }

  rForm() {
    this.jobTypeForm = this.fb.group({
      nameJobType: ['']
    });
  }


  filter(curpage) {
    console.log('searc ', this.searchName);
    this.jobTypeService.filter({
        page: curpage,
        count: 10,
        name: this.searchName,
    })
        .subscribe(
            (res: HttpResponse<JobType[]>) => {
                console.log('res.body', res.body);
                this.successLoadUser(res.body);

            }
        );
  }

  private successLoadUser(data) {
    console.log('succesloaduser ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.jobTypes = data.contents;
    this.maxTotalpage = Math.ceil(data.totalRow / TOTAL_RECORD_PER_PAGE);
    this.totalItems = data.totalRow;
    this.itemsPerPage = TOTAL_RECORD_PER_PAGE;
  }

  ngOnDestroy() {
    // this.userService.clearMessages();
  }

  loadPage(curPage) {
      if (this.curPage !== this.previousPage) {
        this.previousPage = this.curPage;
        this.filter(this.curPage);
        console.log('filter', this.filter);
      }
  }

  open(status: string, data: JobType) {

    if (status === 'Add') {
        data = new JobType();
        data.id = 0;
        data.name = '';
    }
    console.log('isi dataaa ', data);

    this.jobTypeService.updatedDataSelection(data);

    const modalOptions = {
        backdrop: true,
        keyboard: true,
        focus: true,
        show: false,
        ignoreBackdropClick: false,
        class: 'modal-lg',
        containerClass: 'right',
        animated: true,
        data: {
            objedit: data,
            uneditable: false
        }
    };

    if (status === 'detail') {
        modalOptions.data.uneditable = true;
    }

    // console.log()
    this.modalRef = this.modalService.show(JobTypeModalComponent, modalOptions);
    this.modalRef.content.action.subscribe(
        (result: any) => {
            console.log(result);
            if (result === 'refresh') {
                this.curPage = 1;
                this.filter(this.curPage);
            }
        });
  }

}
