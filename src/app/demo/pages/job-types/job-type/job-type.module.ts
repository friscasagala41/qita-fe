import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobTypeRoutingModule } from './job-type-routing.module';
import { JobTypeComponent } from './job-type.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import {NgbDropdownModule, NgbModalModule, NgbModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [JobTypeComponent],
  imports: [
    CommonModule,
    JobTypeRoutingModule,
    SharedModule,
    NgbDropdownModule,
    NgbModalModule,
    NgbPaginationModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class JobTypeModule { }
