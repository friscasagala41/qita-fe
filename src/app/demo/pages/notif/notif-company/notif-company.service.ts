import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { Seeker } from './notif-company.model';

@Injectable({
  providedIn: 'root'
})
export class NotifCompanyService {
  // public subject = new Subject<any>();

  private dataSource = new BehaviorSubject<any>(new Seeker());
  data = this.dataSource.asObservable();
  // data = this.subject.asObservable();
  SERVER: string;
  constructor(private http: HttpClient,
              private configService: AppConfigService) {
        this.SERVER = this.configService.getSavedServerPath();
    }

  updatedDataSelection(data: Seeker) {
      this.dataSource.next(data);
  }

  getNotifRecruiter(req?: any): Observable<HttpResponse<Seeker[]>> {
    let pageNumber = null;
    let pageCount = null;
    let newResourceUrl = null;
    let result = null;
    const search = {};

    Object.keys(req).forEach((key) => {
        if (key === 'page') {
            pageNumber = req[key];
        }
        if (key === 'count') {
            pageCount = req[key];
        }
    });

    newResourceUrl = this.SERVER + `/api/trx-user-vacancy/notif/recruiter/page/${pageNumber}/count/${pageCount}`;

    result = this.http.post<Seeker[]>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                results => console.log('raw', results)
            )
        );
    return result;
  }

}
