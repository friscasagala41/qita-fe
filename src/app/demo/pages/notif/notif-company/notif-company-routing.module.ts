import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotifCompanyComponent } from './notif-company.component';


const routes: Routes = [
  {
    path: '',
    component: NotifCompanyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotifCompanyRoutingModule { }
