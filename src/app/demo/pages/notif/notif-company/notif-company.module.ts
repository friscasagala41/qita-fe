import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotifCompanyRoutingModule } from './notif-company-routing.module';
import { NotifCompanyComponent } from './notif-company.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { NgbPaginationModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [NotifCompanyComponent],
  imports: [
    CommonModule,
    NotifCompanyRoutingModule,
    SharedModule,
    NgbTabsetModule,
    NgbPaginationModule
  ]
})
export class NotifCompanyModule { }
