import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TOTAL_RECORD_PER_PAGE } from 'src/app/theme/shared/constant/base.constant';
import { Seeker } from './notif-company.model';
import { NotifCompanyService } from './notif-company.service';

@Component({
  selector: 'app-notif-company',
  templateUrl: './notif-company.component.html',
  styleUrls: ['./notif-company.component.scss']
})
export class NotifCompanyComponent implements OnInit {
  seekers: Seeker[];
  maxTotalpage = 1;
  curPage: number;
  totalItems: any;
  itemsPerPage: any;
  previousPage: any;

  constructor(
    private notifRecruiterService: NotifCompanyService,
    public router: Router,
  ) {
  }

  ngOnInit() {
    this.curPage = 1;
    this.getNotifRecruiter(this.curPage);
  }

  getNotifRecruiter(curpage) {
    this.notifRecruiterService.getNotifRecruiter({
      page: curpage,
      count: 10
  })
        .subscribe(
            (res: HttpResponse<Seeker[]>) => {
                console.log('res.body', res.body);
                this.successLoadNotifRecruiter(res.body);
            }
        );
  }

  private successLoadNotifRecruiter(data) {
    console.log('successLoadNotifRecruiter ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.seekers = data.contents;
    this.maxTotalpage = Math.ceil(data.totalRow / TOTAL_RECORD_PER_PAGE);
    this.totalItems = data.totalRow;
    this.itemsPerPage = TOTAL_RECORD_PER_PAGE;
  }

  loadPage(curPage) {
    if (this.curPage !== this.previousPage) {
      this.previousPage = this.curPage;
      this.getNotifRecruiter(this.curPage);
    }
  }
}
