/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { NotifCompanyService } from './notif-company.service';

describe('Service: NotifCompany', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotifCompanyService]
    });
  });

  it('should ...', inject([NotifCompanyService], (service: NotifCompanyService) => {
    expect(service).toBeTruthy();
  }));
});
