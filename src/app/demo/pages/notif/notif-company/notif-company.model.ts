export class Seeker {
    constructor(
        public id?: number,
        public image?: string,
        public userID?: number,
        public firstName?: string,
        public lastName?: string,
        public isReadStatus?: number,
        public isReadSave?: number,
        public errCode?: string,
        public errDesc?: string,
        public statusID?: number,
        public isSave?: boolean
    ) {}
}
