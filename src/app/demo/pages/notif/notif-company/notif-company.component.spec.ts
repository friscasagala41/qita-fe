import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotifCompanyComponent } from './notif-company.component';

describe('NotifCompanyComponent', () => {
  let component: NotifCompanyComponent;
  let fixture: ComponentFixture<NotifCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotifCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotifCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
