import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'company',
        loadChildren: () => import('./notif-company/notif-company.module').then(module => module.NotifCompanyModule)
      }
    ]
  },
  {
    path: '',
    children: [
      {
        path: 'user',
        loadChildren: () => import('./notif-user/notif-user.module').then(module => module.NotifUserModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotifRoutingModule { }
