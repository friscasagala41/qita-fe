import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TOTAL_RECORD_PER_PAGE } from 'src/app/theme/shared/constant/base.constant';
import { Recruiter } from './notif-user.model';
import { NotifUserService } from './notif-user.service';

@Component({
  selector: 'app-notif-user',
  templateUrl: './notif-user.component.html',
  styleUrls: ['./notif-user.component.scss']
})
export class NotifUserComponent implements OnInit {

  recruiters: Recruiter[];
  maxTotalpage = 1;
  curPage: number;
  totalItems: any;
  itemsPerPage: any;
  previousPage: any;

  constructor(
    private notifSeekerService: NotifUserService,
    public router: Router,
  ) {
  }

  ngOnInit() {
    this.curPage = 1;
    this.getNotifSeeker(this.curPage);
  }

  getNotifSeeker(curpage) {
    this.notifSeekerService.getNotifSeeker({
      page: curpage,
      count: 10
  })
        .subscribe(
            (res: HttpResponse<Recruiter[]>) => {
                console.log('res.body', res.body);
                this.successLoadNotifSeeker(res.body);
            }
        );
  }

  private successLoadNotifSeeker(data) {
    console.log('successLoadNotifSeeker ', data);
    if (data.totalRow < 0) {
        return;
    }
    console.log(data);
    this.recruiters = data.contents;
    this.maxTotalpage = Math.ceil(data.totalRow / TOTAL_RECORD_PER_PAGE);
    this.totalItems = data.totalRow;
    this.itemsPerPage = TOTAL_RECORD_PER_PAGE;
  }

  loadPage(curPage) {
    if (this.curPage !== this.previousPage) {
      this.previousPage = this.curPage;
      this.getNotifSeeker(this.curPage);
    }
  }

}
