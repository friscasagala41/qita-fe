/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { NotifUserService } from './notif-user.service';

describe('Service: NotifUser', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotifUserService]
    });
  });

  it('should ...', inject([NotifUserService], (service: NotifUserService) => {
    expect(service).toBeTruthy();
  }));
});
