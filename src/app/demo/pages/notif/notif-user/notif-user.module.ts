import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotifUserRoutingModule } from './notif-user-routing.module';
import { NotifUserComponent } from './notif-user.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { NgbPaginationModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [NotifUserComponent],
  imports: [
    CommonModule,
    NotifUserRoutingModule,
    SharedModule,
    NgbTabsetModule,
    NgbPaginationModule
  ]
})
export class NotifUserModule { }
