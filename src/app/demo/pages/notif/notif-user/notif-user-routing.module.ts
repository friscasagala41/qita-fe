import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotifUserComponent } from './notif-user.component';


const routes: Routes = [
  {
    path: '',
    component: NotifUserComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotifUserRoutingModule { }
