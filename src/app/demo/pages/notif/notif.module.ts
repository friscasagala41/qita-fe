import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotifRoutingModule } from './notif-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NotifRoutingModule
  ]
})
export class NotifModule { }
