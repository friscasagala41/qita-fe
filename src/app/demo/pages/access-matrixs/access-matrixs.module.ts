import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccessMatrixsRoutingModule } from './access-matrixs-routing.module';
import { SharedModule } from 'src/app/theme/shared/shared.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AccessMatrixsRoutingModule,
    SharedModule
  ]
})
export class AccessMatrixsModule { }
