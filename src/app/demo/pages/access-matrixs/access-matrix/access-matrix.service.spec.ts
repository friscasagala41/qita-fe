/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AccessMatrixService } from './access-matrix.service';

describe('Service: AccessMatrix', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccessMatrixService]
    });
  });

  it('should ...', inject([AccessMatrixService], (service: AccessMatrixService) => {
    expect(service).toBeTruthy();
  }));
});
