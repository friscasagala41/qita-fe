import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccessMatrixComponent } from './access-matrix.component';


const routes: Routes = [
  {
    path: '',
    component: AccessMatrixComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccessMatrixRoutingModule { }
