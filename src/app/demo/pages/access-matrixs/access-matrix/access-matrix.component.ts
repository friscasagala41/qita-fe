import { Component, Injectable, OnInit } from '@angular/core';
import { AccessMatrixService } from './access-matrix.service';
import { TreeviewConfig, TreeviewItem, TreeviewEventParser, DownlineTreeviewItem,
  OrderDownlineTreeviewEventParser } from 'ngx-treeview';
import { RoleService } from '../../roles/role/role.service';
import { TOTAL_RECORD_PER_PAGE } from 'src/app/theme/shared/constant/base.constant';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Role } from '../../roles/role/role.model';
import { UserMenu } from './user.model';
import * as _ from 'lodash';
import { createTree, levelAndSort } from 'src/app/theme/utils/treeBuilder';
import { RoleMenuView } from './access-matrix.model';
import Swal from 'sweetalert2';
import { element } from 'protractor';

@Injectable()
  export class ProductTreeviewConfig extends TreeviewConfig {
      hasAllCheckBox = true;
      hasFilter = true;
      hasCollapseExpand = false;
      maxHeight = 400;
  }

@Component({
  selector: 'app-access-matrix',
  templateUrl: './access-matrix.component.html',
  styleUrls: ['./access-matrix.component.scss'],
  providers: [
    { provide: TreeviewEventParser, useClass: OrderDownlineTreeviewEventParser },
    { provide: TreeviewConfig, useClass: ProductTreeviewConfig },
  ]
})
export class AccessMatrixComponent implements OnInit {
  items: TreeviewItem[];
  links: any[] = [];
  menuArr = [];
  optionsSelect: Array<any>;

  filter = {
      roleId: null
  };

  curPage = 1;
  totalRecord = TOTAL_RECORD_PER_PAGE;

  constructor(
      private accessMatrixService: AccessMatrixService,
      private roleService: RoleService
  ) { }

  ngOnInit() {
      console.log('ngOnInit');
      this.items = [];
      this.optionsSelect = [];

      this.roleService.filter({
          page: this.curPage,
          count: this.totalRecord,
      })
      .subscribe(
              (res: HttpResponse<Role[]>) => this.onSuccessListRole(res.body),
              (res: HttpErrorResponse) => this.onError(res.message),
              () => { console.log('finally'); }
      );

      this.accessMatrixService.getAllActiveMenu()
      .subscribe(
          (res: HttpResponse<UserMenu[]>) => this.onSuccessListMenu(res.body, res.headers),
          (res: HttpErrorResponse) => this.onError(res.message),
          () => { console.log('finally'); }
      );

      // this.accessMatrixService.getMenuMock()
      // .subscribe(data => {
      //     // console.log('data : ', data);
      //     if ( data.length < 0 ) {
      //         return ;
      //     }
      //     this.setUserMenu(data);
      // });
  }

  private onSuccessListMenu(data, headers) {
      console.log('onSuccessListMenu : ', data);
      if (data.length <= 0) {
          return;
      }
      this.setUserMenu(data);
  }

  private onSuccessListRole(dt) {
      const data = dt.contents;
      console.log('data role', dt);
      console.log('onSuccessListRole : ', data);
      this.optionsSelect = [];
      if (data.length <= 0) {
          return;
      }

      data.forEach(el => {
          this.optionsSelect.push({
              value: el.id, label: el.name
          });
      });
      console.log(this.optionsSelect);
  }

  private onError(error) {
      console.log('error..');
  }

  setUserMenu(arr) {
      // console.log('setUserMenu..');
      // remark below to previous version
      levelAndSort(arr, 1);
      const arrTree = [];
      arr.forEach(tamp => {
          arrTree.push({
              value: tamp.id,
              // text: element.name,
              text: tamp.description,
              parentId: tamp.parentId,
              // checked: false,
          });
      });
      //
      console.log('arr after levelandsort : ', arr);
      // this.links = this.createTree(arr);
      this.links = createTree(arrTree, 'value');
      console.log(this.links);

      this.links.forEach(tamp => {
          this.items.push(new TreeviewItem({
              text: tamp.text,
              value: tamp.value,
              children: tamp.children,
              // checked: element.checked
          }));
      });
      this.setFalseChecked(this.items);

      this.searchByRole();
  }

  onFilterChange(evt) {
      console.log('filter : ', evt);
  }

  searchByRole() {
      // this.items = [];
      this.setFalseChecked(this.items);
      console.log('searchByRole..');
      if (this.filter.roleId !== null) {
          console.log('role id !== null');
          // this.accessMatrixService.findByRoleMock()
          //     .subscribe(data => {
          //         // console.log('data : ', data);
          //         if (data.length < 0) {
          //             return;
          //         }
          //         const header = null;
          //         this.onSuccessByRole(data, header);
          //     });

          this.accessMatrixService.findByRole(this.filter.roleId)
              .subscribe(
                  (res: HttpResponse<RoleMenuView[]>) => {
                      this.onSuccessByRole(res.body, res.headers);
                      // this.menuRegistered = res.body.content;
                  },
                  (res: HttpErrorResponse) => this.onError(res.message),
                  () => { console.log('finally'); }
              );
      }
  }

  private onSuccessByRole(data, headers) {
      console.log('onSuccessByRole...');
      console.log(data);

      if (data.length <= 0) {
          return;
      }
      const temp = [];
      const datas = JSON.stringify([data.contents]);
      const data1 = JSON.parse(datas);
      data1.forEach(tamp => {
          // tslint:disable-next-line:no-string-literal
          // tslint:disable-next-line:prefer-for-of
          for (let index = 0; index < tamp.length; index++) {
              temp.push(tamp[index].menuId);
          }
      });
      this.setFalseChecked(this.items);
      this.findChecked(this.items, temp);
  }

  setFalseChecked(items) {
      if (!items || items.length === 0) { return; }

      for (const item of items) {
          item.checked = false;

          // Test children recursively
          if (item.children && item.children.length > 0) {
              this.setFalseChecked(item.children);
          }
      }
  }

  findChecked(items, arr) {
      if (!items || items.length === 0) { return; }

      for (const item of items) {
          // Test current object
          if (_.indexOf(arr, item.value) > -1) {
              item.checked = true;
          }

          // Test children recursively
          if (item.children && item.children.length > 0) {
              this.findChecked(item.children, arr);
          }
      }
  }

  onSelectedChange(downlineItems: DownlineTreeviewItem[]) {
      this.menuArr = [];
      console.log('downlineItems : ', downlineItems);
      downlineItems.forEach(downlineItem => {
          let arrTemp = [];
          this.recMenuArr(downlineItem, arrTemp);
          arrTemp = _.reverse(arrTemp);
          this.menuArr.push(arrTemp);
      });
      this.menuArr = _.flattenDeep(this.menuArr);
      console.log(this.menuArr);
  }

  recMenuArr(downlineItem: DownlineTreeviewItem, arr) {
      if (_.indexOf(_.flattenDeep(this.menuArr), downlineItem.item.value) < 0) {
          arr.push(downlineItem.item.value);
      }
      if (_.isNil(downlineItem.parent)) {
          return;
      } else {
          this.recMenuArr(downlineItem.parent, arr);
      }
  }

  saveMenuAccess() {
      // get all selected nodes
      console.log('this.menuArr : ', this.menuArr);
      console.log('this.filter.roleId : ', this.filter.roleId);

      if (this.menuArr.length === 0) {
          return;
      }
      this.accessMatrixService.save({
          roleId: this.filter.roleId,
          menuIds: this.menuArr
      })
      .subscribe((result: HttpResponse<any>) => {
          console.log('save success..');

          if (result.body.errCode === '00') {
              console.log('Toast success');
              Swal.fire('Success', result.body.errDesc, 'success');
          } else {
              // console.log('Toast err', result.body.errDesc);
              Swal.fire('Error', result.body.errDesc, 'error');
          }
      },
      (result: HttpErrorResponse) => {
          console.log('error msh ', result);
      });
  }

}
