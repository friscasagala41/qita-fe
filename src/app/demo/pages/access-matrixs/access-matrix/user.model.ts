import { Role } from '../../roles/role/role.model';

export class User {
    constructor(
        public id?: number,
        public email?: string,
        public firstName?: string,
        public isLock?: number,
        public lastName?: string,
        public userName?: string,
        public password?: string,
        public status?: number,
        public roleId?: number,
        public role?: Role,
        public errCode?: string,
        public errDesc?: string
    ) {}

}

export class UserMenu {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public link?: string,
        public icon?: string,
        public parentId?: number,
    ) {
    }
}


export class Menus {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public link?: string,
        public icon?: string,
        public parentId?: number,
    ) {
    }
}
