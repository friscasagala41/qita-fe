import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { RoleMenuView } from './access-matrix.model';
import { UserMenu } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class AccessMatrixService {

  SERVER: string;


  constructor(private http: HttpClient,
              private configService: AppConfigService, ) {
      this.SERVER = this.configService.getSavedServerPath();
  }
  // private resourceUrl = this.SERVER + '/menu';

  getMenuMock(): Observable<any> {
      return this.http.get('./assets/mock-data/menu-mock.json');
  }

  getAllActiveMenu(): Observable<HttpResponse<UserMenu[]>> {
      return this.http.get<UserMenu[]>(`${this.SERVER}/api/menu-role/`, { observe: 'response' })
          .pipe(
              // map((res: HttpResponse<memberType[]>) => this.convertArrayResponse(res))
              tap(accessMatrixMenu => console.log('raw ', accessMatrixMenu))
              // console.log('observable ', accessMatrixMenu)
          );

  }

  findByRole(idrole?: any): Observable<HttpResponse<RoleMenuView[]>> {
      return this.http.get<RoleMenuView[]>(this.SERVER + `/api/menu-role/roleid/${idrole}`, { observe: 'response' })
          .pipe(
              tap(menu => console.log('raw ', menu))
          );
  }

  save(req?: any): Observable<HttpResponse<any[]>> {
      console.log('Request ', req);

      let roleId = null;
      let newresourceUrl = null;

      Object.keys(req).forEach((key) => {
          if (key === 'roleId') {
              roleId = req[key];
          }
      });

      newresourceUrl = this.SERVER + `/api/menu-role/roleid/${roleId}`;
      return this.http.post<any[]>(newresourceUrl, req.menuIds, { observe: 'response' })
          .pipe(
              // map((res: HttpResponse<Member[]>) => this.convertArrayResponse(res))
              tap(results => console.log('raw ', results))
              // console.log('observable ', billerCompanies)
          );

  }

}
