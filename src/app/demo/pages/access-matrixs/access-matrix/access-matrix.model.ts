export class RoleMenuView {
    constructor(
        public menuId?: number,
        public status?: string,
        public menuDescription?: string,
        public nourut?: number,
    ) {
    }
}
