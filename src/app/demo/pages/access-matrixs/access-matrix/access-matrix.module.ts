import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccessMatrixRoutingModule } from './access-matrix-routing.module';
import { AccessMatrixComponent } from './access-matrix.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { TreeviewModule } from 'ngx-treeview';
import {
  CardsModule, TableModule, ButtonsModule, InputsModule, CheckboxModule,
  WavesModule, IconsModule, CollapseModule, TooltipModule, SelectModule,
} from 'ng-uikit-pro-standard';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [AccessMatrixComponent],
  imports: [
    CommonModule,
    AccessMatrixRoutingModule,
    SharedModule,
    TreeviewModule,
    CardsModule,
    TableModule,
    ButtonsModule,
    InputsModule,
    SelectModule,
    CheckboxModule,
    WavesModule,
    IconsModule,
    CollapseModule,
    ReactiveFormsModule,
    TooltipModule,
    FormsModule,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class AccessMatrixModule { }
