import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';
import { CountNotif } from './nav-right.model';

@Injectable({
  providedIn: 'root'
})
export class NavRightService {
  // public subject = new Subject<any>();

 private dataSource = new BehaviorSubject<any>(new CountNotif());
 data = this.dataSource.asObservable();
 // data = this.subject.asObservable();
 SERVER: string;
 constructor(private http: HttpClient,
             private configService: AppConfigService) {
       this.SERVER = this.configService.getSavedServerPath();
   }

 updatedDataSelection(data: CountNotif) {
     this.dataSource.next(data);
 }

 getCountNotifRecruiter(): Observable<HttpResponse<CountNotif>> {
  let newResourceUrl = null;
  let result = null;
  const search = {};

  newResourceUrl = this.SERVER + `/api/trx-user-vacancy/count/read/recruiter`;

  result = this.http.post<CountNotif>(newResourceUrl, search, { observe: 'response' })
      .pipe(
          tap(
              // console.log('sdad')
              results => console.log('raw', results)
          )
      );
  // console.log(result);
  return result;
  }

  getCountNotifSeeker(): Observable<HttpResponse<CountNotif>> {
    let newResourceUrl = null;
    let result = null;
    const search = {};

    newResourceUrl = this.SERVER + `/api/trx-user-vacancy/count/read/seeker`;

    result = this.http.post<CountNotif>(newResourceUrl, search, { observe: 'response' })
        .pipe(
            tap(
                // console.log('sdad')
                results => console.log('raw', results)
            )
        );
    // console.log(result);
    return result;
  }

}
