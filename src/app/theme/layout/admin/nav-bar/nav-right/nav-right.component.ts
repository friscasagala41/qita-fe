import { HttpResponse } from '@angular/common/http';
import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import {NgbDropdownConfig} from '@ng-bootstrap/ng-bootstrap';
import { AuthSigninService } from 'src/app/demo/pages/authentication/auth-signin/auth-signin.service';
import { AuthService } from '../../../auth/auth.service';
import { CountNotif } from './nav-right.model';
import { NavRightService } from './nav-right.service';

@Component({
  selector: 'app-nav-right',
  templateUrl: './nav-right.component.html',
  styleUrls: ['./nav-right.component.scss'],
  providers: [NgbDropdownConfig]
})
export class NavRightComponent implements OnInit {
  counter = 0;
  isRecruiter = false;

  constructor(
    private authService: AuthSigninService,
    private navRightService: NavRightService,
    private router: Router
  ) { }

  ngOnInit() {
    this.authService.getUserDetail().subscribe(result => {
      // tslint:disable-next-line:no-unused-expression
      console.log(result.body.contents);

      if (result.body.contents === 2) {
        this.getCountNotifRecruiter();
      }

      if (result.body.contents === 3 || result.body.contents === 1) {
        this.getCountNotifSeeker();
      }
    });
  }

  getCountNotifRecruiter() {
    this.navRightService.getCountNotifRecruiter()
        .subscribe(
            (res: HttpResponse<CountNotif>) => {
                console.log('res.body', res.body);
                this.successLoadCountNotifRecruiter(res.body);
            }
        );
  }

  private successLoadCountNotifRecruiter(data) {
    this.counter = data.contents;
    this.isRecruiter = true;
    console.log('countNotif: ', this.counter);
  }

  getCountNotifSeeker() {
    this.navRightService.getCountNotifSeeker()
        .subscribe(
            (res: HttpResponse<CountNotif>) => {
                console.log('res.body', res.body);
                this.successLoadCountNotifSeeker(res.body);
            }
        );
  }

  private successLoadCountNotifSeeker(data) {
    this.counter = data.contents;
    this.isRecruiter = false;
    console.log('countNotif: ', this.counter);
  }

  logout() {
    console.log('test');
    this.authService.logout();
  }

  notifRecruiter() {
    this.router.navigate(['/notif/company']);
  }

  notifSeeker() {
    this.router.navigate(['/notif/user']);
  }
}
