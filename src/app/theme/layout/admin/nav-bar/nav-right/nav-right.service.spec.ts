/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { NavRightService } from './nav-right.service';

describe('Service: NavRight', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NavRightService]
    });
  });

  it('should ...', inject([NavRightService], (service: NavRightService) => {
    expect(service).toBeTruthy();
  }));
});
