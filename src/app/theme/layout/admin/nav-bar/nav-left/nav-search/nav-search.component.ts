import { Component, OnInit } from '@angular/core';
import { CountNotif } from '../../nav-right/nav-right.model';

@Component({
  selector: 'app-nav-search',
  templateUrl: './nav-search.component.html',
  styleUrls: ['./nav-search.component.scss']
})
export class NavSearchComponent implements OnInit {
  countNotif: CountNotif;

  constructor() { }

  ngOnInit() { }

}
