import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Menus } from 'src/app/demo/pages/access-matrixs/access-matrix/user.model';
import { AppConfigService } from 'src/app/theme/shared/app-config.service';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

    SERVER = '' ;
    constructor(
      private http: HttpClient,
      private configService: AppConfigService, ) {
      this.SERVER = this.configService.getSavedServerPath(); }

    getUserMenu(): Observable<HttpResponse<Menus[]>> {
        return this.http.get<Menus[]>(`${this.SERVER}/api/menu/list-user-menu`, { observe: 'response' })
            .pipe(
                // map((res: HttpResponse<memberType[]>) => this.convertArrayResponse(res))
                tap(results => console.log('raw ', results))
                // console.log('observable ', accessMatrixMenu)
            );
    }

}
