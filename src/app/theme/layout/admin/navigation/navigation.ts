import {Injectable} from '@angular/core';

export interface NavigationItem {
  id: string;
  title: string;
  type: 'item' | 'collapse' | 'group';
  translate?: string;
  icon?: string;
  hidden?: boolean;
  url?: string;
  classes?: string;
  exactMatch?: boolean;
  external?: boolean;
  target?: boolean;
  breadcrumbs?: boolean;
  function?: any;
  badge?: {
    title?: string;
    type?: string;
  };
  children?: Navigation[];
}

export interface Navigation extends NavigationItem {
  children?: NavigationItem[];
}

// const NavigationItems = [
//   {
//     id: 'navigation',
//     title: 'Navigation',
//     type: 'group',
//     icon: 'feather icon-monitor',
//     children: [
//       {
//         id: 'dashboard',
//         title: 'Dashboard',
//         type: 'item',
//         url: '/dashboard/analytics',
//         icon: 'feather icon-home'
//       }
//       // {
//       //   id: 'page-layouts',
//       //   title: 'Horizontal Layouts',
//       //   type: 'item',
//       //   url: '/layout/horizontal',
//       //   target: true,
//       //   icon: 'feather icon-layout'
//       // }
//     ]
//   },
//   {
//     id: 'Utility',
//     title: 'Utility',
//     type: 'group',
//     icon: 'feather icon-layers',
//     children: [
//       {
//         id: 'profile-user',
//         title: 'Profile',
//         type: 'item',
//         url: '/profile/user',
//         icon: 'feather icon-user'
//       },
//       // {
//       //   id: 'access-matrix',
//       //   title: 'Access Matrix',
//       //   type: 'item',
//       //   url: '/access-matrix',
//       //   icon: 'feather icon-settings'
//       // },
//       {
//         id: 'role',
//         title: 'Role',
//         type: 'item',
//         url: '/role',
//         icon: 'feather icon-users'
//       }
//     ]
//   }
// ];

@Injectable()
export class NavigationItem {
  public get() {
    // return NavigationItems;
  }
}
