import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './theme/shared/shared.module';

import { AppComponent } from './app.component';
import { AdminComponent } from './theme/layout/admin/admin.component';
import { AuthComponent } from './theme/layout/auth/auth.component';
import { NavigationComponent } from './theme/layout/admin/navigation/navigation.component';
import { NavContentComponent } from './theme/layout/admin/navigation/nav-content/nav-content.component';
import { NavGroupComponent } from './theme/layout/admin/navigation/nav-content/nav-group/nav-group.component';
import { NavCollapseComponent } from './theme/layout/admin/navigation/nav-content/nav-collapse/nav-collapse.component';
import { NavItemComponent } from './theme/layout/admin/navigation/nav-content/nav-item/nav-item.component';
import { NavBarComponent } from './theme/layout/admin/nav-bar/nav-bar.component';
import { NavLeftComponent } from './theme/layout/admin/nav-bar/nav-left/nav-left.component';
import { NavSearchComponent } from './theme/layout/admin/nav-bar/nav-left/nav-search/nav-search.component';
import { NavRightComponent } from './theme/layout/admin/nav-bar/nav-right/nav-right.component';
import { ConfigurationComponent } from './theme/layout/admin/configuration/configuration.component';

import { ToggleFullScreenDirective } from './theme/shared/full-screen/toggle-full-screen';

/* Menu Items */
import { NavigationItem } from './theme/layout/admin/navigation/navigation';
import { NgbButtonsModule, NgbDropdownModule, NgbModule, NgbTabsetModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {NgxWebstorageModule} from 'ngx-webstorage';
import { AuthSignupComponent } from './demo/pages/authentication/auth-signup/auth-signup.component';
import { AuthServerProvider } from './theme/shared/auth-jwt/auth-jwt.service';
import { AuthSigninComponent } from './demo/pages/authentication/auth-signin/auth-signin.component';
import { AuthSigninModule } from './demo/pages/authentication/auth-signin/auth-signin.module';
import { MyHttpInterceptor } from './theme/shared/auth-jwt/httpIntercepCall';
import {MDBBootstrapModule, ModalModule, ToastModule} from 'ng-uikit-pro-standard';
import { RoleModalComponent } from './demo/pages/roles/role-modal/role-modal.component';
import { TOAST_OPTIONS } from './theme/shared/constant/base.constant';
import {NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { TreeviewModule } from 'ngx-treeview';
import { SkillModalComponent } from './demo/pages/skills/skill-modal/skill-modal.component';
import { CareerLevelModalComponent } from './demo/pages/career-levels/career-level-modal/career-level-modal.component';
import { JobTypeModalComponent } from './demo/pages/job-types/job-type-modal/job-type-modal.component';
import { SpecializationModalComponent } from './demo/pages/specializations/specialization-modal/specialization-modal.component';
import { WorkingSiteModalComponent } from './demo/pages/working-sites/working-site-modal/working-site-modal.component';
import { LocationModalComponent } from './demo/pages/locations/location-modal/location-modal.component';
// tslint:disable-next-line:max-line-length
import { CompanyLocationModalComponent } from './demo/pages/company/company-location/company-location-modal/company-location-modal.component';
import { CompanyAdditionalComponent } from './demo/pages/company/company-additional/company-additional.component';
// tslint:disable-next-line:max-line-length
import { CompanyAdditionalModalComponent } from './demo/pages/company/company-additional/company-additional-modal/company-additional-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    AuthSignupComponent,
    NavigationComponent,
    NavContentComponent,
    NavGroupComponent,
    NavCollapseComponent,
    NavItemComponent,
    NavBarComponent,
    NavLeftComponent,
    NavSearchComponent,
    NavRightComponent,
    ConfigurationComponent,
    ToggleFullScreenDirective,
    RoleModalComponent,
    SkillModalComponent,
    CareerLevelModalComponent,
    JobTypeModalComponent,
    SpecializationModalComponent,
    WorkingSiteModalComponent,
    CompanyLocationModalComponent,
    CompanyAdditionalModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    NgbDropdownModule,
    NgbTooltipModule,
    NgbButtonsModule,
    NgbTabsetModule,
    HttpClientModule,
    NgxWebstorageModule.forRoot(),
    AuthSigninModule,
    MDBBootstrapModule.forRoot(),
    HttpClientModule,
    ModalModule,
    ToastModule.forRoot( TOAST_OPTIONS),
    NgbModule,
    TreeviewModule.forRoot(),
  ],
  providers: [NavigationItem, AuthServerProvider,
    {
        provide: HTTP_INTERCEPTORS,
        useClass: MyHttpInterceptor,
        multi: true,
    }],
  bootstrap: [AppComponent],
  entryComponents: [RoleModalComponent, SkillModalComponent, CareerLevelModalComponent,
  JobTypeModalComponent, SpecializationModalComponent, WorkingSiteModalComponent,
  CompanyLocationModalComponent, CompanyAdditionalModalComponent],
  schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
